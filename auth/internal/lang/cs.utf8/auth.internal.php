<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['completeregistration'] = 'Dokončit registraci';
$string['confirmcancelregistration'] = 'Jste si jisti, že chcete zrušit tuto registraci? Pokud tak učiníte, výsledkem vaší žádosti bude odstranění ze systému.';
$string['confirmemailmessagehtml'] = '<p>Dobrý den %s,</p> <p>děkujeme za registraci účtu na %s. Pomocí následujícího odkazu potvrďte svou e-mailovou adresu. Správci instituce bude zasláno oznámení s žádostí o schválení a vy budete informování o výsledku.</p> <p><a href="%sregister.php?key=%s">%sregister.php?key=%s</a></p> <p>Platnost odkazu vyprší za 24 hodin.</p> <pre> -- S pozdravem, Tým %s</pre>';
$string['confirmemailmessagetext'] = 'Dobrý den %s,</p> <p>děkujeme za registraci účtu na %s. Pomocí následujícího odkazu potvrďte svou e-mailovou adresu. Správci instituce bude zasláno oznámení s žádostí o schválení a vy budete informování o výsledku. %sregister.php?key=%s Platnost odkazu vyprší za 24 hodin. -- S pozdravem, Tým %s';
$string['confirmemailsubject'] = 'Potvrzovací e-mail pro registraci na %s';
$string['description'] = 'Autentizace oproti databázi Mahary';
$string['emailalreadytaken'] = 'Tuto e-mailovou adresu již nějaký uživatel používá';
$string['emailconfirmedok'] = '<p>Úspěšně jste potvrdili svou e-mailovou adresu. O dalším postupu budete brzy vyrozuměni.</p>';
$string['iagreetothetermsandconditions'] = 'Souhlasím s Podmínkami používání těchto stránek';
$string['internal'] = 'Interní';
$string['passwordformdescription'] = 'Vaše heslo musí být alespoň 6 znaků dlouhé a musí obsahovat alespoň jednu číslici a dvě písmena';
$string['passwordinvalidform'] = 'Vaše heslo musí být alespoň 6 znaků dlouhé a musí obsahovat alespoň jednu číslici a dvě písmena';
$string['pendingregistrationadminemailhtml'] = '<p>Dobrý den %s,</p> <p> nový uživatel žádá o připojení do instituce \'%s\'. </p> <p>Protože jste její správce, je třeba tuto žádost schválit nebo zamítnout. Chcete-li to provést nyní, klikněte na následující odkaz: <a href=\'%s\'>%s</a></p> <p>Tuto žádost je třeba zpracovat do dvou týdnů.</p> <p>Podrobnosti žádosti o registraci následují:</p> <p>Name: %s</p> <p>Email: %s</p> <p>Důvod registrace:</p> <p>%s</p> <pre>-- S pozdravem, Tým %s</pre>';
$string['pendingregistrationadminemailsubject'] = 'Nová registrace uživatele pro instituci \'%s\' na %s.';
$string['pendingregistrationadminemailtext'] = 'Dobrý den %s, nový uživatel žádá o připojení do instituce \'%s\'. Protože jste její správce, je třeba tuto žádost schválit nebo zamítnout. Chcete-li to provést nyní, klikněte na následující odkaz: %s Tuto žádost je třeba zpracovat do dvou týdnů. Podrobnosti žádosti o registraci následují: Name: %s Email: %s Důvod registrace: %s -- S pozdravem, Tým %s';
$string['registeredemailmessagehtml'] = '<p>Toto je automaticky generovaná zpráva pro: %s</p>

<p>Děkujeme vám za registraci uživatelského účtu na stránkách %s. Pro dokončení registrace navštivte následující stránku:</p>

<p><a href="%sregister.php?key=%s">%sregister.php?key=%s</a></p>

<p>Platnost tohoto odkazu vyprší během 24 hodin.</p>

<p>S pozdravem, Tým %s</p>';
$string['registeredemailmessagetext'] = 'Toto je automaticky generovaná zpráva pro: %s

Děkujeme vám za registraci uživatelského účtu na stránkách %s. Pro dokončení registrace navštivte následující stránku:

%sregister.php?key=%s

Platnost tohoto odkazu vyprší během 24 hodin.

S pozdravem, Tým %s';
$string['registeredemailsubject'] = 'Byli jste zaregistrováni na %s';
$string['registeredok'] = '<p>Nyní jste úspěšně zaregistrováni. Další pokyny pro aktivaci vašeho účtu vám byly zaslány e-mailem.</p>';
$string['registeredokawaitingemail'] = 'Úspěšně jste podali přihlášku k registraci. Na vaši adresu byl zaslán e-mail a po jeho potvrzení lze pokračovat dále.';
$string['registrationcancelledok'] = 'Úspěšně jste zrušili žádost o registraci.';
$string['registrationconfirm'] = 'Potvrdit registraci?';
$string['registrationconfirmdescription'] = 'Registrace musí být schválena správcem instituce.';
$string['registrationdeniedemailsubject'] = 'Pokus o registraci na %s byl odepřen.';
$string['registrationdeniedmessage'] = 'Dobrý den %s, obdrželi jsme vaši žádost pro vstup do naší instituce na %s a rozhodli jsme se vám neudělit přístup. Pokud si myslíte, že toto rozhodnutí není správné, spojte se se námi e-mailem. S pozdravem. Tým %s';
$string['registrationdeniedmessagereason'] = 'Dobrý den %s, obdrželi jsme vaši žádost pro vstup do naší instituce na %s a rozhodli jsme se vám neudělit přístup z následujícího důvodu: %s. Pokud si myslíte, že toto rozhodnutí není správné, spojte se se námi e-mailem. S pozdravem, Tým %s';
$string['registrationnosuchkey'] = 'Je nám líto, ale neevidujeme žádnou žádost o registraci s tímto přístupovým klíčem. Možná že jste čekali s aktivací účtu déle než 24 hodin? Jinak by se mohlo jednat o naší chybu.';
$string['registrationreason'] = 'Důvod registrace';
$string['registrationreasondesc'] = 'Zdůvodnění žádosti o registraci do vámi zvolené instituce spolu s poskytnutím dalších detailů může být užitečné pro její správce, kteří žádost budou zpracovávat. Registraci proto nelze dokončit bez uvedení těchto informací.';
$string['registrationunsuccessful'] = 'Omlouváme se, ale při pokusu o registraci došlo k chybě na straně našeho serveru. Prosíme, opakujte registraci později.';
$string['title'] = 'Interní';
$string['usernamealreadytaken'] = 'Toto uživatelské jméno již používá jiný uživatel';
$string['usernameinvalidadminform'] = 'Uživatelská jména mohou obsahovat písmena, čísla a nejběžnější symboly a jejich délka musí být 3 až 236 znaků. Mezery nejsou povoleny.';
$string['usernameinvalidform'] = 'Uživatelská jména mohou obsahovat pouze písmena, čísla a běžné symboly. Musejí být dlouhá 3 až 30 znaků. Mezery nejsou v uživatelských jménech povoleny.';
$string['youmaynotregisterwithouttandc'] = 'Pokud se chcete registrovat, musíte potvrdit, že se budete řídit <a href="terms.php">Podmínkami používání těchto stránek</a>.';
