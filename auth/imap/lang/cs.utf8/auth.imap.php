<?php

defined('INTERNAL') || die();

$string['description'] = 'Autentizace oproti IMAP e-mailovému serveru';
$string['notusable'] = 'Nainstalujte PHP rozšíření pro podporu IMAP';
$string['title'] = 'IMAP';
