<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['badassertion'] = 'Uvedený výraz BrowserID není platný: %s';
$string['badverification'] = 'Mahara neobdržela od ověřovatele BrowserID platný výstup JSON.';
$string['browserid'] = 'BrowserID';
$string['browseridnotenabled'] = 'Zásuvný modul pro ověřování BrowserID není povolen v žádné aktivní instituci.';
$string['description'] = 'Ověření pomocí BrowserID';
$string['emailalreadyclaimed'] = 'E-mailovou adresu %s již využívá jiný uživatelský účet.';
$string['emailclaimedasusername'] = 'E-mailovou adresu %s již využívá jiný uživatelský účet jako své přihlašovací jméno.';
$string['emailnotfound'] = 'Uživatelský účet s e-mailovou adresou \'%s\' nebyl nalezen v žádné z institucí s povoleným BrowserID.';
$string['login'] = 'Přihlášení BrowserID';
$string['missingassertion'] = 'BrowserID nevrátil alfanumerický výraz.';
$string['title'] = 'BrowserID';
