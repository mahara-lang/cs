<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['defaultinstitution'] = 'Výchozí instituce';
$string['description'] = 'Autentizace oproti službě poskytování identit SAML 2.0';
$string['errnosamluser'] = 'Žádný uživatel nenalezen';
$string['errorbadcombo'] = 'Pokud jste si nevybrali vzdálené uživatele, můžete zvolit automatické zakládání nových uživatelů.';
$string['errorbadconfig'] = 'Adresář %s pro konfiguraci SimpleSAMLPHP není správný.';
$string['errorbadinstitution'] = 'Instituce pro připojení uživatele nelze kontaktovat.';
$string['errorbadinstitutioncombo'] = 'Authinstance v kombinaci s institutionattribute a institutionvalue již existuje.';
$string['errorbadlib'] = 'Adresář %s pro knihovny SimpleSAMLPHP není správný.';
$string['errorbadssphp'] = 'Neplatný popisovač relace SimpleSAMLphp - nesmí být phpsession';
$string['errorbadssphplib'] = 'Neplatná konfigurace knihovny SimpleSAMLphp';
$string['errormissinguserattributes'] = 'Zdá se, že jste byl autentizován, ale dosud nejsme neobdrželi požadované uživatelské atributy. Zkontrolujte si prosím, jestli váš poskytovatel identity uvolnil SSO pole pro křestní jméno, příjmení a e-mail a poskytovatele služeb Mahara je spušen nebo kontaktujte správce tohoto serveru.';
$string['errormissinguserattributes1'] = 'Zdáte se být autentizováni, ale dosud jsme neobdrželi potřebné uživatelské atributy. Zkontrolujte, zda poskytovatel vaší identity uvolňuje v rámci SSO pro %s pole pro jméno, příjmení a e-mailovou adresu, nebo informujte správce.';
$string['errorremoteuser'] = 'Vazba na remoteuser je povinná, pokud je usersuniquebyusername vypnutý';
$string['errorretryexceeded'] = 'Maximální počet pokusů překročen (%s) - na její straně musí být problém se službou identit.';
$string['institutionattribute'] = 'Atribut instituce (obsahuje "%s")';
$string['institutionregex'] = 'Ztotožněte dílčí řetězec se zkráceným jménem instituce';
$string['institutionvalue'] = 'Hodnota instituce pro kontrolu vůči atributu';
$string['link'] = 'Propojit účty';
$string['linkaccounts'] = 'Chcete propojit vzdálený účet %s s místním účtem %s?';
$string['login'] = 'Přihlášení SSO';
$string['loginlink'] = 'Povolit uživatelům odkazovat na vlastní účet';
$string['logintolink'] = 'Propojit místní účet %s se vzdáleným účtem.';
$string['logintolinkdesc'] = '<p><b>V tuto chvíli jste připojeni se vzdáleným uživatelem %s. Přihlašte se prosím se svým místním účtem pro jejich propojení nebo se zaregistrujte, pokud dosud na %s nemáte účet</b></p>';
$string['notusable'] = 'Instalujte prosím SP knihovny SimpleSAMLPHP';
$string['remoteuser'] = 'Ztotožněte atribut uživatelského jména se vzdáleným uživatelským jménem';
$string['samlfieldforemail'] = 'SSO pole pro e-mail';
$string['samlfieldforfirstname'] = 'SSO pole pro jméno';
$string['samlfieldforsurname'] = 'SSO pole pro příjmení';
$string['simplesamlphpconfig'] = 'Adresář pro konfiguraci SimpleSAMLPHP';
$string['simplesamlphplib'] = 'Adresář pro knihovny SimpleSAMLPHP';
$string['title'] = 'SAML';
$string['updateuserinfoonlogin'] = 'Aktualizovat uživatelské údaje při přihlášení';
$string['userattribute'] = 'Atribut uživatele';
$string['weautocreateusers'] = 'Automaticky zakládáme nové uživatele';
