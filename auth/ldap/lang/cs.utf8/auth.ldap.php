<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['cannotconnect'] = 'Nemohu se připojit k žádnému hostiteli LDAP';
$string['contexts'] = 'Kontexty';
$string['description'] = 'Ověřovat oproti LDAP serveru';
$string['distinguishedname'] = 'Pod jakým DN se má provést bind';
$string['hosturl'] = 'URL hostitele';
$string['ldapfieldforemail'] = 'LDAP pole s e-mailem';
$string['ldapfieldforfirstname'] = 'LDAP pole s křestním jménem';
$string['ldapfieldforpreferredname'] = 'LDAP pole se jménem';
$string['ldapfieldforstudentid'] = 'LDAP pole pro ID studenta';
$string['ldapfieldforsurname'] = 'LDAP pole s příjmením';
$string['ldapversion'] = 'Verze LDAP';
$string['notusable'] = 'Nainstalujte PHP rozšíření LDAP';
$string['password'] = 'Heslo';
$string['searchsubcontexts'] = 'Prohledávat i subkontexty';
$string['starttls'] = 'Šifrování TLS';
$string['title'] = 'LDAP';
$string['updateuserinfoonlogin'] = 'Aktualizovat uživatelské údaje při přihlášení';
$string['updateuserinfoonloginadnote'] = 'Poznámka: Povolením této volby můžete zábránit následnému přihlášování k Mahaře uživatelům a stránkám MS ActiveDirectory';
$string['userattribute'] = 'Uživatelské jméno hledat v atributu';
$string['usertype'] = 'Typ uživatelů';
$string['weautocreateusers'] = 'Automaticky zakládat profily v Mahaře';
