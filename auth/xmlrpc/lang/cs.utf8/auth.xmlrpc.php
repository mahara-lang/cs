<?php

defined('INTERNAL') || die();

$string['description'] = 'Přihlašování pomocí SSO z jiné aplikace';
$string['networkingdisabledonthissite'] = 'Podpora síťových služeb není povolena';
$string['networkservers'] = 'Zasíťované servery';
$string['notusable'] = 'Nainstalujte PHP rozšíření XMLRPC, Curl a OpenSSL';
$string['title'] = 'XMLRPC';
$string['youhaveloggedinfrom'] = 'Jste přihlášeni z <a href="%s">%s</a>';
