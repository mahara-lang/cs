<?php

defined('INTERNAL') || die();

$string['Post'] = 'Přidat vzkaz';
$string['addpostsuccess'] = 'Příspěvek byl úspěšně přidán.';
$string['backtoprofile'] = 'Zpět k profilu';
$string['delete'] = 'odstranit vzkaz';
$string['deletepost'] = 'Odstranit vzkaz';
$string['deletepostsuccess'] = 'Odstranění vzkazu proběhlo úspěšně';
$string['deletepostsure'] = 'Opravdu chcete provést tuto akci? Akce je nenávratná.';
$string['description'] = 'Zobrazuje oblast, kde vám mohou uživatelé nechávat vzkazy';
$string['makeyourpostprivate'] = 'Jedná se o soukromý vzkaz?';
$string['maxcharacters'] = 'Maximálně %s znaků';
$string['noposts'] = 'Žádné vzkazy k zobrazení';
$string['otherusertitle'] = 'Zeď uživatele %s';
$string['postsizelimit'] = 'Omezení velikosti vzkazu';
$string['postsizelimitdescription'] = 'Zde můžete omezit velikost nových vzkazů na zdi. Již vložené vzkazy nebudou zkráceny.';
$string['postsizelimitinvalid'] = 'Neplatné číslo';
$string['postsizelimitmaxcharacters'] = 'Maximální počet znaků';
$string['postsizelimittoosmall'] = 'Nemůže být menší než nula.';
$string['posttextrequired'] = 'Toto pole je povinné.';
$string['reply'] = 'odpovědět';
$string['sorrymaxcharacters'] = 'Bohužel, vzkazy nemohou být delší než %s znaků.';
$string['title'] = 'Zeď';
$string['viewwall'] = 'Zobrazit zeď';
$string['wall'] = 'Zeď';
$string['wholewall'] = 'Zobrazit celou zeď';
