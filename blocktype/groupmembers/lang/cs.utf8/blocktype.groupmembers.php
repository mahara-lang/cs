<?php

defined('INTERNAL') || die();

$string['Latest'] = 'Poslední';
$string['Random'] = 'Náhodný';
$string['defaulttitledescription'] = 'Výchozí název bude vygenerován pokud necháte pole s titulkem prázdné.';
$string['description'] = 'Zobrazit seznam členů této skupiny';
$string['options_numtoshow_desc'] = 'Počet členů, které chcete zobrazit.';
$string['options_numtoshow_title'] = 'Členové zobrazeni';
$string['options_order_desc'] = 'Můžete si zvolit zobrazení nejnovějších členů skupiny nebo jejich náhodný výběr.';
$string['options_order_title'] = 'Pořadí';
$string['show_all'] = 'Zobrazit všechny členy této skupiny...';
$string['title'] = 'Členové skupiny';
