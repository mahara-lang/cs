<?php

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'Výchozí název bude vygenerován pokud necháte pole s titulkem prázdné.';
$string['description'] = 'Zobrazit poslední příspěvky fóra pro skupinu';
$string['group'] = 'Skupina';
$string['nogroupstochoosefrom'] = 'Promiňte, žádné skupiny pro výběr z';
$string['poststoshow'] = 'Maximální počet příspěvků ke zobrazení';
$string['poststoshowdescription'] = 'Mezi 1 a 100';
$string['recentforumpostsforgroup'] = 'Poslední příspěvky fóra pro %s';
$string['title'] = 'Poslední příspěvky fóra';
