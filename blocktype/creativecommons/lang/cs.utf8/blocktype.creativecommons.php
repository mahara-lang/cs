<?php

defined('INTERNAL') || die();

$string['alttext'] = 'Licence Creative Commons';
$string['blockcontent'] = 'Obsah bloku';
$string['by'] = 'Uveďte autora';
$string['by-nc'] = 'Uveďte autora-Neužívejte dílo komerčně';
$string['by-nc-nd'] = 'Uveďte autora-Neužívejte dílo komerčně-Nezasahujte do díla';
$string['by-nc-sa'] = 'Uveďte autora-Nevyužívejte dílo komerčně-Zachovejte licenci';
$string['by-nd'] = 'Uveďte autora-Nezasahujte do díla';
$string['by-sa'] = 'Uveďte autora-Zachovejte licenci';
$string['cclicensename'] = 'Creative Commons %s 3.0 Unported';
$string['cclicensestatement'] = '%s od %s je licencováno pod licencí %s';
$string['config:noderivatives'] = 'Povolit úpravy vaší práce?';
$string['config:noncommercial'] = 'Povolit komerční využití vaší práce?';
$string['config:sharealike'] = 'Ano, pokud upravené dílo bude k dispozici pod stejnou (nebo slučitelnou) licencí';
$string['description'] = 'Připojit licenci Creative Commons do vašeho pohledu';
$string['otherpermissions'] = 'Oprávnění nad rámec této licence může udělit %s.';
$string['sealalttext'] = 'Tato licence je přijatelná pro Svobodná kulturní díla.';
$string['title'] = 'Licence Creative Commons';
