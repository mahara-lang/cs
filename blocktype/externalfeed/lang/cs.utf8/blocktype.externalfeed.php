<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['authpassword'] = 'Heslo HTTP';
$string['authpassworddesc'] = 'Pro přístup k tomuto kanálu (základní HTTP autentizace) je potřeba heslo (v případě potřeby)';
$string['authuser'] = 'Uživatelské jméno HTTP';
$string['authuserdesc'] = 'Pro přístup k tomuto kanálu (základní HTTP autentizace) je potřeba uživatelské jméno (v případě potřeby)';
$string['defaulttitledescription'] = 'Ponecháte-li prázdné, bude použit název kanálu';
$string['description'] = 'Čtečka informačních kanálů RSS nebo ATOM';
$string['feedlocation'] = 'Zdroj';
$string['feedlocationdesc'] = 'Adresa URL informačního kanálu RSS nebo ATOM';
$string['insecuresslmode'] = 'Nespolehlivý SSL režim';
$string['insecuresslmodedesc'] = 'Zakázat ověření SSL certifikátu. To se sice nedoporučuje, ale může být nezbytné, pokud ke kanál distribuován za použití neplatného nebo nedůvěryhodného certifikátu.';
$string['invalidfeed'] = 'Neplatný formát kanálu. Chybové hlášení zní: %s';
$string['invalidurl'] = 'Takové URL není platné. Můžete používat pouze adresy začínající http:// nebo https://';
$string['itemstoshow'] = 'Příspěvky k zobrazení';
$string['itemstoshowdescription'] = 'Mezi 1 a 20';
$string['lastupdatedon'] = 'Naposledy aktualizováno %s';
$string['showfeeditemsinfull'] = 'Plné znění příspěvků?';
$string['showfeeditemsinfulldesc'] = 'Zda se má zobrazovat pouze souhrn příspěvků ve zdroji nebo kompletní popis každého z nich';
$string['title'] = 'Webový kanál';
