<?php

defined('INTERNAL') || die();

$string['More'] = 'Více';
$string['defaulttitledescription'] = 'Výchozí název bude vygenerován pokud necháte pole s titulkem prázdné.';
$string['description'] = 'Zobrazit výběr z vašich nedávných zpráv doručené pošty';
$string['maxitems'] = 'Maximální počet zobrazených položek';
$string['maxitemsdescription'] = 'Mezi 1 a 100';
$string['messagetypes'] = 'Typy zpráv ke zobrazení';
$string['nomessages'] = 'Žádné zprávy';
$string['title'] = 'Má doručená pošta';
