<?php

defined('INTERNAL') || die();

$string['description'] = 'Zobrazuje seznam skupin, do nichž patříte';
$string['otherusertitle'] = 'Skupiny uživatele %s';
$string['title'] = 'Moje skupiny';
