<?php

defined('INTERNAL') || die();

$string['description'] = 'Zobrazuje dostupné pohledy na vaše portfolio';
$string['otherusertitle'] = 'Pohledy uživatele %s';
$string['title'] = 'Moje pohledy';
