<?php

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'Výchozí název bude vygenerován pokud necháte pole s titulkem prázdné.';
$string['description'] = 'Zobrazit pohledy sdílené se skupinou';
$string['displaygroupviews'] = 'Zobrazit pohledy skupiny?';
$string['displaygroupviewsdesc'] = 'Pohledy skupiny - pohledy vytvořené ve skupině';
$string['displaysharedviews'] = 'Zobrazit sdílené pohledy?';
$string['displaysharedviewsdesc'] = 'Sdílené pohledy - pohledy sdílené členy skupiny z jejich individuálních portfolií';
$string['title'] = 'Skupinové pohledy';
