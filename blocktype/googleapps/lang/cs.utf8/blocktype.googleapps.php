<?php

defined('INTERNAL') || die();

$string['appscodeorurl'] = 'Vložit kód nebo URL';
$string['appscodeorurldesc'] = 'Vložit kód nebo URL stránky, která je v Google Apps veřejně přístupná.';
$string['description'] = 'Vložit Google kalendář nebo dokumenty';
$string['height'] = 'Výška';
$string['title'] = 'Google Apps';
