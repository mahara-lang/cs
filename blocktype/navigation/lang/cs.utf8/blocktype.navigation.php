<?php

defined('INTERNAL') || die();

$string['collection'] = 'Sbírka';
$string['defaulttitledescription'] = 'Není-li zde uveden titulek, bude použit název sbírky.';
$string['description'] = 'Zobrazit jednoduchou navigaci u sbírky pohledů.';
$string['nocollections'] = 'Žádné sbírky pohledů. <a href="%scollection/edit.php?new=1">Vytvořte nějaké</a>.';
$string['title'] = 'Navigace';
