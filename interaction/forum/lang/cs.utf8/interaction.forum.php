<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['Body'] = 'Tělo';
$string['Close'] = 'Zavřít';
$string['Closed'] = 'Uzavřená';
$string['Count'] = 'Počet';
$string['Key'] = 'Klíč';
$string['Moderators'] = 'Moderátoři';
$string['Open'] = 'Otevřít';
$string['Order'] = 'Pořadí';
$string['Post'] = 'Poslat';
$string['Poster'] = 'Přispěvatel';
$string['Posts'] = 'Příspěvky';
$string['Reply'] = 'Odpovědět';
$string['Sticky'] = 'Trvalý';
$string['Subject'] = 'Předmět';
$string['Subscribe'] = 'Odebírat';
$string['Subscribed'] = 'Odebíráno';
$string['Topic'] = 'Téma';
$string['Topics'] = 'Témata';
$string['Unsticky'] = 'Není trvalé';
$string['Unsubscribe'] = 'Neodebírat';
$string['activetopicsdescription'] = 'Naposledy aktualizovaná témata ve vašich skupinách.';
$string['addpostsuccess'] = 'Příspěvek úspěšně přidán';
$string['addtitle'] = 'Přidat fórum';
$string['addtopic'] = 'Přidat téma';
$string['addtopicsuccess'] = 'Téma úspěšně přidáno';
$string['allposts'] = 'Všechny příspěvky';
$string['autosubscribeusers'] = 'Automaticky nastavovat odebírání';
$string['autosubscribeusersdescription'] = 'Mají členové skupiny automaticky odebírat příspěvky e-mailem?';
$string['cantaddposttoforum'] = 'Nemáte oprávnění přispívat do tohoto fóra';
$string['cantaddposttotopic'] = 'Nemáte oprávnění přispívat k tomuto tématu';
$string['cantaddtopic'] = 'Nemáte oprávnění přidávat témata do tohoto fóra';
$string['cantdeletepost'] = 'Nemáte oprávnění odstraňovat příspěvky z tohoto fóra';
$string['cantdeletethispost'] = 'Nemáte oprávnění odstranit tento příspěvek';
$string['cantdeletetopic'] = 'Nemáte oprávnění odstraňovat témata z tohoto fóra';
$string['canteditpost'] = 'Nemáte oprávnění upravovat tento příspěvek';
$string['cantedittopic'] = 'Nemáte oprávnění upravovat toto téma';
$string['cantfindforum'] = 'Nelze najít fórum id %s';
$string['cantfindpost'] = 'Nelze najít příspěvek id %s';
$string['cantfindtopic'] = 'Nelze najít téma id %s';
$string['cantviewforums'] = 'Nemáte oprávnění nahlížet do diskusních fór v této skupině';
$string['cantviewtopic'] = 'Nemáte oprávnění prohlížet si témata v tomto fóru';
$string['chooseanaction'] = 'Zvolte akci';
$string['clicksetsubject'] = 'Nastavit předmět';
$string['closeddescription'] = 'Na uzavřená témata mohou odpovídat pouze moderátoři a správci skupiny';
$string['closetopics'] = 'Zavřít nová témata';
$string['closetopicsdescription'] = 'Pokud je zaškrtnuto, budou všechna nová témata v tomto fóru uzavřena dle výchozího nastavení. Jen moderátoři a skupina správců může odpovídat na uzavřená témata.';
$string['createtopicusersdescription'] = 'Pokud je nastaveno na "Všichni členové skupiny", může kdokoliv zakládat nová témata a odpovídat do založených témat. Je-li nastaveno na "Pouze moderátoři a správci skupiny", mohou nové téma založit pouze moderátoři a správci, ale kdokoliv může přispívat do takto založené diskuse.';
$string['currentmoderators'] = 'Stávající moderátoři';
$string['defaultforumdescription'] = '%s fórum obecné diskuse';
$string['defaultforumtitle'] = 'Obecná diskuse';
$string['deleteforum'] = 'Odstranit fórum';
$string['deletepost'] = 'Odstranit příspěvek';
$string['deletepostsuccess'] = 'Příspěvek úspěšně odstraněn';
$string['deletepostsure'] = 'Jste si skutečně jisti, že to chcete udělat? Tuto akci nelze vrátit zpět.';
$string['deletetopic'] = 'Odstranit téma';
$string['deletetopicsuccess'] = 'Téma úspěšně odstraněno';
$string['deletetopicsure'] = 'Jste si skutečně jisti, že to chcete udělat? Tuto akci nelze vrátit zpět.';
$string['deletetopicvariable'] = 'Odstranit téma \'%s\'';
$string['editpost'] = 'Upravit příspěvek';
$string['editpostsuccess'] = 'Příspěvek úspěšně odstraněn';
$string['editstothispost'] = 'Úpravy tohoto příspěvku:';
$string['edittitle'] = 'Upravit fórum';
$string['edittopic'] = 'Upravit téma';
$string['edittopicsuccess'] = 'Téma úspěšně upraveno';
$string['forumname'] = 'Název fóra';
$string['forumposthtmltemplate'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s od %s</ strong><br />
%s</div><div style = "margin: 1em 0;">%s</div>

<div style="font-size: smaller; border-top: 1px solid #999;">

<p><a href="%s">Odpovědět na tento příspěvek on-line</a></p>

<p><a href="%s">Odhlásit se z této %s</a></p></div>';
$string['forumposttemplate'] = '%s od %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
Pro zobrazení příspěvku a pro vložení odpovědi přejděte na:
%s

Ke zrušení odebírání emailů z %s navštivte:
%s';
$string['forumsuccessfulsubscribe'] = 'Odebírání úspěšně nastaveno';
$string['forumsuccessfulunsubscribe'] = 'Odebírání úspěšně zrušeno';
$string['gotoforums'] = 'Jdi k fórům';
$string['groupadminlist'] = 'Správci skupiny:';
$string['groupadmins'] = 'Správci skupiny';
$string['indentflatindent'] = 'Žádné členění';
$string['indentfullindent'] = 'Plně rozšířit';
$string['indentmaxindent'] = 'Rozšířit na maximum';
$string['indentmode'] = 'Režim členění fóra';
$string['indentmodedescription'] = 'Určete, jak by měla být témata v tomto fóru členěna.';
$string['lastpost'] = 'Poslední příspěvek';
$string['latestforumposts'] = 'Poslední příspěvky ve fóru';
$string['maxindent'] = 'Maximální úroveň členění';
$string['maxindentdescription'] = 'Nastavit maximální úroveň členění pro téma. To platí pouze v případě, že režim členění byl nastaven na volbu Rozšířit na maximum.';
$string['moderatorsandgroupadminsonly'] = 'Pouze moderátoři a správci skupiny';
$string['moderatorsdescription'] = 'Moderátoři mohou upravovat a odstraňovat témata a příspěvky. Mohou rovněž otevírat a uzavírat témata diskuse nebo je nastavovat jako trvale otevřená';
$string['moderatorslist'] = 'Moderátoři:';
$string['name'] = 'Fórum';
$string['nameplural'] = 'Fóra';
$string['newforum'] = 'Nové fórum';
$string['newforumpostnotificationsubject'] = '%s: %s: %s';
$string['newforumpostnotificationsubjectline'] = '%s';
$string['newpost'] = 'Nový příspěvek';
$string['newtopic'] = 'Nové téma';
$string['noforumpostsyet'] = 'V této skupině zatím nejsou žádné příspěvky';
$string['noforums'] = 'V této skupině nejsou žádná fóra';
$string['notopics'] = 'V tomto fóru nejsou žádná témata diskuse';
$string['orderdescription'] = 'Zvolte, kde se má toto fórum zobrazit vzhledem k ostatním fórům';
$string['postaftertimeout'] = 'Odeslali jste změny po vypršení časového limitu %s minut. Vaše změny nebudou použity.';
$string['postbyuserwasdeleted'] = 'Příspěvek autora %s byl odstraněn';
$string['postdelay'] = 'Zpoždění příspěvku';
$string['postdelaydescription'] = 'Minimální doba (v minutách), které musí proběhnout, než může být nový příspěvek rozeslán účastníkům fóra. Autor příspěvku může provádět jeho úpravy během této doby.';
$string['postedin'] = '%s ve fóru %s';
$string['postreply'] = 'Odpověď na příspěvek';
$string['postsvariable'] = 'Příspěvky: %s';
$string['potentialmoderators'] = 'Moderátoři k dispozici';
$string['re'] = 'Re: %s';
$string['regulartopics'] = 'Pravidelná témata';
$string['replyforumpostnotificationsubject'] = 'Re: %s: %s: %s';
$string['replyforumpostnotificationsubjectline'] = 'Re: %s';
$string['replyto'] = 'Odpověď na:';
$string['stickydescription'] = 'Trvalá témata diskuse jsou zobrazena vždy nahoře na stránce';
$string['stickytopics'] = 'Trvalá témata';
$string['strftimerecentfullrelative'] = '%%v, %%k:%%M';
$string['strftimerecentrelative'] = '%%v, %%k:%%M';
$string['subscribetoforum'] = 'Odebírat příspěvky ve fóru e-mailem';
$string['subscribetotopic'] = 'Odebírat příspěvky v tomto tématu e-mailem';
$string['timeleftnotice'] = 'Zbývá vám %s minut pro dokončení úprav.';
$string['today'] = 'Dnes';
$string['topicclosedsuccess'] = 'Témata úspěšně uzavřena';
$string['topicisclosed'] = 'Toto téma je uzavřeno. Pouze moderátoři a správci skupiny mohou přidávat nové odpovědi.';
$string['topiclower'] = 'téma';
$string['topicopenedsuccess'] = 'Témata úspěšně otevřena';
$string['topicslower'] = 'témata';
$string['topicstickysuccess'] = 'Témata úspěšně nastavena jako trvalá';
$string['topicsubscribesuccess'] = 'Odebírání témat úspěšně nastaveno';
$string['topicsuccessfulunsubscribe'] = 'Odebírání témat zrušeno';
$string['topicunstickysuccess'] = 'Označení tématu jako trvalého úspěšně zrušeno';
$string['topicunsubscribesuccess'] = 'Odebírání témat úspěšně zrušeno';
$string['topicupdatefailed'] = 'Úprava témat selhala';
$string['typenewpost'] = 'Nový příspěvek ve fóru';
$string['unsubscribefromforum'] = 'Neodebírat příspěvky z tohoto fóra';
$string['unsubscribefromtopic'] = 'Neodebírat příspěvky z tohoto téma';
$string['updateselectedtopics'] = 'Aktualizovat vybraná témata';
$string['whocancreatetopics'] = 'Kdo může zakládat nová témata diskuse';
$string['yesterday'] = 'Včera';
$string['youarenotsubscribedtothisforum'] = 'Neodebíráte emaily z tohoto fóra';
$string['youarenotsubscribedtothistopic'] = 'Neodebíráte emaily z tohoto tématu diskuse';
$string['youcannotunsubscribeotherusers'] = 'Nemůžete rušit odebírání jiných uživatelů';
