<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['addauthority'] = 'Přidat autoritu';
$string['application'] = 'Aplikace';
$string['authloginmsg'] = 'Vložte zprávu, která se zobrazí uživatelům při pokusu o přihlášení se na tyto stránky pomocí přihlašovacího formuláře Mahary';
$string['authname'] = 'Jméno autority';
$string['cannotjumpasmasqueradeduser'] = 'Nemůžete přejít do jiné aplikace pokud se vydáváte za cizího uživatele.';
$string['cannotremove'] = 'Tento autentizační modul nelze odebrat, protože je jediný pro tuto instituci';
$string['cannotremoveinuse'] = 'Tento autentizační modul nelze odebrat, protože je používán některými uživateli. Nejprve je třeba upravit jejich nastavení.';
$string['cantretrievekey'] = 'Nepodařilo se získat veřejný klíč vzdáleného serveru.<br />Ujistěte se, že máte správně vyplněny pole Aplikace a Kořenová WWW adresa a že vzdálený server má zapnutu podporu síťových služeb.';
$string['changepasswordurl'] = 'URL ke změně hesla';
$string['duplicateremoteusername'] = 'Uživatelské jméno předané externí autentifikací je již používáno uživatelem %s. Externí uživatelská jména musí být unikátní v rámci všech autentifikačních metod.';
$string['duplicateremoteusernameformerror'] = 'Externí uživatelská jména musí být unikátní v rámci všech autentifikačních metod.';
$string['editauthority'] = 'Upravit autoritu';
$string['errnoauthinstances'] = 'Vypadá to, že nemáme nakonfigurovanou žádnou instanci autentizačního modulu pro hostitele %s';
$string['errnoxmlrpcinstances'] = 'Vypadá to, že nemáme nakonfigurovanou žádnou instanci autentizačního modulu XMLRPC pro hostitele %s';
$string['errnoxmlrpcuser'] = 'Je mi líto, ale nepodařilo se vás přihlásit. Možná, že vaše SSO přihlášení vypršelo. Vraťte se na partnerský server a klikněte na příslušný odkaz, který vás znovu přivede na tento portál. Dalším důvodem může být, že nemáte oprávnění přihlásit se na tento portál přes SSO. V případě nejasností se spojte s vaším správcem.';
$string['errnoxmlrpcuser1'] = 'Nebyli jsme schopni vás v tuto chvíli ověřit. To může být pravděpodobně proto, že: * Vaše relace SSO mohla vypršela. Vraťte se do jiné aplikace a klikněte na odkaz pro přihlášení do %s znovu. * Nemusíte mít povolen přístup skrze SSO na %s. Prosím, poraďte se se svým správcem, pokud si myslíte, že by to mělo být možné.';
$string['errnoxmlrpcwwwroot'] = 'Nemáme záznam pro žádného hostitele na %s';
$string['errorcertificateinvalidwwwroot'] = 'Tento certifikát byl vydán pro %s, ale vy se jej snažíte použít pro %s.';
$string['errorcouldnotgeneratenewsslkey'] = 'Nemohu vygenerovat nový SSL klíč. Jste si jistí že máte na tomto počítači nainstalováno OpenSSL a pro něj také příslušný modul PHP?';
$string['errornotvalidsslcertificate'] = 'Neplatný SSL certifikát';
$string['host'] = 'Jméno hostitele nebo adresa';
$string['hostwwwrootinuse'] = 'WWW root je již používán jinou institucí (%s)';
$string['ipaddress'] = 'IP adresa';
$string['name'] = 'Název stránek';
$string['noauthpluginconfigoptions'] = 'Tento modul nemá žádné konfigurační parametry';
$string['nodataforinstance'] = 'Nelze najít data pro instanci autentizačního modulu';
$string['parent'] = 'Nadřazená autoria';
$string['port'] = 'Číslo portu';
$string['protocol'] = 'Protokol';
$string['requiredfields'] = 'Požadovaná pole profilu';
$string['requiredfieldsset'] = 'Požadovaná pole profilu nastavena';
$string['saveinstitutiondetailsfirst'] = 'Nejprve uložte podrobnosti o instituci a poté nastavte autentizační moduly';
$string['shortname'] = 'Krátký název vašich stránek';
$string['ssodirection'] = 'směr SSO';
$string['theyautocreateusers'] = 'Oni automaticky vytvářejí uživatele';
$string['theyssoin'] = 'Jejich uživatelé se k nám mohou hlásit přes SSO';
$string['toomanytries'] = 'Překročili jste maximální počet pokusů o přihlášení. Tento účet byl uzamčen po dobu pěti minut.';
$string['unabletosigninviasso'] = 'Nelze se přihlásit přes SSO';
$string['updateuserinfoonlogin'] = 'Automaticky aktualizovat informace o uživateli';
$string['updateuserinfoonlogindescription'] = 'Znovu získat informace o uživateli ze vzdáleného serveru a aktualizovat lokální záznamy při každém přihlášení';
$string['weautocreateusers'] = 'My automaticky vytváříme uživatele';
$string['weimportcontent'] = 'My importujeme obsah (pouze z některých aplikací)';
$string['weimportcontentdescription'] = '(pouze z některých aplikací)';
$string['wessoout'] = 'Naši uživatelé se k nim mohou hlásit přes SSO';
$string['wwwroot'] = 'Kořenová WWW adresa';
$string['xmlrpccouldnotlogyouin'] = 'Je mi líto, ale nepodařilo se vás přihlásit :-(';
$string['xmlrpccouldnotlogyouindetail'] = 'Bohužel, vaše přihlášení se nezdařilo. Zkuste to, prosím, za chvíli znovu a pokud problém přetrvá, spojte se s vaším správcem.';
$string['xmlrpccouldnotlogyouindetail1'] = 'Je mi líto, ale nepodařilo se vás přihlásit do %s. Prosím, zkuste to zakrátko znovu. Pokud problém přetrvává, obraťte se na správce.';
$string['xmlrpcserverurl'] = 'URL adresa serveru XMLRPC';
