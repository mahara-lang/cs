<?php

defined('INTERNAL') || die();

$string['element.bytes.bytes'] = 'B';
$string['element.bytes.gigabytes'] = 'GB';
$string['element.bytes.invalidvalue'] = 'Hodnota musí být číselná';
$string['element.bytes.kilobytes'] = 'kB';
$string['element.bytes.megabytes'] = 'MB';
$string['element.calendar.invalidvalue'] = 'Neplatný datum/čas';
$string['element.date.monthnames'] = 'leden,únor,březen,duben,květen,červen,červenec,srpen,září,říjen,listopad,prosinec';
$string['element.date.notspecified'] = 'Neurčeno';
$string['element.date.or'] = 'nebo';
$string['element.expiry.days'] = 'dnů';
$string['element.expiry.months'] = 'měsíců';
$string['element.expiry.noenddate'] = 'nikdy';
$string['element.expiry.weeks'] = 'týdnů';
$string['element.expiry.years'] = 'roků';
$string['rule.before.before'] = 'Zadaná hodnota nemůže být větší než v poli "%s"';
$string['rule.email.email'] = 'Neplatná e-mailová adresa';
$string['rule.integer.integer'] = 'Musíte zadat celé číslo';
$string['rule.maxlength.maxlength'] = 'Můžete zadat nejvýše %d znaků';
$string['rule.maxvalue.maxvalue'] = 'Hodnota nemůže být větší než %d';
$string['rule.minlength.minlength'] = 'Musíte zadat nejméně %d znaků';
$string['rule.minvalue.minvalue'] = 'Nemůžete zadat hodnotu menší než %d';
$string['rule.regex.regex'] = 'Neplatný formát';
$string['rule.required.required'] = 'Povinné pole';
$string['rule.validateoptions.validateoptions'] = 'Neplatný výběr "%s"';
