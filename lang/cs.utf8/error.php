<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['accessdenied'] = 'Přístup odepřen';
$string['accessdeniedexception'] = 'Nemáte právo zobrazit tento pohled';
$string['apcstatoff'] = 'Váš server je spuštěn s hodnotou APC apc.stat = 0. Mahara nepodporuje tuto konfiguraci. Je třeba nastavit apc.stat = 1 v souboru php.ini. Pokud používáte sdílený hosting, je malá pravděpodobnost, že budete moci tuto změnu provést, proto se obraťte na svého poskytovatele hostingu. Možná byste měli uvažovat o přemístění na jiný počítač.';
$string['artefactnotfound'] = 'Položka portfolia s identifikátorem %s nebyla nalezena';
$string['artefactnotfoundmaybedeleted'] = 'Položka portfolia s identifikátorem %s nebyla nalezena. Možná již byla odstraněna';
$string['artefactnotinview'] = 'Položka portfolia %s není v pohledu %s';
$string['artefactonlyviewableinview'] = 'Položka portfolia tohoto typu je zobrazitelná jen v pohledu';
$string['artefactpluginmethodmissing'] = 'Modul %s musí implementovat metodu %s';
$string['artefacttypeclassmissing'] = 'Typy položek portfolia musí implementovat nějakou třídu. Chybí %s';
$string['artefacttypemismatch'] = 'Neshoda v položkách portfolia, pokoušíte se použít %s jako %s.';
$string['artefacttypenametaken'] = 'Typ položky portfolia %s je již zabrán modulem %s';
$string['blockconfigdatacalledfromset'] = 'Konfigurační data (configdata) by se neměly nastavovat přímo. Použijte PluginBlocktype::instance_config_save';
$string['blockinstancednotfound'] = 'Instance bloku s identifikátorem %s nebyl nalezen';
$string['blocktypelibmissing'] = 'V bloku %s modulu %s chybí knihovna lib.php';
$string['blocktypemissingconfigform'] = 'Blok %s musí implementovat instance_config_form';
$string['blocktypenametaken'] = 'Blok %s je již zabrán modulem %s';
$string['blocktypeprovidedbyartefactnotinstallable'] = 'Toto bude nainstalováno v průněhu instalace modulu %s';
$string['classmissing'] = 'Chybějící třída %s typu %s v modulu %s';
$string['couldnotmakedatadirectories'] = 'Z nějakého důvodu nemohly být některé z hlavních datových adresářů vytvořeny. K tomu by nemělo dojít, protože Mahara si ověřila, že má právo zápisu do datového adresáře. Zkontrolujte nastavení přístupových práv k datovému adresáři pro proces webového serveru.';
$string['curllibrarynotinstalled'] = 'Nemáte nainstalováno PHP rozšíření curl. Tato knihovna je potřebná pro integraci s Moodle a pro odebírání externích RSS kanálů. Ujistěte se, že je curl nainstalováno a povoleno v php.ini';
$string['datarootinsidedocroot'] = 'Váš datový adresář je nevhodně umístěn tak, že je dostupný přímo přes webové rozhraní. To je závažný bezpečnostní problém, protože případný útočník může získat přístup k aktuálním session a vydávat se za jiné uživatel nebo získat přístup k nezveřejněným souborům. Přesuňte datový adresář (dataroot) tak, aby neležel v adresáři dostupném přes URL (document root)';
$string['datarootnotwritable'] = 'Do nastaveného datového adresáře %s (dataroot) nelze zapisovat. Mahara potřebuje, aby proces webového serveru mohl do tohoto adresáře ukládat soubory session, soubory uživatelů';
$string['dbconnfailed'] = 'Mahara se nemůže připojit ke své databázi.
* Pokud jste běžný uživatel a právě jste pracovali s těmito stránkami, vyčkejte chvilku a akci opakujte
* Pokud jste správce stránek, zkontrolujte konfiguraci připojení k databázi a ujistěte se, že je databáze dostupná. Následuje popis chyby:';
$string['dbnotutf8'] = 'Vaše databáze není v kódování UTF-8. Mahara ukládá všechna data v UTF-8. Je třeba znovu založit databázi s korektně nastaveným kódováním.';
$string['dbversioncheckfailed'] = 'Mahara vyžaduje vyšší verzi vašeho databázového serveru. Momentálně máte %s %s, ale Mahara potřebuje alespoň verzi %s.';
$string['domextensionnotloaded'] = 'Konfigurace vašeho serveru neobsahuje rozšíření dom. Ten Mahara vyžaduje k tomu, aby mohla procházet XML data z různých zdrojů.';
$string['gdextensionnotloaded'] = 'Vaše PHP nemá podporu rozšíření GD. Mahara vyžaduje tuto knihovnu pro manipulaci s grafikou, např. změna rozměrů nahraných obrázků apod. Buď povolte rozšíření "gd" v php.ini nebo překompilujte PHP s příslušným nastavením.';
$string['gdfreetypenotloaded'] = 'Rozšíření GD neobsahuje podporu fontů Freetype. Tyto fonty jsou potřeba pro generování kontrolních CAPTCHA obrázků. Opravte konfiguraci PHP a gd.';
$string['gdlibrarylacksgifsupport'] = 'Instalovené rozšíření GD pro PHP nepodporuje vytváření a čtení formátu GIF. Pro ukládání obrázků GIF je potřeba jejich plná podpora.';
$string['gdlibrarylacksjpegsupport'] = 'Instalovené rozšíření GD pro PHP nepodporuje formát JPEG/JPG. Pro ukládání obrázků JPEG/JPG je potřeba jejich plná podpora.';
$string['gdlibrarylackspngsupport'] = 'Instalovené rozšíření GD pro PHP nepodporuje formát PNG. Pro ukládání obrázků PNG je potřeba jejich plná podpora.';
$string['interactioninstancenotfound'] = 'Instance aktivity s identifikátorem %s nenalezena';
$string['invaliddirection'] = 'Neplatný směr %s';
$string['invalidviewaction'] = 'Neplatná operace %s';
$string['jsonextensionnotloaded'] = 'Vaše PHP nemá podporu rozšíření JSON. Mahara vyžaduje tuto knihovnu pro AJAX komunikaci mezi prohlížečem a serverem. Buď povolte toto rozšíření v php.ini nebo překompilujte PHP s příslušným nastavením.';
$string['magicquotesgpc'] = 'Máte povoleno PHP nastavení magic_quotes_gpc, což je vážné bezpečnostní riziko. Mahara se pokusí pracovat i v tomto režimu, ale měli byste si opravit konfiguraci.';
$string['magicquotesruntime'] = 'Máte povoleno PHP nastavení magic_quotes_runtime, což je vážné bezpečnostní riziko. Mahara se pokusí pracovat i v tomto režimu, ale měli byste si opravit konfiguraci.';
$string['magicquotessybase'] = 'Máte povoleno PHP nastavení magic_quotes_sybase, což je vážné bezpečnostní riziko. Mahara se pokusí pracovat i v tomto režimu, ale měli byste si opravit konfiguraci.';
$string['missingparamblocktype'] = 'Nejprve vyberte typ bloku, který chcete přidat';
$string['missingparamcolumn'] = 'Chybí určení sloupce';
$string['missingparamid'] = 'Chybějící id';
$string['missingparamorder'] = 'Chybí určení pořadí';
$string['mysqldbextensionnotloaded'] = 'Vaše PHP nemá podporu MySQL. Mahara vyžaduje tuto knihovnu pro ukládání dat ve stejnojmenné  databázi. Buď povolte toto rozšíření v php.ini nebo překompilujte PHP s příslušným nastavením.';
$string['mysqlnotriggerprivilege'] = 'Mahara vyžaduje oprávnění k vytvoření databázových triggerů, ale nemůže tak činit. Ujistěte se, že byla udělena oprávnění pro triggery příslušnému uživateli v instalaci MySQL. Pokyny, jak to provést, naleznete na https://wiki.mahara.org/index.php/System_Administrator "s_Guide / Granting_Trigger_Privilege';
$string['nopasswordsaltset'] = 'Nebylo nastaveno heslo pro celý web (salt). Upravte svůj config.php a nastavte parametru "passwordsaltmain" nějakou rozumnou tajnou frázi.';
$string['noreplyaddressmissingorinvalid'] = 'Nastavení adresy noreply je buď prázdné nebo obsahuje neplatnou e-mailovou adresu. Zkontrolujte prosím konfiguraci e-mailu v <a href="%s">nastavení správy stránek</a>.';
$string['notartefactowner'] = 'Nejste vlastníkem této položky portfolia';
$string['notenoughsessionentropy'] = 'Hodnota nastavení vaší direktivy session.entropy_length je příliš malá. Měli byste ji v php.ini nastavit nejméně na 16, aby se mohlo zajistit, že generované ID relace bude dostatečně náhodné a nepředvídatelné.';
$string['notfound'] = 'Nenalezeno';
$string['notfoundexception'] = 'Stránka, kterou hledáte, nebyla nalezena';
$string['onlyoneblocktypeperview'] = 'Do pohledu nemůžete vložit více jak jeden blok typu %s';
$string['onlyoneprofileviewallowed'] = 'Je dovoleno mít pouze jeden pohled s profilem';
$string['openbasedirenabled'] = 'Váš server má v nastavení php povolené omezení v direktivě open_basedir.';
$string['openbasedirpaths'] = 'Mahara může otevřít pouze soubory v rámci uvedených cest: %s';
$string['openbasedirwarning'] = 'Některé požadavky na externí webové stránky mohou selhat před dokončením. Tomu by mohlo mimo jiné zamezit zastavení aktualizace určitých kanálů.';
$string['parameterexception'] = 'Chybí povinný parametr';
$string['passwordsaltweak'] = 'Vaše heslo pro celý web (salt) není dostatečně silné. Upravte svůj config.php a nastavte pro parametr "passwordsaltmain" delší tajnou frázi.';
$string['pgsqldbextensionnotloaded'] = 'Vaše PHP nemá podporu PgSQL. Mahara vyžaduje tuto knihovnu pro ukládání dat ve stejnojmenné  databázi. Buď povolte toto rozšíření v php.ini nebo překompilujte PHP s příslušným nastavením.';
$string['phpversion'] = 'Mahara nepojede na PHP verzi menší než %s. Buď upgradujte vaši instalaci PHP nebo Maharu nainstalujte na jiném serveru.';
$string['pleaseloginforjournals'] = 'Musíte se odhlásit a znovu přihlásit předtím než uvidíte všechny vaše blogy a příspěvky.';
$string['plpgsqlnotavailable'] = 'Jazyk PL/pgSQL není povolen ve vaší instalaci Postgres a Mahara ho povolit nemůže. Nainstalujte prosím PL/pqSQL pro vaší databázi ručně. Pro instrukce, jak na to se podívejte na https://wiki.mahara.org/index.php/System_Administrator\'s_Guide/Enabling_Plpgsql';
$string['postmaxlessthanuploadmax'] = 'Vaše nastavení PHP post_max_size nastavení (%s) je menší než upload_max_filesize nastavení (%s). Soubory než %s se neuloží bez zobrazení jakékoliv chyby. Obvykle bývá post_max_size mnohem větší, než upload_max_filesize.';
$string['registerglobals'] = 'Máte povoleno PHP nastavení register_globals, což je bezpečnostní riziko. Mahara se pokusí pracovat i v tomto režimu, ale měli byste si opravit konfiguraci.';
$string['safemodeon'] = 'Váš server pravděpodobně běží v režimu safe_mode. Maharu není možné provozovat v tomto režimu. Musíte jej vypnout v php.ini nebo v konfiguraci Apache. Pokud provozujete Maharu u externího poskytovatele, budete muset požádat jejich technickou podporu. Případně zvažte instalaci na jiném serveru.';
$string['sessionextensionnotloaded'] = 'aše PHP nemá podporu session. Mahara vyžaduje tuto knihovnu pro přihlašování uživatelů. Buď povolte toto rozšíření v php.ini nebo překompilujte PHP s příslušným nastavením.';
$string['smallpostmaxsize'] = 'Vaše nastavení PHP post_max_size nastavení (%s) je velmi malé. Soubory než %s se neuloží bez zobrazení jakékoliv chyby.';
$string['themenameinvalid'] = 'Motiv jménem "%s" obsahuje neplatné znaky.';
$string['timezoneidentifierunusable'] = 'PHP na vašem hostingu nevrací platnou hodnotu pro identifikátor časové zóny (%%z) - určité akce s tímto spojené, jako např. export LEAP2A neproběhnou korektně. %%z je kód PHP pro formátování datumu. Tento problém je obvykle dán omezením PHP běžícím na systému Windows.';
$string['unabletosetmultipleblogs'] = 'Povolování více blogů pro uživatele %s při kopírování pohledu %s selhalo. To lze nastavit ručně na stránce <a href="%s">nastavení</a>.';
$string['unknowndbtype'] = 'V konfiguraci je uveden neplatný typ databáze. Možné hodnoty jsou "postgres8" nebo "mysql5". Opravte nastavení v souboru config.php';
$string['unrecoverableerror'] = 'Vyskytla se neošetřená chyba. Pravděpodobně jste odhalili chybu v systému.';
$string['unrecoverableerrortitle'] = '%s - stránky nejsou dostupné';
$string['versionphpmissing'] = 'V modulu %s %s chybí soubor version.php';
$string['viewnotfound'] = 'Pohled s identifikátorem %s nebyl nalezen';
$string['viewnotfoundexceptionmessage'] = 'Pokoušíte se zobrazit neexistující pohled!';
$string['viewnotfoundexceptiontitle'] = 'Pohled nenalezen';
$string['wwwrootnothttps'] = 'Vámi nastavený wwwroot, %s, nemá HTTPS přístup. Nicméně, další nastavení (například sslproxy) pro vaši instalaci vyžadují, aby váš wwwroot byl přístupný prostřednictvím HTTPS. Prosím aktualizujte své nastavení wwwroot, aby bylo skrze HTTPS přístupné nebo opravte nesprávné nastavení.';
$string['xmlextensionnotloaded'] = 'Vaše PHP nemá podporu rozšíření %s. Mahara vyžaduje tuto knihovnu pro parsování XML dat z různých zdrojů. Buď povolte toto rozšíření v php.ini nebo překompilujte PHP s příslušným nastavením.';
$string['youcannotviewthisusersprofile'] = 'Nemůžete si prohlédnout profil tohoto uživatele.';
