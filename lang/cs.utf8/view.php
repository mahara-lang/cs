<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['15,70,15'] = 'Mnohem větší prostřední sloupec';
$string['20,20,20,20,20'] = 'Stejné šířky';
$string['20,30,30,20'] = 'Větší prostřední sloupce';
$string['25,25,25,25'] = 'Stejně široké';
$string['25,50,25'] = 'Větší prostřední sloupec';
$string['33,33,33'] = 'Stejně široké';
$string['33,67'] = 'Větší pravý sloupec';
$string['50,50'] = 'Stejně široké';
$string['67,33'] = 'Větší levý sloupec';
$string['Added'] = 'Přidáno';
$string['Browse'] = 'Procházet';
$string['Configure'] = 'Konfigurovat';
$string['Locked'] = 'Zamčený';
$string['Me'] = 'Má';
$string['Owner'] = 'Vlastník';
$string['Preview'] = 'Náhled';
$string['Search'] = 'Hledat';
$string['Submitted'] = 'Zasláno';
$string['Template'] = 'Šablona';
$string['Untitled'] = 'Bez názvu';
$string['View'] = 'Pohled';
$string['Views'] = 'Pohledy';
$string['access'] = 'Přístup';
$string['accessbetweendates2'] = 'Nikdo neuvidí tento pohled před %s nebo po %s';
$string['accessdates'] = 'Přístup datum / čas';
$string['accessfromdate2'] = 'Nikdo neuvidí tento pohled před %s';
$string['accesslist'] = 'Seznam sledování';
$string['accessuntildate2'] = 'Nikdo neuvidí tento pohled po %s';
$string['add'] = 'Přidat';
$string['addcolumn'] = 'Přidat sloupec';
$string['addedtowatchlist'] = 'Pohled byl přidán do seznamu sledovaných pohledů';
$string['addnewblockhere'] = 'Přidat nový blok';
$string['addtowatchlist'] = 'Sledovat pohled';
$string['addtutors'] = 'Přidat učitele';
$string['addusertogroup'] = 'Přidat tohoto uživatele do skupiny';
$string['allowcommentsonview'] = 'Pokud je zaškrtnuté, uživatelé mohou zanechat komentář.';
$string['allowcopying'] = 'Umožnit kopírování pohledu';
$string['allviews'] = 'Všechny pohledy';
$string['alreadyinwatchlist'] = 'Tento pohled je již na seznamu sledovaných pohledů';
$string['artefacts'] = 'Položky portofolia';
$string['artefactsinthisview'] = 'Položky v tomto pohledu';
$string['attachedfileaddedtofolder'] = 'Přiložený soubor %s byl uložen do vaší složky "%s".';
$string['attachment'] = 'Příloha';
$string['back'] = 'Zpět';
$string['blockconfigurationrenderingerror'] = 'Konfigurace selhala protože blok nemůže být vypsán.';
$string['blockcopypermission'] = 'Oprávnění kopírovat blok';
$string['blockcopypermissiondesc'] = 'Pokud dovolíte ostatním uživatelům, aby si kopírovali váš pohled, můžete si zvolit, jak se má kopírovat tento blok';
$string['blockinstanceconfiguredsuccessfully'] = 'Blok úspěšně nakonfigurován';
$string['blocksinstructionajax'] = 'Pomocí myši přetáhněte ikony jednotlivých položek vašeho portfolia pod tuto čáru. Tím je umístíte do bloků v tomto pohledu. Bloky poté můžete v rámci pohledu přesouvat na požadovanou pozici.';
$string['blocksintructionnoajax'] = 'Vyberte si blok a poté zvolte místo, kam se má ve vašem pohledu umístit. Bloky můžete přesouvat pomocí tlačítek v jejich záhlaví.';
$string['blocktitle'] = 'Název bloku';
$string['blocktypecategory.external'] = 'Externí obsah';
$string['blocktypecategory.fileimagevideo'] = 'Soubory, obrázky a videa';
$string['blocktypecategory.general'] = 'Obecné';
$string['by'] = '-';
$string['cantaddcolumn'] = 'Nemůžete přidat žádné další sloupce do tohoto pohledu';
$string['cantdeleteview'] = 'Nemůžete odstranit tento pohled';
$string['canteditdontown'] = 'Nemůžete upravovat tento pohled, protože nejste jeho vlastníci';
$string['canteditsubmitted'] = 'Tento pohled nemůžete momentálně upravovat, protože byl odevzdán k ohodnocení v rámci skupiny "%s". Musíte počkat, dokud jej váš učitel neuvolní.';
$string['cantremovecolumn'] = 'Nemůžete smazat poslední sloupec z tohoto pohledu';
$string['cantsubmitviewtogroup'] = 'Tento pohled nemůžete odevzdat k hodnocení v rámci této skupiny';
$string['changecolumnlayoutfailed'] = 'Nelze změnit rozvržení sloupců. Někdo jiný by mohl upravovat rozvržení současně. Zkuste to prosím později.';
$string['changemyviewlayout'] = 'Změnit rozvržení pohledu';
$string['changeviewlayout'] = 'Změnit rozvržení pohledu';
$string['changeviewtheme'] = 'Motiv, který jste si zvolili pro tento pohled vám již dále není k dispozici. Vyberte si prosím jiný motiv.';
$string['choosetemplategrouppagedescription'] = '<p>Zde jsou šablony pohledů, které si můžete jako člen skupiny zkopírovat a použít je jako výchozí bod pro svůj vlastní pohled. Pro náhled daného pohledu klikněte na jeho název. Jakmile najdete vhodný pohled, stiskněte tlačítko "Kopírovat pohled". Tím se vytvoří vaše vlastní kopie, kterou můžete dále upravovat.</p>

<p><strong>Poznámka:</strong> V rámci skupiny nelze kopírovat blogy ani příspěvky blogů.</p>';
$string['choosetemplateinstitutionpagedescription'] = '<p>Zde jsou šablony pohledů, které si můžete jako člen této instituce zkopírovat a použít je jako výchozí bod pro svůj vlastní pohled. Pro náhled daného pohledu klikněte na jeho název. Jakmile najdete vhodný pohled, stiskněte tlačítko "Kopírovat pohled". Tím se vytvoří vaše vlastní kopie, kterou můžete dále upravovat.</p>

<p><strong>Poznámka:</strong> V rámci instituce nelze kopírovat blogy ani příspěvky blogů.</p>';
$string['choosetemplatepageandcollectiondescription'] = '<p>Zde jsou šablony pohledů, které si můžete jako člen této instituce zkopírovat a použít je jako výchozí bod pro svůj vlastní pohled. Pro náhled daného pohledu klikněte na jeho název. Jakmile najdete vhodný pohled, stiskněte tlačítko "Kopírovat pohled". Tím se vytvoří vaše vlastní kopie, kterou můžete dále upravovat. Můžete také zkopírovat celou sbírku tak, že stisknete tlačítko "Kopírovat sbírku".';
$string['choosetemplatepagedescription'] = '<p>Zde je šablony pohledů, které si můžete zkopírovat a použít je jako výchozí bod pro svůj vlastní pohled. Pro náhled daného pohledu klikněte na jeho název. Jakmile najdete vhodný pohled, stiskněte tlačítko "Kopírovat pohled". Tím se vytvoří vaše vlastní kopie, kterou můžete dále upravovat.</p>';
$string['clickformoreinformation'] = 'Klikněte pro více informací a pro přidání komentáře';
$string['column'] = 'sloupec';
$string['columns'] = 'sloupce';
$string['complaint'] = 'Stížnost';
$string['configureblock'] = 'Konfigurovat blok %s';
$string['configurethisblock'] = 'Konfigurovat tento blok';
$string['confirmcancelcreatingview'] = 'Tvorba pohledu ještě není u konce. Opravdu chcete úpravy zrušit?';
$string['confirmdeleteblockinstance'] = 'Opravdu chcete odstranit tento blok?';
$string['copiedblocksandartefactsfromtemplate'] = 'Úspěšně zkopírováno %d bloků a %d položek portfolia z %s';
$string['copyaview'] = 'Kopírovat pohled';
$string['copyfornewgroups'] = 'Kopie pro nové skupiny';
$string['copyfornewgroupsdescription'] = 'Automaticky udělej kopii tohoto pohledu ve všech skupinách tohoto typu:';
$string['copyfornewmembers'] = 'Kopie pro nové členy';
$string['copyfornewmembersdescription'] = 'Automaticky udělej vlastní kopii tohoto pohledu pro všechny nové členy instituce %s.';
$string['copyfornewusers'] = 'Kopie pro nové uživatele';
$string['copyfornewusersdescription'] = 'Automaticky udělej vlastní kopii tohoto pohledu pro každého nového uživatele těchto stránek';
$string['copynewusergroupneedsloggedinaccess'] = 'Ke kopiím pro nové skupiny nebo nové uživatele musejí mít přístup všichni přihlášení uživatelé.';
$string['copythisview'] = 'Kopírovat tento pohled';
$string['copyview'] = 'Kopírovat pohled';
$string['copyvieworcollection'] = 'Kopírovat pohled nebo sbírku';
$string['createemptyview'] = 'Vytvořit prázdný pohled';
$string['createview'] = 'Vytvořit pohled';
$string['dashboard'] = 'Ovládací panel';
$string['dashboardviewtitle'] = 'Pohled ovládacího panelu';
$string['date'] = 'Datum';
$string['deletespecifiedview'] = 'Odstranit pohled "%s"';
$string['deletethisview'] = 'Odstranit tento pohled';
$string['deleteviewconfirm'] = 'Opravdu chcete odstranit tento pohled? Tato akce nemůže být vrácena.';
$string['deleteviewconfirm1'] = 'Opravdu chcete smazat tento pohled? Nelze to vrátit zpět.';
$string['deleteviewconfirmbackup'] = 'Prosím, zvažte vytvoření zálohy tohoto pohledu tak, že ho <a href="%sexport/" target="_blank">exportujete</a>.';
$string['deleteviewconfirmnote'] = '<p><strong>Poznámka:</strong> všechny bloky s obsahem, které byly přidány k pohledu nebudou smazány. Nicméně budou odstraněny jakékoliv zpětné vazby k danému pohledu vztažené. Zvažte prosím nejprve zálohu pohledu v podobě jeho exportu.</p>';
$string['deleteviewconfirmnote1'] = '<strong> POZNÁMKA:</strong>Všechny vaše soubory a záznamy blogu, které propojené na této stránce budou stále k dispozici.<br/>Nicméně, zpětná vazba umístěná v tomto pohledu bude smazána.';
$string['description'] = 'Popis pohledu';
$string['displayview'] = 'Zobrazit pohled';
$string['editaccess'] = 'Upravit přístup';
$string['editaccessinvalidviewset'] = 'Pokus o úpravu přístupu k neplatnému souboru pohledů a sbírek';
$string['editaccesspagedescription3'] = 'Ve výchozím nastavení jsou pohledy přístupné pouze vám. S ostatními je můžete sdílet přidáním patřičných přístupových pravidel. Až budete hotovi, přejděte dolů a klikněte na tlačítko Uložit pro pokračování.';
$string['editblockspagedescription'] = '<p>Na jednotlivých záložkách najdete ikony, které představují možné typy obsahu vašeho portfolia. Ikony přetáhněte do spodní části obrazovky do plochy vašeho pohledu. Pomocí ikony (?) získáte více informací.</p>';
$string['editcontent'] = 'Upravit obsah';
$string['editcontentandlayout'] = 'Upravit obsah a rozvržení';
$string['editlayout'] = 'Upravit rozvržení';
$string['editsecreturlaccess'] = 'Upravit tajnou adresu URL';
$string['editsecreturlsintable'] = '<b>Tajná URL</b> protože musí být generována samostatně. Pro nastavení tajných URL se vraťte na <a href="%s">seznam sbírek a pohledů</a>.';
$string['editthisview'] = 'Upravit tento pohled';
$string['edittitle'] = 'Upravit titulek';
$string['edittitleanddescription'] = 'Upravit titulek a popis';
$string['empty_block'] = 'Vyberte si položku portfolia ze seznamu vlevo, kterou sem chcete umístit';
$string['emptylabel'] = 'Klikněte sem pro vložení textu popisku';
$string['err.addblocktype'] = 'Nelze přidat blok do vašeho pohledu';
$string['err.addcolumn'] = 'Nelze přidat nový sloupec';
$string['err.changetheme'] = 'Nelze aktualizovat motiv';
$string['err.moveblockinstance'] = 'Nelze přesunout blok na určenou pozici';
$string['err.removeblockinstance'] = 'Nelze odstranit blok';
$string['err.removecolumn'] = 'Nelze odstranit sloupec';
$string['everyoneingroup'] = 'Všichni ve skupině';
$string['filescopiedfromviewtemplate'] = 'Zkopírované soubory z %s';
$string['forassessment'] = 'k hodnocení';
$string['friend'] = 'Přítel';
$string['friends'] = 'Přátelé';
$string['generatesecreturl'] = 'Vytvořit nové tajné URL pro %s';
$string['grouphomepage'] = 'Domovská stránka skupiny';
$string['grouphomepagedescription'] = 'Pohled domovské stránky skupiny je obsah, který se zobrazí na kartě "O této skupině"';
$string['grouphomepageviewtitle'] = 'Pohled domovské stránky skupiny';
$string['groups'] = 'Skupiny';
$string['groupviews'] = 'Pohledy skupiny';
$string['in'] = '-';
$string['institutionviews'] = 'Pohledy instituce';
$string['invalidcolumn'] = 'Sloupec %s mimo rozsah';
$string['inviteusertojoingroup'] = 'pozvat tohoto uživatele do skupiny';
$string['listedinpages'] = 'Uvedeno v pohledech';
$string['listviews'] = 'Seznam pohledů';
$string['lockedgroupviewdesc'] = 'Pokud zamknete tento pohled, jsou schopni ho upravit pouze správci skupin.';
$string['loggedin'] = 'Přihlášení uživatelé';
$string['moreinstitutions'] = 'Více institucí';
$string['moreoptions'] = 'Pokročilé možnosti';
$string['moveblockdown'] = 'Přesunout blok %s dolů';
$string['moveblockleft'] = 'Přesunout blok %s doleva';
$string['moveblockright'] = 'Přesunout blok %s doprava';
$string['moveblockup'] = 'Přesunout blok %s nahoru';
$string['movethisblockdown'] = 'Přesunout tento blok dolů';
$string['movethisblockleft'] = 'Přesunout tento blok doleva';
$string['movethisblockright'] = 'Přesunout tento blok doprava';
$string['movethisblockup'] = 'Přesunout tento blok nahoru';
$string['newsecreturl'] = 'Nová tajná adresa URL';
$string['newstartdatemustbebeforestopdate'] = 'Datum začátku pro "%s" musí být před koncovým datem';
$string['newstopdatecannotbeinpast'] = 'Koncové datum pro "%s" nemůže být v minulosti';
$string['next'] = 'Další';
$string['noaccesstoview'] = 'Nemáte oprávnění vidět tento pohled';
$string['noartefactstochoosefrom'] = 'Žádné položky na výběr';
$string['noblocks'] = 'Žádné bloky v této kategorii';
$string['nobodycanseethisview2'] = 'Pouze vy můžete vidět tento pohled';
$string['nocopyableviewsfound'] = 'Žádné pohledy ke kopírování';
$string['noownersfound'] = 'Vlastníci nenalezeni';
$string['nothemeselected'] = 'Motiv nevybrán';
$string['notifyadministrator'] = 'Informovat správce';
$string['notifyadministratorconfirm'] = 'Jste si jisti, že chcete nahlásit tuto stránku jako stránku obsahující nevhodný materiál?';
$string['notifysiteadministrator'] = 'Upozornit správce stránek';
$string['notifysiteadministratorconfirm'] = 'Jste si opravdu jisti, že chcete nahlásit tento pohled z důvodu nepřípustného obsahu?';
$string['notitle'] = 'Bez názvu';
$string['notobjectionable'] = 'Není problematické';
$string['noviewlayouts'] = 'Žádná rozvržení pro pohledy se %s sloupci';
$string['noviews'] = 'Žádné pohledy';
$string['numberofcolumns'] = 'Počet sloupců';
$string['nviews'] = 'Pole';
$string['otherusersandgroups'] = 'Sílet s ostatními uživateli a skupinami';
$string['overridingstartstopdate'] = 'Přenastavení data zpřístupnění a znepřístupnění';
$string['overridingstartstopdatesdescription'] = 'Pokud chcete, zde můžete nastavit datum zpřístupnění a/nebo datum znepřístupnění vašeho pohledu. Nikdo kromě vás nebude moci pohled vidět před datem zpřístupnění nebo po datu znepřístupnění, a to bez ohledu na ostatní nastavení přístupových práv na této stránce.';
$string['owner'] = 'vlastník';
$string['ownerformat'] = 'Formát zobrazení jména';
$string['ownerformatdescription'] = 'Jak se má zobrazovat vaše jméno lidem, kteří navštíví váš pohled';
$string['owners'] = 'vlastníci';
$string['portfolio'] = 'Portfolio';
$string['print'] = 'Vytisknout';
$string['profile'] = 'profil';
$string['profileicon'] = 'Ikona profilu';
$string['profilenotshared'] = 'Plný přístup k profilu tohoto uživatele je omezeno.';
$string['profileviewtitle'] = 'Uživatelský profil';
$string['public'] = 'Veřejnost';
$string['publicaccessnotallowed'] = 'Vaše instituce nebo správce webu zakázal veřejné stránky a tajné adresy URL. Jakékoliv tajné adresy URL, zde uvedené, jsou v současné době neaktivní.';
$string['reallyaddaccesstoemptyview'] = 'Váš pohled neobsahuje žádné bloky. Opravdu chcete dát uživatelům přístup k tomuto pohledu?';
$string['reallydeletesecreturl'] = 'Jste si jisti, že chcete smazat tuto adresu URL?';
$string['remove'] = 'Odstranit';
$string['removeblock'] = 'Odstranit tento blok';
$string['removecolumn'] = 'Odstranit tento sloupec';
$string['removedfromwatchlist'] = 'Pohled odebrán ze seznamu sledovaných pohledů';
$string['removefromwatchlist'] = 'Přestat sledovat tento pohled';
$string['removethisblock'] = 'Odstranit tento blok';
$string['reportobjectionablematerial'] = 'Nahlásit nevhodný obsah';
$string['reportsent'] = 'Vaše zpráva byla odeslána';
$string['retainviewrights1'] = 'Zachovat přístup pro prohlížení zkopírovaným pohledům nebo sbírkám';
$string['retainviewrightsdescription'] = 'Zaškrtněte toto políčko, pokud chcete zajistit váš přístup ke zobrazení kopií tohoto pohledu nebo sbírky, které vytvoří ostatní uživatelé. Tito uživatelé mohou přístup zrušit poté, co vytvoří jeho kopie. K pohledům zkopírovaným z kopie tohoto pohledu nebo sbírky mít přístup nebudete.';
$string['retainviewrightsgroupdescription'] = 'Zaškrtněte toto políčko, pokud chcete zajistit členům této skupiny přístup ke zobrazení kopií tohoto pohledu, které vytvoří ostatní uživatelé. Tito uživatelé mohou přístup zrušit poté, co vytvoří jeho kopie. K pohledům zkopírovaným z kopie tohoto pohledu nebo sbírky mít přístup nebudete.';
$string['searchowners'] = 'Prohledat vlastníky';
$string['searchviews'] = 'Prohledat pohledy';
$string['searchviewsbyowner'] = 'vyhledat pohledy podle vlastníka:';
$string['secreturldeleted'] = 'Vaše tajná adresa URL byla smazána.';
$string['secreturls'] = 'Tajné adresy URL';
$string['secreturlupdated'] = 'Tajná adresa URL aktualizována';
$string['selectaviewtocopy'] = 'Vyberte si pohled ke zkopírování:';
$string['share'] = 'Sdílet';
$string['sharedviews'] = 'Sdílené pohledy';
$string['sharedviewsdescription'] = 'Tato stránka obsahuje seznam naposledy upravených nebo komentovaných pohledů, které s vámi ostatní sdílí. Mohou tak činit přímo, sdílením s přáteli vlastníka nebo sdílením s jednou z vašich skupin.';
$string['sharedwith'] = 'Sdílet s';
$string['shareview'] = 'Sdílet pohled';
$string['sharewith'] = 'Sdílet s';
$string['sharewithmygroups'] = 'Sdílet s mými skupinami';
$string['sharewithmyinstitutions'] = 'Sdílet s mou institucí';
$string['sharewithusers'] = 'Sdílet s uživatelem';
$string['show'] = 'Ukázat';
$string['startdate'] = 'Datum zpřístupnění';
$string['stopdate'] = 'Datum znepřístupnění';
$string['submitaviewtogroup'] = 'Odeslat pohled do této skupiny';
$string['submittedforassessment'] = 'Odeslán k posouzení';
$string['submitthisviewto'] = 'Zaslat tento pohled k ohodnocení -';
$string['submitviewconfirm'] = 'Pokud odevzdáte "%s" do "%s" k ohodnocení, nebudete jej moci upravovat až do okamžiku, než jej váš učitel oznámkuje. Opravdu chcete nyní váš pohled odeslat?';
$string['submitviewtogroup'] = 'Zaslat "%s" do "%s k ohodnocení';
$string['success.addblocktype'] = 'Blok úspěšně přidán';
$string['success.addcolumn'] = 'Sloupec úspešně přidán';
$string['success.changetheme'] = 'Motiv byl úspěšně aktualizován';
$string['success.moveblockinstance'] = 'Blok úspěšně přesunut';
$string['success.removeblockinstance'] = 'Blok úspěšně odstraněn';
$string['success.removecolumn'] = 'Sloupec úspěšně odstraněn';
$string['tagsonly'] = 'Pouze štítky';
$string['templatedescription'] = 'Zaškrtněte, pokud chcete návštěvníkům vašeho pohledu dovolit, aby jej používali jako šablonu pro jejich vlastní pohledy.';
$string['templatedescriptionplural'] = 'Zaškrtněte, pokud chcete, aby lidé, kteří vidí vaše pohledy, mohli vytvářet jejich vlastní kopie spolu se všemi soubory a složkami, které obsahují.';
$string['thisviewmaybecopied'] = 'Kopírování povoleno';
$string['timeofsubmission'] = 'Doba podání';
$string['title'] = 'Název pohledu';
$string['titleanddescription'] = 'Název, popis, štítky';
$string['token'] = 'Tajná adresa URL';
$string['tutors'] = 'učitelé';
$string['unrecogniseddateformat'] = 'Neznámý formát datumu';
$string['updatedaccessfornumviews'] = 'Přístupová práva pro stránku (stránky) %d byla aktualizována';
$string['updatewatchlistfailed'] = 'Aktualizace seznamu sledovaných pohledů selhala';
$string['users'] = 'Uživatelé';
$string['view'] = 'pohled';
$string['viewaccesseditedsuccessfully'] = 'Nastavení přístupových práv úspěšně uloženo';
$string['viewauthor'] = 'od <a href="%s">%s</a>';
$string['viewcolumnspagedescription'] = 'Nejprve zvolte, do kolika sloupců chcete rozvrhnout obsah vašeho pohledu. Šířku sloupců budete moci změnit v dalším kroku.';
$string['viewcopywouldexceedquota'] = 'Kopírováním tohoto pohledu by mohlo dojít k překročení přidělené diskové kvóty.';
$string['viewcreatedsuccessfully'] = 'Pohled úspěšně vytvořen';
$string['viewdeleted'] = 'Pohled odstraněn';
$string['viewfilesdirdesc'] = 'Soubory ze zkopírovaných pohledů';
$string['viewfilesdirname'] = 'soubory_pohledu';
$string['viewinformationsaved'] = 'Informace o pohledu úspěšně zkopírovány';
$string['viewlayoutchanged'] = 'Rozvržení pohledu bylo změněno';
$string['viewlayoutpagedescription'] = 'Vyberte si, jak chcete rozmístit sloupce ve vašem pohledu.';
$string['viewname'] = 'Název pohledu';
$string['viewobjectionableunmark'] = 'Tento pohled nebo nějaký jeho obsah byl nahlášen jako závadný. Pokud je již závadný obsah odstraněn, můžete kliknout na tlačítko k odstranění tohoto oznámení a sdělit tuto skutečnost ostatním správcům.';
$string['views'] = 'pohledy';
$string['viewsavedsuccessfully'] = 'Pohled úspěšně uložen';
$string['viewscopiedfornewgroupsmustbecopyable'] = 'Musíte povolit kopírování pohledu, který má být automaticky kopírován do nových skupin.';
$string['viewscopiedfornewusersmustbecopyable'] = 'Musíte povolit kopírování pohledu, který má být automaticky kopírován novým uživatelům.';
$string['viewsownedbygroup'] = 'Pohledy vlastněné touto skupinou';
$string['viewssharedtogroup'] = 'Pohledy, které sdílíte s touto skupinou';
$string['viewssharedtogroupbyothers'] = 'Pohledy, které ostatní sdílí s touto skupinou';
$string['viewssubmittedtogroup'] = 'Pohledy zaslané do této skupiny';
$string['viewsubmitted'] = 'Pohled zaslán';
$string['viewsubmittedtogroup'] = 'Tento pohled byl zaslán <a href="%s">%s</a>';
$string['viewsubmittedtogroupon'] = 'Tento pohled byl zaslán <a href="%s">%s</a> na %s';
$string['viewswithretainviewrightsmustbecopyable'] = 'Musíte povolit kopírování předtím než budete moci nastavit zachování přístupu pohledu pro prohlížení.';
$string['viewtitleby'] = '%s - <a href="%s">%s</a>';
$string['viewunobjectionablebody'] = '%s se podíval na %s od %s a označil to jako již dále nezávadný materiál.';
$string['viewunobjectionablesubject'] = 'Pohled %s byl označen %s jako nezávadný materiál';
$string['viewvisitcount'] = '%d stránek návštěva z %s na %s';
$string['watchlistupdated'] = 'Seznam sledovaných pohledů byl aktualizován';
$string['whocanseethisview'] = 'Kdo může vidět tento pohled';
$string['youhavenoviews'] = 'Nemáte žádné pohledy';
$string['youhaventcreatedanyviewsyet'] = 'Dosud jste nevytvořili žádné pohledy.';
$string['youhaveoneview'] = 'Máte jeden pohled.';
$string['youhavesubmitted'] = 'Odeslali jste <a href="%s">%s</a> do této skupiny';
$string['youhavesubmittedon'] = 'Odeslali jste <a href="%s">%s</a> do této skupiny na %s';
$string['youhaveviews'] = 'Počet vašich pohledů je: %s.';
$string['100'] = 'Stejné šířky';
