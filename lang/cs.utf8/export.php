<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['Done'] = 'Hotovo';
$string['Export'] = 'Exportovat';
$string['Starting'] = 'Spustění';
$string['allmydata'] = 'Všechna má data';
$string['chooseanexportformat'] = 'Vyberte formát pro export';
$string['clicktopreview'] = 'Klepněte pro náhled';
$string['collectionstoexport'] = 'Sbírky k exportu';
$string['creatingzipfile'] = 'Vytvářím soubor typu ZIP';
$string['exportgeneratedsuccessfully'] = 'Export byl vygenerován úspěšně. %sKlepněte zde pro jeho stažení%s';
$string['exportgeneratedsuccessfullyjs'] = 'Export byl vygenerován úspěšně. %sPokračovat%s';
$string['exportingartefactplugindata'] = 'Exportovat data položek portfolia';
$string['exportingartefacts'] = 'Exportovat položky portfolia';
$string['exportingartefactsprogress'] = 'Exportuji položky portfolia: %s%s';
$string['exportingfooter'] = 'Exportuji zápatí';
$string['exportingviews'] = 'Exportuji pohledz';
$string['exportingviewsprogress'] = 'Exportuji pohledy: %s%s';
$string['exportpagedescription'] = 'Tento nástroj exportuje informace ze všech vašich portfolií a pohledů, ale neexportuje nastavení vašich stránek.';
$string['exportportfoliodescription'] = 'Tento nástroj exportuje všechny informace o vašem portfoliu a pohledy. Obsah nahraný nebo vytvořený ve skupinách nebo vaše uživatelská nastavení exportována nebudou.';
$string['exportyourportfolio'] = 'Exportovat vaše portfolio';
$string['generateexport'] = 'Generuji export';
$string['includefeedback'] = 'Zahrnout zpětnou vazbu uživatelů';
$string['includefeedbackdescription'] = 'HMTL export bude obsahovat všechny komentáře uživatelů.';
$string['justsomecollections'] = 'Jen některé z mých sbírek';
$string['justsomeviews'] = 'Jen některé z mých pohledů';
$string['noexportpluginsenabled'] = 'Tuto vlastnost nemůžete použít, protože žádné z rozšíření pro export nebylo správcem povoleno';
$string['nonexistentfile'] = 'Pokus o přidání neexistujícího souboru: \'%s\'';
$string['pleasewaitwhileyourexportisbeinggenerated'] = 'Počkejte prosím, zatímco je váš export generován...';
$string['reverseselection'] = 'Obrátit výběr';
$string['selectall'] = 'Vybrat vše';
$string['setupcomplete'] = 'Nastavení kompletní';
$string['unabletoexportportfoliousingoptions'] = 'Nelze generovat export s použitím zvolených možností';
$string['unabletogenerateexport'] = 'Nelze generovat export';
$string['viewstoexport'] = 'Pohledy na export';
$string['whatdoyouwanttoexport'] = 'Co chcete exportovat?';
$string['writingfiles'] = 'Zapisuji soubory';
$string['youarehere'] = 'Nacházíte se zde';
$string['youmustselectatleastonecollectiontoexport'] = 'Musíte vybrat alespoň jednu sbírku pro export';
$string['youmustselectatleastoneviewtoexport'] = 'Musíte vybrat alespoň jeden pohled pro export';
$string['zipnotinstalled'] = 'Váš systém neobsahuje příkaz zip. Nainstalujte prosím zip k povolení této vlastnosti.';
