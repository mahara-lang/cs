<?php

defined('INTERNAL') || die();

$string['aboutdefaultcontent'] = '<h2>O těchto stránkách</p>

<p>Tyto stránky běží na systému <a href="http://mahara.org">Mahara</a>. Mahara vznikla v roce 2006 jako výsledek společného úsilí několika institucí na Novém Zélandu: Tertiary Education Commission\'s e-learning Collaborative Development Fund (eCDF), Massey University, Auckland University of Technology, The Open Polytechnic of New Zealand a Victoria University of Wellington.</p>

<p>Mahara je systém, který integruje nástroje pro tvorbu e-portfolia, weblogu, životopisu, budování sociálních sítí a online komunit. Mahara je navržena tak, aby si její uživatelé mohli vytvářet osobní i profesní vzdělávací prostředí.</p>

<p>Slovo mahara znamená "myslet" nebo "myšlenka" v jazyce původních obyvatel Te Reo Maori. Reprezentuje záměr svých tvůrců vytvořit prostředí zaměřené na celoživotní vzdělávání a osobnostní rozvoj uživatelů a zároveň přesvědčení, že podobnou technologii nelze vyvinout bez zohlednění řady pedagogických aspektů.</p>

<p>Mahara je poskytována zdarma jako software s otevřeným kódem (open source) a je možno ji dále modifikovat a šířit za podmínek daných všeobecnou veřejnou licencí GNU GPL.</p>

<p>V případě dotazů nás neváhejte <a href="contact.php">kontaktovat</a>.</p>
';
$string['homedefaultcontent'] = '<h2>Vítejte na těchto stránkách</h2>

<p>Tyto stránky běží na systému <a href="http://mahara.org">Mahara</a>. Mahara je systém, který integruje nástroje pro tvorbu e-portfolia, weblogu, životopisu, budování sociálních sítí a online komunit. Mahara je navržena tak, aby si její uživatelé mohli vytvářet osobní i profesní vzdělávací prostředí.</p>

<p>Přečtěte si více <a href="about.php">o těchto stránkách</a>. V případě dotazů nás neváhejte <a href="contact.php">kontaktovat</a>.</p>';
$string['loggedouthomedefaultcontent'] = '<h2>Vítejte na těchto stránkách</h2>

<p>Tyto stránky běží na systému <a href="http://mahara.org">Mahara</a>. Mahara je systém, který integruje nástroje pro tvorbu e-portfolia, weblogu, životopisu, budování sociálních sítí a online komunit. Mahara je navržena tak, aby si její uživatelé mohli vytvářet osobní i profesní vzdělávací prostředí.</p>

<p>Přečtěte si více <a href="about.php">o těchto stránkách</a>. V případě dotazů nás neváhejte <a href="contact.php">kontaktovat</a>.</p>';
$string['privacydefaultcontent'] = '<h2>Prohlášení o ochraně osobních údajů</h2>';
$string['termsandconditionsdefaultcontent'] = '<h2>Podmínky používání těchto stránek</h2>';
$string['uploadcopyrightdefaultcontent'] = 'Tímto prohlašuji, že soubor, který ukládám na server, jsem vytvořil já sám/sama nebo jsme oprávněn/-a jej reprodukovat a dále šířit. Zároveň prohlašuji, že nahráním tohoto souboru neporušuji platné zákony týkající se ochrany autorských práv ani další ujednání vztahující se k tomuto souboru. Obsah souboru nesmí být v rozporu s podmínkami používání těchto stránek.';
