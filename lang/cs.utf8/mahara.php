<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['All'] = 'Vše';
$string['Artefact'] = 'Položka';
$string['Artefacts'] = 'Položky';
$string['Close'] = 'Zavřít';
$string['Content'] = 'Obsah';
$string['Copyof'] = 'Kopie - %s';
$string['Created'] = 'Vytvořeno';
$string['Failed'] = 'Selhalo';
$string['From'] = 'Od';
$string['Help'] = 'Nápověda';
$string['Hide'] = 'Schovat';
$string['Invitations'] = 'Pozvánky';
$string['Memberships'] = 'Členství';
$string['Organise'] = 'Organizovat';
$string['Permissions'] = 'Oprávnění';
$string['Query'] = 'Dotaz';
$string['Requests'] = 'Požadavky';
$string['Results'] = 'Výsledky';
$string['Site'] = 'Stránky';
$string['Tag'] = 'Štítek';
$string['Title'] = 'Titulek';
$string['To'] = 'Do';
$string['Total'] = 'Celkem';
$string['Updated'] = 'Aktualizováno';
$string['Visibility'] = 'Viditelnost';
$string['Visits'] = 'Navštíveno';
$string['about'] = 'O těchto stránkách';
$string['accept'] = 'Souhlasit';
$string['accessforbiddentoadminsection'] = 'Nemáte přístup k administraci stránek';
$string['accesstotallydenied_institutionsuspended'] = 'Vaše instituce %s byla pozastavena. Dokud nebude její činnost znovu obnovena, není možné se k %s přihlásit. Prosím kontaktujte svou instituci pro další pomoc.';
$string['account'] = 'Můj účet';
$string['accountcreated'] = '%s: Nový účet';
$string['accountcreatedchangepasswordhtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p>

<p>Na stránkách <a href="%s">%s</a> byl pro vás založen nový uživatelský účet. Zde jsou uvedeny vaše přihlašovací údaje:</p>
<ul>
<li><strong>Uživatelské jméno:</strong> %s</li>
<li><strong>Heslo:</strong> %s</li>
</ul>
<p>Poté, co se poprvé přihlásíte, budete vyzváni ke změně tohoto hesla.</p>

<p>Navštivte <a href="%s">%s</a> a začněte budovat své e-portfolio!</p>

<p>S pozdravem, správce stránek %s</p>';
$string['accountcreatedchangepasswordtext'] = 'Toto je automaticky generovaný e-mail pro: %s

Na stránkách %s byl pro vás založen nový uživatelský účet. Zde jsou uvedeny vaše přihlašovací údaje:

Uživatelské jméno: %s
Heslo: %s

Poté, co se poprvé přihlásíte, budete vyzváni ke změně tohoto hesla.
Navštivte nyní %s a začněte budovat své e-portfolio!

S pozdravem, správce stránek %s';
$string['accountcreatedhtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p>

<p>Na stránkách <a href="%s">%s</a> byl pro vás založen nový uživatelský účet. Zde jsou uvedeny vaše přihlašovací údaje:</p>
<ul>
<li><strong>Uživatelské jméno:</strong> %s</li>
<li><strong>Heslo:</strong> %s</li>
</ul>
<p>Navštivte <a href="%s">%s</a> a začněte budovat své e-portfolio!</p>

<p>S pozdravem, správce stránek %s</p>';
$string['accountcreatedtext'] = 'Toto je automaticky generovaný e-mail pro: %s

Na stránkách %s byl pro vás založen nový uživatelský účet. Zde jsou uvedeny vaše přihlašovací údaje:

Uživatelské jméno: %s
Heslo: %s

Navštivte nyní %s a začněte budovat své e-portfolio!

S pozdravem, správce stránek %s';
$string['accountdeleted'] = 'Bohužel, váš účet byl odstraněn';
$string['accountexpired'] = 'Bohužel, platnost vašeho účtu vypršela';
$string['accountexpirywarning'] = 'Upozornění na vypršení platnosti';
$string['accountexpirywarninghtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p>

<p>Platnost vašeho účtu na stránkách %s vyprší během %s.</p>

<p>Doporučujeme vám uložit si obsah vašeho portfolia pomocí nástroje pro export. Návod k používání tohoto nástroje jsou k dispozici v uživatelské dokumentaci.</p>

<p>Pokud si přejete prodloužit platnost vašeho účtu nebo máte dotaz k uvedeným pokynům, neváhejte <a href="%s">nás kontaktovat</a>.</p>

<p>S pozdravem, %s, správce stránek</p>';
$string['accountexpirywarningtext'] = 'Toto je automaticky generovaný e-mail pro: %s

Platnost vašeho účtu na stránkách %s vyprší během %s.

Doporučujeme vám uložit si obsah vašeho portfolia pomocí nástroje pro export. Návod k používání tohoto nástroje jsou k dispozici v uživatelské dokumentaci.

Pokud si přejete prodloužit platnost vašeho účtu nebo máte dotaz k uvedeným pokynům, neváhejte nás kontaktovat: %s.

S pozdravem, %s, správce stránek';
$string['accountinactive'] = 'Je mi líto, váš účet není momentálně aktivní';
$string['accountinactivewarning'] = 'Upozornění - deaktivace účtu';
$string['accountinactivewarninghtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p>

<p>Váš účet na stránkách %s bude deaktivován během %s.</p>

<p>S neaktivním uživatelským účtem se nebudete moci na stránky přihlásit. Reaktivovat účet může pouze váš správce.</p>

<p>Chcete-li předejít deaktivaci vašeho účtu, přihlaste se na uvedených stránkách.</p>

<p>S pozdravem, %s, správce stránek</p>';
$string['accountinactivewarningtext'] = 'Toto je automaticky generovaný e-mail pro: %s

Váš účet na stránkách %s bude deaktivován během %s.

S neaktivním uživatelským účtem se nebudete moci na stránky přihlásit. Reaktivovat účet může pouze váš správce.

Chcete-li předejít deaktivaci vašeho účtu, přihlaste se na uvedených stránkách.

S pozdravem, %s, správce stránek';
$string['accountprefs'] = 'Předvolby';
$string['accountsuspended'] = 'Váš účet byl suspendován ke dni %s. Důvod suspendování je: <blockquote>%s</blockquote>';
$string['activityprefs'] = 'Předvolby upozorňování';
$string['add'] = 'Přidat';
$string['addemail'] = 'Přidat e-mailovou adresu';
$string['adminofinstitutions'] = 'Správce %s';
$string['adminphpuploaderror'] = 'Chyba při nahrávání souboru byla pravděpodobně způsobená konfigurací vašeho serveru.';
$string['after'] = 'po';
$string['allonline'] = 'Zobrazit všechny připojené uživatele';
$string['allowpublicaccess'] = 'Povolit přístup i nepřihlášeným uživatelům';
$string['alltags'] = 'Všechny štítky';
$string['allusers'] = 'Všichni uživatelé';
$string['alphabet'] = 'A,Á,B,C,Č,D,Ď,E,É,Ě,F,G,H,I,Í,J,K,L,M,N,Ň,O,Ó,P,Q,R,Ř,S,Š,T,Ť,U,Ú,Ů,V,W,X,Y,Ý,Z,Ž';
$string['applychanges'] = 'Uložit změny';
$string['artefact'] = 'položka portfolia';
$string['artefactnotfound'] = 'Položka portfolia s identifikátorem %s nenalezena';
$string['artefactnotpublishable'] = 'Položka portfolia %s nebyla zveřejněna v pohledu %s';
$string['artefactnotrendered'] = 'Nevykreslená položka portfolia';
$string['at'] = '-';
$string['attachment'] = 'Příloha';
$string['back'] = 'Zpět';
$string['backto'] = 'Zpět do %s';
$string['before'] = 'před';
$string['belongingto'] = 'Patří k:';
$string['blacklisteddomaininurl'] = 'URL v tomto poli obsahuje doménu %s umístěnou na černé listině.';
$string['bytes'] = 'bajtů';
$string['cancel'] = 'Zrušit';
$string['cancelrequest'] = 'Zrušit požadavek';
$string['cannotremovedefaultemail'] = 'Nelze odstranit primární e-mailovou adresu';
$string['cantchangepassword'] = 'Bohužel, pomocí tohoto rozhraní si nemůžete změnit vaše heslo. Použijte rozhraní vaší instituce.';
$string['change'] = 'Změnit';
$string['changepassword'] = 'Změna hesla';
$string['changepasswordinfo'] = 'Před pokračováním si musíte změnit vaše stávající heslo.';
$string['chooseinstitution'] = 'Vyberte vaši instituci';
$string['choosetheme'] = 'Vyberte vzhled...';
$string['chooseusernamepassword'] = 'Vyberte si své uživatelské jméno a heslo';
$string['chooseusernamepasswordinfo'] = 'Pořebujete uživatelské jméno a heslo k přihlášení k %s. Prosím zvolte je nyní.';
$string['clambroken'] = 'Na těchto stránkách se používá antivirová kontrola nahrávaných souborů, ale objevila se chyba v konfiguraci této služby. Váš soubor se NEPODAŘILO nahrát. Váš správce byl na tuto událost upozorněn e-mailem. Pokuste se nahrát soubor později.';
$string['clamdeletedfile'] = 'Soubor byl odstraněn';
$string['clamdeletedfilefailed'] = 'Soubor nemohl být odstraněn';
$string['clamemailsubject'] = '%s :: upozornění Clam AV';
$string['clamfailed'] = 'Nepodařilo se spustit Clam AV. Vrácené chybové hlášení zní: %s. Následuje výstup programu Clam:';
$string['clamlost'] = 'U nahrávaných souborů se má provádět antivirová kontrola, ale nastavená cesta k programu Clam AV (%s) není platná.';
$string['clammovedfile'] = 'Soubor byl přesunut do karantény.';
$string['clamunknownerror'] = 'Neznámá chyba programu clam.';
$string['collapse'] = 'Sbalit';
$string['complete'] = 'Hotovo';
$string['config'] = 'Konfigurovat';
$string['confirmdeletetag'] = 'Jste si skutečně jistí, že chcete vymazat tento štítek ze všeho ve vašem portfoliu?';
$string['confirminvitation'] = 'Potvrdit pozvání';
$string['confirmpassword'] = 'Potvrdit heslo';
$string['contactus'] = 'Kontakt';
$string['controlyourprivacylinked'] = 'Ovládejte své <a href="%s">soukromí</a>';
$string['cookiesnotenabled'] = 'Pro přihlášení se na těchto stránkách se používají cookies. Váš prohlížeč buď nemá cookies povoleny, nebo blokuje cookies zaslané z tohoto serveru.';
$string['couldnotgethelp'] = 'Vyskytla se chyba při načítání stránky s nápovědou';
$string['country.ad'] = 'Andorra';
$string['country.ae'] = 'Spojené arabské emiráty';
$string['country.af'] = 'Afghánistán';
$string['country.ag'] = 'Antigua a Barbuda';
$string['country.ai'] = 'Anguilla';
$string['country.al'] = 'Albánie';
$string['country.am'] = 'Arménie';
$string['country.an'] = 'Nizozemské Antily';
$string['country.ao'] = 'Angola';
$string['country.aq'] = 'Antarktida';
$string['country.ar'] = 'Argentina';
$string['country.as'] = 'Americká Samoa';
$string['country.at'] = 'Rakousko';
$string['country.au'] = 'Austrálie';
$string['country.aw'] = 'Aruba';
$string['country.ax'] = 'Ålandy';
$string['country.az'] = 'Ázerbájdžán';
$string['country.ba'] = 'Bosna a Hercegovina';
$string['country.bb'] = 'Barbados';
$string['country.bd'] = 'Bangladéš';
$string['country.be'] = 'Belgie';
$string['country.bf'] = 'Burkina Faso';
$string['country.bg'] = 'Bulharsko';
$string['country.bh'] = 'Bahrajn';
$string['country.bi'] = 'Burundi';
$string['country.bj'] = 'Benin';
$string['country.bm'] = 'Bermudy';
$string['country.bn'] = 'Brunej';
$string['country.bo'] = 'Bolívie';
$string['country.br'] = 'Brazílie';
$string['country.bs'] = 'Bahamy';
$string['country.bt'] = 'Bhútán';
$string['country.bv'] = 'Bouvetův ostrov';
$string['country.bw'] = 'Botswana';
$string['country.by'] = 'Bělorusko';
$string['country.bz'] = 'Belize';
$string['country.ca'] = 'Kanada';
$string['country.cc'] = 'Kokosové ostrovy';
$string['country.cd'] = 'Demokratická republika Kongo';
$string['country.cf'] = 'Středoafrická republika';
$string['country.cg'] = 'Kongo';
$string['country.ch'] = 'Švýcarsko';
$string['country.ci'] = 'Pobřeží slonoviny';
$string['country.ck'] = 'Cookovy ostrovy';
$string['country.cl'] = 'Chile';
$string['country.cm'] = 'Kamerun';
$string['country.cn'] = 'Čína';
$string['country.co'] = 'Kolumbie';
$string['country.cr'] = 'Kostarika';
$string['country.cs'] = 'Srbsko a Černá Hora';
$string['country.cu'] = 'Kuba';
$string['country.cv'] = 'Kapverdy';
$string['country.cx'] = 'Vánoční ostrov';
$string['country.cy'] = 'Kypr';
$string['country.cz'] = 'Česká republika';
$string['country.de'] = 'Německo';
$string['country.dj'] = 'Džibutsko';
$string['country.dk'] = 'Dánsko';
$string['country.dm'] = 'Dominika';
$string['country.do'] = 'Dominikánská republika';
$string['country.dz'] = 'Alžírsko';
$string['country.ec'] = 'Ekvádor';
$string['country.ee'] = 'Estonsko';
$string['country.eg'] = 'Egypt';
$string['country.eh'] = 'Západní Sahara';
$string['country.er'] = 'Eritrea';
$string['country.es'] = 'Španělsko';
$string['country.et'] = 'Etiopie';
$string['country.fi'] = 'Finsko';
$string['country.fj'] = 'Fidži';
$string['country.fk'] = 'Falklandy (Malvíny)';
$string['country.fm'] = 'Mikronésie';
$string['country.fo'] = 'Faerské ostrovy';
$string['country.fr'] = 'Francie';
$string['country.ga'] = 'Gabun';
$string['country.gb'] = 'Velká Británie';
$string['country.gd'] = 'Grenada';
$string['country.ge'] = 'Georgia';
$string['country.gf'] = 'Francouzská Guyana';
$string['country.gg'] = 'Guernsey';
$string['country.gh'] = 'Ghana';
$string['country.gi'] = 'Gibraltar';
$string['country.gl'] = 'Grónsko';
$string['country.gm'] = 'Gambie';
$string['country.gn'] = 'Guinea';
$string['country.gp'] = 'Guadeloupe';
$string['country.gq'] = 'Rovníková Guinea';
$string['country.gr'] = 'Řecko';
$string['country.gs'] = 'Jižní Georgie a Jižní Sandwichovy ostrovy';
$string['country.gt'] = 'Guatemala';
$string['country.gu'] = 'Guam';
$string['country.gw'] = 'Guinea-Bissau';
$string['country.gy'] = 'Guyana';
$string['country.hk'] = 'Hongkong';
$string['country.hm'] = 'Heardův ostrov a McDonaldovy ostrovy';
$string['country.hn'] = 'Honduras';
$string['country.hr'] = 'Chorvatsko';
$string['country.ht'] = 'Haiti';
$string['country.hu'] = 'Maďarsko';
$string['country.id'] = 'Indonésie';
$string['country.ie'] = 'Irsko';
$string['country.il'] = 'Izrael';
$string['country.im'] = 'Ostrov Man';
$string['country.in'] = 'Indie';
$string['country.io'] = 'Britské indickooceánské území';
$string['country.iq'] = 'Irák';
$string['country.ir'] = 'Írán';
$string['country.is'] = 'Island';
$string['country.it'] = 'Itálie';
$string['country.je'] = 'Jersey';
$string['country.jm'] = 'Jamajka';
$string['country.jo'] = 'Jordánsko';
$string['country.jp'] = 'Japonsko';
$string['country.ke'] = 'Keňa';
$string['country.kg'] = 'Kyrgyzstán';
$string['country.kh'] = 'Kambodža';
$string['country.ki'] = 'Kiribati';
$string['country.km'] = 'Komory';
$string['country.kn'] = 'Svatý Kryštof a Nevis';
$string['country.kp'] = 'Korejská lidově demokratická republika';
$string['country.kr'] = 'Korea';
$string['country.kw'] = 'Kuvajt';
$string['country.ky'] = 'Kajmanské ostrovy';
$string['country.kz'] = 'Kazachstán';
$string['country.la'] = 'Laos';
$string['country.lb'] = 'Libanon';
$string['country.lc'] = 'Svatá Lucie';
$string['country.li'] = 'Lichtenštejnsko';
$string['country.lk'] = 'Srí Lanka';
$string['country.lr'] = 'Libérie';
$string['country.ls'] = 'Lesotho';
$string['country.lt'] = 'Litva';
$string['country.lu'] = 'Lucembursko';
$string['country.lv'] = 'Lotyšsko';
$string['country.ly'] = 'Libye';
$string['country.ma'] = 'Maroko';
$string['country.mc'] = 'Monako';
$string['country.md'] = 'Moldávie';
$string['country.mg'] = 'Madagaskar';
$string['country.mh'] = 'Marshallovy ostrovy';
$string['country.mk'] = 'Makedonie';
$string['country.ml'] = 'Mali';
$string['country.mm'] = 'Myanmar';
$string['country.mn'] = 'Mongolsko';
$string['country.mo'] = 'Macao';
$string['country.mp'] = 'Severní Mariany';
$string['country.mq'] = 'Martinik';
$string['country.mr'] = 'Mauritánie';
$string['country.ms'] = 'Montserrat';
$string['country.mt'] = 'Malta';
$string['country.mu'] = 'Mauricius';
$string['country.mv'] = 'Maledivy';
$string['country.mw'] = 'Malawi';
$string['country.mx'] = 'Mexiko';
$string['country.my'] = 'Malajsie';
$string['country.mz'] = 'Mosambik';
$string['country.na'] = 'Namibie';
$string['country.nc'] = 'Nová Kaledonie';
$string['country.ne'] = 'Niger';
$string['country.nf'] = 'Norfolk';
$string['country.ng'] = 'Nigérie';
$string['country.ni'] = 'Nikaragua';
$string['country.nl'] = 'Nizozemsko';
$string['country.no'] = 'Norsko';
$string['country.np'] = 'Nepál';
$string['country.nr'] = 'Nauru';
$string['country.nu'] = 'Niue';
$string['country.nz'] = 'Nový Zéland';
$string['country.om'] = 'Omán';
$string['country.pa'] = 'Panama';
$string['country.pe'] = 'Peru';
$string['country.pf'] = 'Francouzská Polynésie';
$string['country.pg'] = 'Papua-Nová Guinea';
$string['country.ph'] = 'Filipíny';
$string['country.pk'] = 'Pákistán';
$string['country.pl'] = 'Polsko';
$string['country.pm'] = 'Saint-Pierre a Miquelon';
$string['country.pn'] = 'Pitcairn';
$string['country.pr'] = 'Portoriko';
$string['country.ps'] = 'Palestina';
$string['country.pt'] = 'Portugalsko';
$string['country.pw'] = 'Palau';
$string['country.py'] = 'Paraguay';
$string['country.qa'] = 'Katar';
$string['country.re'] = 'Réunion';
$string['country.ro'] = 'Rumunsko';
$string['country.ru'] = 'Ruská federace';
$string['country.rw'] = 'Rwanda';
$string['country.sa'] = 'Saúdská Arábie';
$string['country.sb'] = 'Šalamounovy ostrovy';
$string['country.sc'] = 'Seychely';
$string['country.sd'] = 'Súdán';
$string['country.se'] = 'Švédsko';
$string['country.sg'] = 'Singapur';
$string['country.sh'] = 'Svatá Helena';
$string['country.si'] = 'Slovinsko';
$string['country.sj'] = 'Špicberky';
$string['country.sk'] = 'Slovenská republika';
$string['country.sl'] = 'Sierra Leone';
$string['country.sm'] = 'San Marino';
$string['country.sn'] = 'Senegal';
$string['country.so'] = 'Somálsko';
$string['country.sr'] = 'Surinam';
$string['country.st'] = 'Svatý Tomáš a Princův ostrov';
$string['country.sv'] = 'Salvador';
$string['country.sy'] = 'Sýrie';
$string['country.sz'] = 'Svazijsko';
$string['country.tc'] = 'Turks a Caicos';
$string['country.td'] = 'Čad';
$string['country.tf'] = 'Francouzská jižní území';
$string['country.tg'] = 'Togo';
$string['country.th'] = 'Thajsko';
$string['country.tj'] = 'Tádžikistán';
$string['country.tk'] = 'Tokelau';
$string['country.tl'] = 'Východní Timor';
$string['country.tm'] = 'Turkmenistán';
$string['country.tn'] = 'Tunisko';
$string['country.to'] = 'Tonga';
$string['country.tr'] = 'Turecko';
$string['country.tt'] = 'Trinidad a Tobago';
$string['country.tv'] = 'Tuvalu';
$string['country.tw'] = 'Tchaj-wan';
$string['country.tz'] = 'Tanzanie';
$string['country.ua'] = 'Ukrajina';
$string['country.ug'] = 'Uganda';
$string['country.um'] = 'Menší odlehlé ostrovy USA';
$string['country.us'] = 'Spojené státy americké';
$string['country.uy'] = 'Uruguay';
$string['country.uz'] = 'Uzbekistán';
$string['country.va'] = 'Vatikán';
$string['country.vc'] = 'Svatý Vincenc a Grenadiny';
$string['country.ve'] = 'Venezuela';
$string['country.vg'] = 'Panenské ostrovy (V. Británie)';
$string['country.vi'] = 'Panenské ostrovy (USA)';
$string['country.vn'] = 'Vietnam';
$string['country.vu'] = 'Vanuatu';
$string['country.wf'] = 'Wallisovy ostrovy';
$string['country.ws'] = 'Samoa';
$string['country.ye'] = 'Jemen';
$string['country.yt'] = 'Mayotte';
$string['country.za'] = 'Jižní Afrika';
$string['country.zm'] = 'Zambie';
$string['country.zw'] = 'Zimbabwe';
$string['createcollect'] = 'Vytvářet a shromažďovat';
$string['createcollectsubtitle'] = 'Rozvíjejte své portfolio';
$string['createyourresume'] = 'Vytvořte svůj <a href="%s">životopis</a>';
$string['dashboarddescription'] = 'Pohled ovládacího panelu uvidíte vždy na titulní stránce, když se poprvé přihlásíte. Pouze vy k němu máte přístup.';
$string['date'] = 'Datum';
$string['dateformatguide'] = 'Použijte formát RRRR/MM/DD';
$string['dateofbirthformatguide'] = 'Použijte formát RRRR/MM/DD';
$string['datetimeformatguide'] = 'Použijte formát RRRR/MM/DD HH:MM';
$string['day'] = 'den';
$string['days'] = 'dní';
$string['debugemail'] = 'POZNÁMKA: Tento e-mail je určen pro %s <%s>, ale byl zaslán vám díky nastavení "sendallemailto".';
$string['decline'] = 'Zamítnout';
$string['default'] = 'Výchozí';
$string['delete'] = 'Odstranit';
$string['deleteaccount'] = 'Odstraněn účet: %s / %s';
$string['deleteduser'] = 'Odstraněný uživatel';
$string['deletetag'] = 'Smazat <a href="%s">%s</a>';
$string['deletetagdescription'] = 'Smazat tento štítek ze všech položek ve vašem portfoliu';
$string['description'] = 'Popis';
$string['disable'] = 'Zakázat';
$string['discusstopicslinked'] = 'Diskutujte o <a href="%s">tématech</a>';
$string['displayname'] = 'Zobrazované jméno';
$string['divertingemailto'] = 'Přesměrování e-mailu na %s';
$string['done'] = 'Hotovo';
$string['edit'] = 'Upravit';
$string['editdashboard'] = 'Upravit';
$string['editing'] = 'Úprava';
$string['editmyprofilepage'] = 'Upravit stránku s profilem';
$string['edittag'] = 'Upravit <a href="%squot;"gt;%s</a>';
$string['edittagdescription'] = 'Všechny položky ve vašem portfoliu označené štítkem "%s" byly aktualizovány';
$string['edittags'] = 'Upravit štítky';
$string['editthistag'] = 'Upravit tento štítek';
$string['email'] = 'E-mail';
$string['emailaddress'] = 'E-mailová adresa';
$string['emailaddressdescription'] = 'Adresa elektronické pošty';
$string['emailaddressorusername'] = 'E-mailová adresa nebo uživatelské jméno';
$string['emailnotsent'] = 'Nepodařilo se zaslat e-mail. Chybová zpráva: "%s"';
$string['emailtoolong'] = 'E-mailová adresa nemůže být delší než 255 znaků';
$string['enable'] = 'Povolit';
$string['errorprocessingform'] = 'Při odesílání tohoto formuláře došlo k chybě. Překontrolujte označená pole a pokuste se uložit formulář znovu.';
$string['expand'] = 'Rozbalit';
$string['filenotfound'] = 'Soubor nenalezen';
$string['filenotimage'] = 'Formát nahraného souboru není přípustný formát obrázku, musí se jednat o PNG, JPEG nebo GIF.';
$string['fileunknowntype'] = 'Typ nahraného souboru nebyl rozpoznán. Buď je váš soubor poškozen nebo se objevila chyba v konfiguraci serveru. Prosím, spojte se s vaším správcem.';
$string['filter'] = 'Filtr';
$string['filterresultsby'] = 'Fitrovat výsledek podle:';
$string['findfriends'] = 'Najít přátele';
$string['findfriendslinked'] = 'Najděte <a href="%s">přátele</a>';
$string['findgroups'] = 'Najít skupiny';
$string['first'] = 'První';
$string['firstname'] = 'Křestní jméno';
$string['firstnamedescription'] = 'Křestní jméno uživatele';
$string['firstpage'] = 'První stránka';
$string['forgotpassemailsendunsuccessful'] = 'Bohužel, nepodařilo se zaslat vám e-mail. Prosím, zkuste to později.';
$string['forgotpassemailsentanyway'] = 'E-mail byl odeslán na adresu evidovanou u tohoto uživatele, což neznamená, že je platná nebo jí server uživatele nevrátí zpět. Pokud jste e-mail neobdrželi, obraťte se na správce Mahary za účelem nastavení nového hesla.';
$string['forgotpassemailsentanyway1'] = 'E-mail byl odeslán na adresu evidovanou u tohoto uživatele, ale adresa nemusí být správná nebo ji server nemusí doručit. V takovém případě se prosím obraťte se na správce %s.';
$string['forgotpassnosuchemailaddressorusername'] = 'Tato e-mailová adresa nebo uživatelské jméno nepatří žádnému z našich uživatelů';
$string['forgotpasswordenternew'] = 'Před pokračováním zadejte vaše nové heslo';
$string['forgotusernamepassword'] = 'Zapomněli jste své uživatelské jméno nebo heslo?';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>Toto je automaticky generovaný email pro: %s</p>

<p>Na stránkách %s byla podána žádost o zaslání zapomenutého uživatelského jména nebo hesla pro přihlášení.</p>

<p>Vaše uživatelské jméno je <strong>%s</strong>.</p>

<p>Pokud si přejete resetovat vaše heslo, přejděte na následující stránku:</p>

<p><a href="%s">%s</a></p>

<p>Pokud jste o zaslání zapomenutého jména nebo hesla nepožádal/a, ignorujte tento e-mail.</p>

<p>V případě jakýchkoliv dotazů nás neváhejte <a href="%s">kontaktovat</a>.</p>

<p>S pozdravem, %s, správce stránek</p>';
$string['forgotusernamepasswordemailmessagetext'] = 'Toto je automaticky generovaný email pro: %s

Na stránkách %s byla podána žádost o zaslání zapomenutého uživatelského jména nebo hesla pro přihlášení.

Vaše uživatelské jméno je: %s

Pokud si přejete resetovat vaše heslo, přejděte na následující stránku:

%s

Pokud jste o zaslání zapomenutého jména nebo hesla nepožádal/a, ignorujte tento e-mail.

V případě jakýchkoliv dotazů nás neváhejte kontaktovat: %s

S pozdravem, %s, správce stránek';
$string['forgotusernamepasswordemailsubject'] = 'Přihlašovací údaje pro %s';
$string['forgotusernamepasswordtext'] = '<p>Pokud jste zapomněli své uživatelské jméno nebo heslo, zadejte vaši e-mailovou adresu z vašeho uživatelského profilu. Bude vám zaslána zpráva, pomocí níž budete moci získat nové heslo.</p>

<p>Pokud si uživatelské jméno pamatujete a zapomněli jste pouze heslo, můžete namísto e-mailu použít své uživatelské jméno.</p>';
$string['forgotusernamepasswordtextprimaryemail'] = '<p>Pokud jste zapomněli své uživatelské jméno nebo heslo, zadejte primární e-mailovou adresu uvedenou ve svém profilu a my vám zašleme zprávu, kterou můžete použít pro nastavení nového hesla.</p> <p>Pokud znáte své uživatelské jméno a zapomněli jste pouze své heslo, můžete ho zadat namísto primárního e-mailu.</p>';
$string['formatpostbbcode'] = 'Pro formátování můžete použít syntaxi BBCode. %sVíce podrobností%s';
$string['formerror'] = 'Došlo k chybě při zpracování vašeho podání. Zkuste to prosím znovu.';
$string['formerroremail'] = 'Kontaktujte nás na %s pokud vaše problémy přetrvávají.';
$string['fullname'] = 'Celé jméno';
$string['general'] = 'Povolání';
$string['go'] = 'Přejít';
$string['gotoinbox'] = 'Přejít k doručené poště';
$string['groupquota'] = 'Diskový prostor skupiny';
$string['groups'] = 'Skupiny';
$string['height'] = 'Výška';
$string['heightshort'] = 'v';
$string['hidden'] = 'skrytý';
$string['hide'] = 'Skrýt';
$string['home'] = 'Domů';
$string['howtodisable'] = 'Vaše informační plocha je skrytá. Její viditelnost můžete měnit v <a href="%s">nastavení</a>.';
$string['image'] = 'Obrázek';
$string['importedfrom'] = 'Naimportováno z %s';
$string['inbox'] = 'Doručená pošta';
$string['incomingfolderdesc'] = 'Soubory naimportovány z jiných serverů';
$string['installedplugins'] = 'Nainstalované moduly';
$string['institution'] = 'Instituce';
$string['institutionadministration'] = 'Správa instituce';
$string['institutioncontacts'] = '\'%s\' kontaktů';
$string['institutionexpirywarning'] = 'Upozornění na vypršení členství v instituci';
$string['institutionexpirywarninghtml_institution'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p>

<p>Platnost členství %s v %s vyprší za %s.</p>

<p>Pokud chcete prodloužit své členství nebo máte nějaké otázky na výše uvedené, neváhejte nás prosím <a href="%s">kontaktovat</a>.</p>

<p>S pozdravem, správce stránek %s</p>';
$string['institutionexpirywarninghtml_site'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p>

<p>Platnost instituce %s vyprší za %s.</p>

<p> Možná nás budete chtít kontaktovat za účelem prodloužení %s.</p>

<p>S pozdravem, správce stránek %s</p>';
$string['institutionexpirywarningtext_institution'] = 'Toto je automaticky generovaný e-mail pro: %s

Platnost členství %s v %s vyprší za %s.

Pokud chcete prodloužit své členství nebo máte nějaké dotay, neváhejte nás kontatovat na: %s

S pozdravem, správce stránek %s';
$string['institutionexpirywarningtext_site'] = 'Toto je automaticky generovaný e-mail pro: %s

Platnost instituce %s vyprší za %s. Možná nás budete chtít kontaktovat za účelem prodloužení %s.

S pozdravem, správce stránek %s';
$string['institutionfull'] = 'Vybraná instituce již nepřijímá žádné nové členy.';
$string['institutioninformation'] = 'Informace instituce';
$string['institutionlink'] = '<a href="%s">%s</a>';
$string['institutionmemberconfirmmessage'] = 'Bylo vám přiděleno členství v instituci %s.';
$string['institutionmemberconfirmsubject'] = 'Potvrzení členství v instituci';
$string['institutionmemberrejectmessage'] = 'Vaše žádost o členství v %s byla zamítnuta.';
$string['institutionmemberrejectsubject'] = 'Členství v instituci zamítnuto';
$string['institutionmembership'] = 'Členství v instituci';
$string['institutionmembershipdescription'] = 'Zde se zobrazuje seznam všech institucí, jejichž jste členem. Můžete rovněž požádat o členství v nějaká instituci. Případně, pokud vám nějaká instituce nabídla členství, zde můžete přijmout nebo odmítnout tuto nabídku.';
$string['institutionmembershipexpirywarning'] = 'Varování pro vypršení členství v instituci';
$string['institutionmembershipexpirywarninghtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p> <p>

Vaše členství v %s na %s vyprší za %s.</p> <p> Pokud chcete prodloužit své členství nebo máte nějaké otázky na výše uvedené, neváhejte nás prosím <a href="%s">kontaktovat</a>.</p> <p>

S pozdravem, správce stránek %s</p>';
$string['institutionmembershipexpirywarningtext'] = 'Toto je automaticky generovaný e-mail pro: %s

Vaše členství v %s na %s vyprší za %s. Pokud chcete prodloužit své členství nebo máte nějaké otázky na výše uvedené, neváhejte nás prosím kontaktovat na: %s.

S pozdravem, správce stránek %s';
$string['invalidsesskey'] = 'Neplatný klíč sezení (session key)';
$string['invitedgroup'] = 'skupina nabízející členství';
$string['invitedgroups'] = 'skupiny nabízející členství';
$string['itemstaggedwith'] = 'Položka oštítkována jako "%s"';
$string['javascriptnotenabled'] = 'Váš prohlížeč neumožňuje nebo nedovoluje použití Javascriptu na těchto stránkách. Před přihlášením je nutné Javascript povolit.';
$string['joingroups'] = 'Zapojte se do <a href="%s">skupin</a>';
$string['joininstitution'] = 'Stát se členem instituce';
$string['language'] = 'Jazyk';
$string['last'] = 'Poslední';
$string['lastminutes'] = 'Posledních %s minut';
$string['lastname'] = 'Příjmení';
$string['lastnamedescription'] = 'Příjmení uživatele';
$string['lastpage'] = 'Poslední stránka';
$string['lastupdate'] = 'Poslední změna';
$string['lastupdateorcomment'] = 'Poslední změna komentáře';
$string['leaveinstitution'] = 'Opustit instituci';
$string['linksandresources'] = 'Odkazy a zdroje';
$string['loading'] = 'Nahrávám...';
$string['loggedinusersonly'] = 'Povolit přístup pouze pro přihlášené uživatele';
$string['loggedoutok'] = 'Byli jste úspěšně odhlášeni';
$string['login'] = 'Přihlásit se';
$string['loginfailed'] = 'Nezadali jste správné přihlašovací údaje. Ujistěte se, že používáte správné uživatelské jméno a heslo.';
$string['loginto'] = 'Přihlásit se na %s';
$string['logout'] = 'Odhlásit';
$string['lostusernamepassword'] = 'Zapomenuté jméno nebo heslo';
$string['memberofinstitutions'] = 'Členství v %s';
$string['membershipexpiry'] = 'Končí platnost členství';
$string['message'] = 'Zpráva';
$string['messagesent'] = 'Vaše zpráva byla odeslána';
$string['months'] = 'měsíců';
$string['more...'] = 'více...';
$string['mustspecifyoldpassword'] = 'Musíte zadat stávající heslo';
$string['mydashboard'] = 'Můj ovládací panel';
$string['myfriends'] = 'Moji přátelé';
$string['mygroups'] = 'Moje skupiny';
$string['mymessages'] = 'Mé zprávy';
$string['myportfolio'] = 'Moje portfolio';
$string['mytags'] = 'Mé štítky';
$string['name'] = 'Jméno';
$string['namedfieldempty'] = 'Požadované pole "%s" je prázdné';
$string['newpassword'] = 'Nové heslo';
$string['next'] = 'Další';
$string['nextpage'] = 'Další stránka';
$string['nitems'] = 'Pole';
$string['no'] = 'Ne';
$string['nocountryselected'] = 'Žádná země nebyla vybrána';
$string['nodeletepermission'] = 'Nemáte oprávnění odstranit tuto položku portfolia';
$string['noeditpermission'] = 'Nemáte oprávnění upravovat tuto položku portfolia';
$string['noenddate'] = 'Bez termínu';
$string['nohelpfound'] = 'Nebyla nalezena žádná nápověda k této položce';
$string['nohelpfoundpage'] = 'Nebyla nalezena žádná nápověda k této stránce';
$string['noinstitutionadminfound'] = 'Žádní správci instituce nebyli nalezeni';
$string['noinstitutionoldpassemailmessagehtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p> <p>Skončil vaše členství v %s.</p> <p>I nadále můžete používat %s s vaším aktuálním uživatelským jménem %s a heslem zvoleným pro váš účet.</p> <p>Pokud jste zapomněli vaše heslo, můžete ho resetovat na následující stránce po zadání vašeho uživatelského jména.</p> <p><a href="%sforgotpass.php">%sforgotpass.php</a></p> <p>V případě jakýchkoliv dotazů nás neváhejte <a href="%scontact.php">kontaktovat</a>.</p> <p>S pozdravem, %s, správce stránek</p>';
$string['noinstitutionoldpassemailmessagetext'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p> <p>Skončil vaše členství v: %s.</p> <p>I nadále můžete používat %s s vaším aktuálním uživatelským jménem %s a heslem zvoleným pro váš účet.</p> <p>Pokud jste zapomněli vaše heslo, můžete ho resetovat na následující stránce po zadání vašeho uživatelského jména.%sforgotpass.php V případě jakýchkoliv dotazů nás neváhejte scontact.php kontaktovat</a>. S pozdravem, %s, správce stránek';
$string['noinstitutionoldpassemailsubject'] = '%s: Členství v %s';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>Toto je automaticky generovaný e-mail pro: %s</p> <p>Skončilo vaše členství v: %s.</p> <p>I nadále můžete používat %s s vaším aktuálním uživatelským jménem %s a heslem zvoleným pro váš účet.</p> <p>Pokud jste zapomněli vaše heslo, můžete ho resetovat na následující stránce po zadání vašeho uživatelského jména.</p> <p><a href="%sforgotpass.php">%sforgotpass.php</a></p> <p>V případě jakýchkoliv dotazů nás neváhejte <a href="%scontact.php">kontaktovat</a>.</p> <p>S pozdravem, %s, správce stránek</p>';
$string['noinstitutionsetpassemailmessagetext'] = 'Toto je automaticky generovaný e-mail pro: %s

Skončilo vaše členství v: %s

Stránky %s můžete i nadále používat pod svým uživatelským jménem "%s", ale budete si muset nastavit nové heslo.

Přejděte na následující stránku pro získání nového hesla:

%sforgotpass.php?key=%s

V případě jakýchkoliv dotazů nás neváhejte kontaktovat: %scontact.php

S pozdravem, %s, správce stránek';
$string['noinstitutionsetpassemailsubject'] = '%s: Členství v %s';
$string['noinstitutionstafffound'] = 'Žádní zaměstnanci instituce nebyli nalezeni';
$string['none'] = 'Žádné';
$string['noonlineusersfound'] = 'Žádní připojení uživatelé nebyli nalezeni';
$string['nopublishpermissiononartefact'] = 'Nemáte oprávnění zveřejnit %s';
$string['noresultsfound'] = 'Nenalezeno';
$string['nosendernamefound'] = 'Nebylo vloženo jméno odesilatele';
$string['nosessionreload'] = 'Pro přihlášení obnovte tuto stránku';
$string['nosuchpasswordrequest'] = 'Nebyla nalezena taková žádost o změnu hesla';
$string['notifications'] = 'Přehled událostí';
$string['notinstallable'] = 'Nelze instalovat!';
$string['notinstalledplugins'] = 'Nenainstalované moduly';
$string['notphpuploadedfile'] = 'Soubor se v průběhu nahrávaní ztratil. To by se nemělo stát, spojte se s vaším správcem.';
$string['numitems'] = 'Počet položek: %s';
$string['nusers'] = 'Pole';
$string['oldpassword'] = 'Stávající heslo';
$string['onlineusers'] = 'Připojení uživatelé';
$string['optionalinstitutionid'] = 'Identifikátor instituce (volitelné)';
$string['organisedescription'] = 'Uspořádejte své portfolio do <a href="%s">pohledů.</a> Vytvořte různé pohledy pro různé účely - můžete zvolit, jaké prvky budou obsahovat.';
$string['organisesubtitle'] = 'Ukažte své portfolio s pohledy';
$string['password'] = 'Heslo';
$string['passwordchangedok'] = 'Vaše heslo bylo úspěšně změněno';
$string['passwordhelp'] = 'Heslo, které musíte zadat pro přihlášení se na tyto stránky';
$string['passwordnotchanged'] = 'Heslo jste nezměnili, zadejte nové heslo';
$string['passwordsaved'] = 'Nové heslo bylo uloženo';
$string['passwordsdonotmatch'] = 'Hesla neodpovídají';
$string['passwordtooeasy'] = 'Takové heslo je příliš snadné uhádnout! Zvolte si lepší heslo';
$string['pendingfriend'] = 'čekající žádost o přátelství';
$string['pendingfriends'] = 'čekající žádosti o přátelství';
$string['phpuploaderror'] = 'Vyskytla se chyba při nahrávání souboru: %s (Kód chyby %s)';
$string['phpuploaderror_1'] = 'Velikost souboru přesahuje hodnotu definovanou parametrem upload_max_filesize directive v souboru php.ini.';
$string['phpuploaderror_2'] = 'Velikost souboru přesahuje hodnotu definovanou direktivou MAX_FILE_SIZE v HTML formuláři.';
$string['phpuploaderror_3'] = 'Soubor byl nahrán pouze zčásti.';
$string['phpuploaderror_4'] = 'Nebyl nahrán žádný soubor';
$string['phpuploaderror_6'] = 'Chybí dočasný adresář';
$string['phpuploaderror_7'] = 'Nepodařilo se zapsat soubor na disk';
$string['phpuploaderror_8'] = 'Nahrávání souboru bylo ukončeno rozšiřujícím modulem.';
$string['pleasedonotreplytothismessage'] = 'Prosíme, neodpovídejte na tuto zprávu.';
$string['pluginbrokenanddisabled'] = 'Uživatel se pokusil načíst zásuvný modul %s, který nemohl být nahrán.
Pro zamezení budoucích chyb byl zásuvný modul zakázán. Vygenerovaná chybová zpráva zásuvného modulu byla: ---------------------------------------------------------------------------- %s ---------------------------------------------------------------------------- Chcete-li znovu povolit plugin, navštivte stránku "Rozšíření".';
$string['pluginbrokenanddisabledtitle'] = 'Poškozený zásuvný modul (%s) byl zakázán';
$string['plugindisabled'] = 'Zásuvný modul byl skryt.';
$string['pluginenabled'] = 'Zásuvný modul povolen';
$string['pluginexplainaddremove'] = 'Zásuvné moduly v Mahaře jsou vždycky nainstalované a mohou být přístupné, pokud uživatel zná příslušnou adresu URL a může je používat. Spíše než zakázané nebo povolené jsou zásuvné moduly skryté a lze je zviditelnit kliknutím na příslušnou volbu vedle jejich názvu.';
$string['pluginexplainartefactblocktypes'] = 'Pokud skryjete zásuvný modul typu "artefact", systém Mahara přestane zobrazovat jemu odpovídající "blocktype".';
$string['pluginnotenabled'] = 'Zásuvný modul je skrytý. Nejprve musíte zásuvný modul %s zviditelnit.';
$string['plugintype'] = 'Typ zásuvného modulu';
$string['preferences'] = 'Předvolby';
$string['preferredname'] = 'Upřednostňované jméno';
$string['previous'] = 'Předchozí';
$string['prevpage'] = 'Předchozí stránka';
$string['primaryemailinvalid'] = 'Vaše primární e-mailová adresa není platná';
$string['privacystatement'] = 'Prohlášení o ochraně osobních údajů';
$string['processing'] = 'Zpracovávám';
$string['profile'] = 'profil';
$string['profiledescription'] = 'Váš profilový pohled je to, co vidí ostatní, když kliknou na vaše jméno nebo na ikonu profilu';
$string['profileimage'] = 'Obrázek v profilu';
$string['publishablog'] = 'Začněte <a href="%s">blogovat</a>';
$string['pwchangerequestsent'] = 'Během krátké chvíle byste měli obdržet e-mail v odkazem na stránku, na které si budete moci změnit vaše heslo';
$string['quarantinedirname'] = 'karantena';
$string['query'] = 'Dotaz';
$string['querydescription'] = 'Slova, která mají bát hledána';
$string['quota'] = 'Diskový prostor';
$string['quotausage'] = 'Využíváte <span id="quota_used">%s</span> z přiděleného diskového prostoru<span id="quota_total">%s</span>.';
$string['reallyleaveinstitution'] = 'Opravdu chcete opustit tuto instituci?';
$string['reason'] = 'Důvod';
$string['recentactivity'] = 'Poslední aktivita';
$string['register'] = 'Registrovat';
$string['registeragreeterms'] = 'Musíte také souhlasit s <a href="terms.php">podmínkami</a>.';
$string['registeringdisallowed'] = 'Bohužel, na těchto stránkách se nemůžete sami registrovat.';
$string['registerprivacy'] = 'Údaje, které shromažďujeme, zde budou uloženy v souladu s našim <a href="privacy.php">prohlášením o ochraně soukromí</a>.';
$string['registerstep3fieldsmandatory'] = '<h3>Vyplňte povinná pole uživatelského profilu</h3>

<p>Následující pole jsou povinná. Musíte je vyplnit pro dokončení registrace.</p>';
$string['registerstep3fieldsoptional'] = '<h3>Do profilu můžete vložit váš obrázek</h3>

<p>Zdárně jste se registrovali na stránkách %s. Nyní si můžete zvolit fotografii či obrázek, který bude zobrazován ve vašem profilu.</p>';
$string['registerwelcome'] = 'Vítejte! Chcete-li používat tuto stránku, musíte se nejprve zaregistrovat.';
$string['registrationcomplete'] = 'Děkujeme za registraci na stránkách %s';
$string['registrationnotallowed'] = 'Vámi vybraná instituce neumožňuje, abyste se sami registrovali.';
$string['reject'] = 'Odmítnout';
$string['remotehost'] = 'Vzdálený hostitel %s';
$string['remove'] = 'Odebrat';
$string['republish'] = 'Publikovat';
$string['requestmembershipofaninstitution'] = 'Požádat o členství v instituci';
$string['requiredfieldempty'] = 'Požadované pole je prázdné';
$string['result'] = 'výsledek';
$string['results'] = 'výsledky';
$string['resultsperpage'] = 'Výsledků na stránku';
$string['returntosite'] = 'Zpět na titulní stránku';
$string['save'] = 'Uložit';
$string['search'] = 'Hledání';
$string['searchresultsfor'] = 'Výsledky hledání';
$string['searchusers'] = 'Vyhledat uživatele';
$string['searchwithin'] = 'Hledat v rámci';
$string['select'] = 'Vybrat';
$string['selectatagtoedit'] = 'Vyberte šítek k úpravě';
$string['selfsearch'] = 'Prohledat moje portfolio';
$string['send'] = 'Poslat';
$string['sendmessage'] = 'Poslat zprávu';
$string['sendrequest'] = 'Poslat žádost';
$string['sessiontimedout'] = 'Vaše přihlášení vypršelo. Pro pokračování musíte znovu zadat vaše přihlašovací údaje.';
$string['sessiontimedoutpublic'] = 'Vaše přihlášení vypršelo. Pro pokračování se musíte <a href="%s">přihlásit</a>';
$string['sessiontimedoutreload'] = 'Vaše přihlášení vypršelo. Obnovte stránku pro opětovné přihlášení';
$string['setblocktitle'] = 'Nastavit název bloku';
$string['settings'] = 'Nastavení';
$string['settingssaved'] = 'Nastavení uloženo';
$string['settingssavefailed'] = 'nastavení se nepodařilo uložit';
$string['sharenetwork'] = 'Sdílet a síťovat';
$string['sharenetworkdescription'] = 'Můžete jemně doladit, kdo bude mít přístup ke každému vašemu pohledu a na jak dlouho.';
$string['sharenetworksubtitle'] = 'Pozvěte přátele a přidejte se do skupin';
$string['show'] = 'Ukázat';
$string['showtags'] = 'Uložit moje štítky';
$string['siteadministration'] = 'Správa stránek';
$string['siteclosed'] = 'Stránky jsou momentálně uzavřeny z důvodu upgrade databáze. Přihlásit se mohou pouze správci.';
$string['siteclosedlogindisabled'] = 'Stránky jsou momentálně uzavřeny z důvodu upgrade databáze. <a href="%s">Spustit upgrade.</a>';
$string['sitecontentnotfound'] = 'Text "%s" není dostupný';
$string['siteinformation'] = 'Informace o stránkách';
$string['sizeb'] = 'B';
$string['sizegb'] = 'GB';
$string['sizekb'] = 'KB';
$string['sizemb'] = 'MB';
$string['sortalpha'] = 'Seřadit štítky podle abecedy';
$string['sortfreq'] = 'Seřadit štítky podle frekvence používání';
$string['sortresultsby'] = 'Seřadit výsledky podle:';
$string['spamtrap'] = 'Past na spam';
$string['staffofinstitutions'] = 'Zaměstnanci z %s';
$string['strftimenotspecified'] = 'Neurčeno';
$string['studentid'] = 'Identifikátor';
$string['subject'] = 'Předmět';
$string['submit'] = 'Odeslat';
$string['system'] = 'Systém';
$string['tagdeletedsuccessfully'] = 'Štítek byl úspěšně odstraněn';
$string['tagfilter_all'] = 'Vše';
$string['tagfilter_file'] = 'Soubory';
$string['tagfilter_image'] = 'Obrázky';
$string['tagfilter_text'] = 'Text';
$string['tagfilter_view'] = 'Pohledy';
$string['tags'] = 'Štítky';
$string['tagsdesc'] = 'Vložte seznam štítků (oddělených čárkami) pro tuto položku.';
$string['tagsdescprofile'] = 'Vložte seznam štítků (oddělených čárkami) pro tuto položku. Položky nesoucí štítek "profile" budou zobrazeny v postranním bloku.';
$string['tagupdatedsuccessfully'] = 'Štítky byly úspěšně aktualizovány';
$string['termsandconditions'] = 'Podmínky používání těchto stránek';
$string['theme'] = 'Motiv';
$string['thisistheprofilepagefor'] = 'Toto je stránka s profilem uživatele %s';
$string['topicsimfollowing'] = 'Témata, s nimiž se ztotožňuji';
$string['unknownerror'] = 'Vyskytla se neznámá chyba (0x20f91a0)';
$string['unreadmessage'] = 'nepřečtená zpráva';
$string['unreadmessages'] = 'nepřečtených zpráv';
$string['update'] = 'Upravit';
$string['updatefailed'] = 'Úprava selhala';
$string['updateyourprofile'] = 'Aktualizujte svůj <a href="%s">profil</a>';
$string['upload'] = 'Nahrát';
$string['uploadedfiletoobig'] = 'Soubor je příliš velký. Požádejte o pomoc vašeho správce.';
$string['uploadyourfiles'] = 'Nahrajte své <a href="%s">soubory</a>';
$string['username'] = 'Uživatelské jméno';
$string['usernamehelp'] = 'Uživatelské jméno, pod kterým se přihlašujete na tyto stránky';
$string['users'] = 'Uživatelé';
$string['usersdashboard'] = 'Ovládací panel uživatele %s';
$string['usersprofile'] = 'Profil uživatele %s';
$string['view'] = 'Pohled';
$string['viewmyprofilepage'] = 'Zobrazit stránku s profilem';
$string['views'] = 'Pohledy';
$string['virusfounduser'] = 'Soubor %s je infikován počítačovým virem - není možné jej nahrát!';
$string['virusrepeatmessage'] = 'Uživatel %s se pokusil nahrát několik souborů infikovaných počítačovým virem.';
$string['virusrepeatsubject'] = 'Upozornění: %s opakovaně nahrává zavirované soubory';
$string['weeks'] = 'týdnů';
$string['width'] = 'Šířka';
$string['widthshort'] = 'š';
$string['years'] = 'roků';
$string['yes'] = 'Ano';
$string['youareamemberof'] = 'Jste členem %s';
$string['youaremasqueradingas'] = 'Vydáváte se za uživatele %s';
$string['youhavebeeninvitedtojoin'] = 'Bylo vám nabídnuto členství v %s';
$string['youhavenottaggedanythingyet'] = 'Zatím jste štítkem nic neoznačili';
$string['youhaverequestedmembershipof'] = 'Požádal jste o členství v %s';
$string['youraccounthasbeensuspended'] = 'Váš účet byl suspendován';
$string['youraccounthasbeensuspendedreasontext'] = 'Váš účet na stránkách %s byl suspendován uživatelem %s. Důvod: %s';
$string['youraccounthasbeensuspendedtext2'] = 'Váš účet na stránkách %s byl suspendován uživatelem %s.';
$string['youraccounthasbeenunsuspended'] = 'Suspendování vašeho účtu bylo zrušeno';
$string['youraccounthasbeenunsuspendedtext2'] = 'Suspendování vašeho účtu na stránkách %s bylo zrušeno. Můžete se opět přihlásit.';
$string['yournewpassword'] = 'Nové heslo';
$string['yournewpasswordagain'] = 'Nové heslo (znovu)';
