<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['accountdeleted'] = 'Váš účet byl odstraněn';
$string['accountoptionsdesc'] = 'Zde můžete nastavit obecné parametry účtu';
$string['changepassworddesc'] = 'Pokud si přejete změnit heslo, vyplňte následující údaje';
$string['changepasswordotherinterface'] = 'Heslo si můžete změnit na <a href="%s">jiné stránce</a>';
$string['changeusername'] = 'Nové uživatelské jméno';
$string['changeusernamedesc'] = 'Uživatelské jméno, které používáte na %s. Uživatelská jména mají 3 až 30 znaků a mohou obsahovat písmena, číslice a většinu běžných znaků kromě mezery.';
$string['changeusernameheading'] = 'Změna uživatelského jména';
$string['deleteaccount'] = 'Zrušit účet';
$string['deleteaccountdescription'] = 'Pokud odstraníte svůj účet, vaše profilové informace a pohledy nebudou dále viditelné ostatním uživatelům. Obsah všech příspěvků ve fórech, které jste napsali, bude nadále viditelný, nebude už u nich ale uvedeno vaše jméno.';
$string['disabled'] = 'Zakázat';
$string['disableemail'] = 'Zakázat zasílání e-mailů';
$string['disablemultipleblogserror'] = 'Nemůžete zakázat více blogů, protože máte jen jeden';
$string['enabled'] = 'Povolit';
$string['enablemultipleblogs'] = 'Povolit více blogů';
$string['enablemultipleblogsdescription'] = 'Stanadardně můžete mít jeden blog. Pokud jich chcete mít více, zaškrtněnte tuto volbu.';
$string['friendsauth'] = 'Noví přátelé požadují mou autorizaci';
$string['friendsauto'] = 'Noví přátelé jsou automaticky autorizováni';
$string['friendsdescr'] = 'Nastavení přátel';
$string['friendsnobody'] = 'Nikdo mě nemůže přidat mezi své přátele';
$string['hiderealname'] = 'Skrýt skutečné jméno';
$string['hiderealnamedescription'] = 'Zaškrtněte toto políčko, pokud jste nastavili zobrazení svého jména a nechcete, aby vás ostatní uživatelé mohli najít podle vašeho skutečného jména při vyhledávání uživatelů.';
$string['language'] = 'Jazyk';
$string['maildisabled'] = 'E-mail zakázán';
$string['maildisabledbounce'] = 'Odeslání e-mailů na vaši adresu bylo zakázáno, protože se nám z ní vrátilo příliš mnoho zpráv jako nedoručitelných. Zkontrolujte si prosím, jestli váš účet funguje předtím, než ho znovu povolíte v nastavení účtu na %s.';
$string['maildisableddescription'] = 'Odesílání e-mailů na váš účet bylo zakázáno. Můžete ho <a href="%s">znovu povolit</a> na stránce s nastavením účtu.';
$string['messagesallow'] = 'Kdokoliv mně může poslat zprávu';
$string['messagesdescr'] = 'Zprávy od ostatních uživatelů';
$string['messagesfriends'] = 'Mí přátelé mi mohou poslat zprávu';
$string['messagesnobody'] = 'Nikdo mně nemůže poslat zprávu';
$string['mobileuploadtoken'] = 'Token pro nahrávání mobilem';
$string['mobileuploadtokendescription'] = 'Vložte token sem a do vašeho telefonu, aby bylo možné nahrávání povolit (poznámka: bude jiný pro každé nahrávání). <br/>Pokud se setkáte s nějakými problémy, jednoduše token resetujte zde i na telefonu.';
$string['off'] = 'Vypnuto';
$string['oldpasswordincorrect'] = 'Toto není stávající heslo';
$string['on'] = 'Zapnuto';
$string['prefsnotsaved'] = 'Ukládání vašich předvoleb selhalo!';
$string['prefssaved'] = 'Předvolby uloženy';
$string['showhomeinfo'] = 'Zobrazit informace o Mahaře na titulní stránce';
$string['showhomeinfo1'] = 'Informace na domovské stránce';
$string['showhomeinfodescription'] = 'Na domovské stránce zobrazí informace o tom, jak používat %s.';
$string['showviewcolumns'] = 'Při úpravách pohledu zobrazovat prvky pro přidání a odebrání sloupců';
$string['tagssideblockmaxtags'] = 'Maximum štítků v přehledu';
$string['tagssideblockmaxtagsdescription'] = 'Maximální počet položek zobrazných ve vašem přehledu štítků';
$string['updatedfriendcontrolsetting'] = 'Nastavení přátel upraveno';
$string['usernameexists'] = 'Uživatelské jméno již existuje, vyberte prosím jiné.';
$string['wysiwygdescr'] = 'Editor HTML';
