<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['Collection'] = 'Sbírka';
$string['Collections'] = 'Sbírky';
$string['about'] = 'O';
$string['access'] = 'Přístup';
$string['accesscantbeused'] = 'Potlačení přístupu nebylo uložené. Vybraný přístup k pohledu (tajné URL) nemuže být použito pro více zobrazení.';
$string['accessignored'] = 'Některé typy tajných URL odkazů byly ignorovány.';
$string['accessoverride'] = 'Přístup potlačen';
$string['accesssaved'] = 'Přístup ke sbírce byl úspěšně uložen.';
$string['add'] = 'Přidat';
$string['addviews'] = 'Přidat pohledy';
$string['addviewstocollection'] = 'Přidat pohledy do sbírky';
$string['back'] = 'Zpět';
$string['by'] = 'od';
$string['cantdeletecollection'] = 'Nemůžete odstranit tuto sbírku.';
$string['canteditdontown'] = 'Nemůžete upravit tuto sbírku, protože nejste jejím vlastníkem.';
$string['collection'] = 'sbírka';
$string['collectionaccess'] = 'Přístup ke sbírce';
$string['collectionaccesseditedsuccessfully'] = 'Přístup ke sbírce byl úspěšně uložen';
$string['collectionconfirmdelete'] = 'Pohledy v této sbírce nebudou odstraněny. Jste si jisti, že chcete odstranit tuto sbírku?';
$string['collectioncopywouldexceedquota'] = 'Kopírováním této sbírky bude překročen váš limit místa na disku.';
$string['collectioncreatedsuccessfully'] = 'Sbírka byla úspěšně vytvořena.';
$string['collectioncreatedsuccessfullyshare'] = 'Vaše sbírka byla úspěšně vytvořena. Podělte se o své sbírky s ostatními prostřednictvím níže uvedených odkazy.';
$string['collectiondeleted'] = 'Sbírka byla úspěšně odstraněna.';
$string['collectiondescription'] = 'Sbírka je soubor pohledů, které jsou navzájem propojené a mají stejné oprávnění k přístupu. Můžete vytvořit tolik sbírek, kolik chcete, ale pohled se nemůže objevit ve více než jedné sbírce.';
$string['collectioneditaccess'] = 'Právě upravujete přístup pro pohled %d v této sbírce';
$string['collections'] = 'Sbírky.';
$string['collectionsaved'] = 'Sbírka byla uložena.';
$string['collectiontitle'] = 'Titulek sbírky';
$string['confirmcancelcreatingcollection'] = 'Tato sbírka nebyla dokončena. Opravdu chcete zrušit?';
$string['copiedblogpoststonewjournal'] = 'Zkopírováné příspevky v blogu byly vloženy do nového samostatného blogu.';
$string['copiedpagesblocksandartefactsfromtemplate'] = 'Zkopírováno %d pohledů, %d bloků a %d položek portfolia z %s';
$string['copyacollection'] = 'Kopírovat sbírku';
$string['copycollection'] = 'Kopírovat sbírku';
$string['created'] = 'Vytvořeno';
$string['deletecollection'] = 'Smazat sbírku';
$string['deletespecifiedcollection'] = 'Odstranit sbírku \'%s\'';
$string['deleteview'] = 'Odstranit pohled ze sbírky';
$string['deletingcollection'] = 'Mažu sbírku';
$string['description'] = 'Popis sbírky';
$string['editaccess'] = 'Upravit přístup ke sbírce';
$string['editcollection'] = 'Upravit sbírku';
$string['editingcollection'] = 'Upravuji sbírku';
$string['edittitleanddesc'] = 'Upravit název a popis';
$string['editviewaccess'] = 'Upravit přístup k pohledu';
$string['editviews'] = 'Upravit pohledy sbírky';
$string['emptycollection'] = 'Prázdná sbírka';
$string['emptycollectionnoeditaccess'] = 'Nemůžete měnit přístup k prázdné sbírkce. Nejprve přidejte nějaké pohledy.';
$string['manageviews'] = 'Spravovat pohledy';
$string['name'] = 'Název sbírky';
$string['newcollection'] = 'Nová sbírka';
$string['nocollectionsaddone'] = 'Dosud nejsou vytvořeny žádné sbírky. %sVytvořte nějaké%s!';
$string['nooverride'] = 'Žádné potlačení';
$string['noviews'] = 'Žádné pohledy.';
$string['noviewsaddsome'] = 'Ve sbírce nejsou žádné pohledy. %sPřidejte nějaké!%s';
$string['noviewsavailable'] = 'Žádné pohledy nejsou k dispozici pro přidání.';
$string['overrideaccess'] = 'Potlačit přístup';
$string['pluginname'] = 'Sbírky';
$string['potentialviews'] = 'Potenciální pohledy';
$string['saveapply'] = 'Použít &amp; uložit';
$string['savecollection'] = 'Uložit sbírku';
$string['update'] = 'Aktualizovat';
$string['usecollectionname'] = 'Použít jméno sbírky?';
$string['usecollectionnamedesc'] = 'Pokud chcete použít jméno sbírky namísto titulku bloku, ponechejte tuto volbu zaškrtnutou.';
$string['viewaddedtocollection'] = 'Pohledy byly přidány do sbírky. Sbírka je aktualizována, aby zahrnovala přístup k novým pohledům.';
$string['viewcollection'] = 'Zobrazit detaily sbírky';
$string['viewconfirmremove'] = 'Opravdu si přejete odstranit tento pohled se sbírky?';
$string['viewcount'] = 'Pohledy';
$string['viewnavigation'] = 'Zobraz navigační lištu';
$string['viewnavigationdesc'] = 'Přidej horizontální navigační lištu ke každému pohledu v této sbírce jako výchozí nastavení.';
$string['viewremovedsuccessfully'] = 'Pohled byl úspěšně odstraněn.';
$string['viewsaddedtocollection'] = 'Pohledy byly přidány do sbírky. Sbírka je aktualizována, aby zahrnovala přístup k novým pohledům.';
$string['viewstobeadded'] = 'Pohledy byly přidány';
