<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2011
 *
 */

defined('INTERNAL') || die();

$string['addtowatchlist'] = 'Přidat na seznam sledovaných';
$string['adminnotificationerror'] = 'Chyba v upozorněních uživatele je pravděpodobně způsobena konfigurací vašeho serveru.';
$string['alltypes'] = 'Všechny typy';
$string['artefacts'] = 'Položky portfolia';
$string['attime'] = 'v';
$string['date'] = 'Datum';
$string['deleteallnotifications'] = 'Smazat všechna upozornění';
$string['deletednotifications'] = 'Počet odstraněných upozornění: %s';
$string['failedtodeletenotifications'] = 'Nelze odstranit upozornění';
$string['failedtomarkasread'] = 'Nelze označit upozornění jako přečtené';
$string['groups'] = 'Skupiny';
$string['institutioninvitemessage'] = 'Příslušnost k této instituci můžete potvrdit na stránce Nastavení institucí:';
$string['institutioninvitesubject'] = 'Byla vám nabídnuta příslušnost k instituci %s';
$string['institutionrequestmessage'] = 'Přiřadit uživatele k této instituci můžete na stránce Členové instituce:';
$string['institutionrequestsubject'] = '%s požádal(a) o příslušnost k %s';
$string['markasread'] = 'Označit jako přečtené';
$string['markedasread'] = 'Upozornění označena jako přečtená';
$string['missingparam'] = 'Chybí požadovaný parametr %s u typu činnosti %s';
$string['monitored'] = 'Monitorováno';
$string['newcontactus'] = 'Nový kontakt';
$string['newcontactusfrom'] = 'Nový kontakt z';
$string['newgroupmembersubj'] = '%s je nyní členem skupiny!';
$string['newviewaccessmessage'] = 'Byli jste přidáni mezi uživatele oprávněné k prohlížení pohledu "%s" uživatele %s';
$string['newviewaccessmessagenoowner'] = 'Byli jste přidání mezi uživatele oprávněné k prohlížení pohledu "%s"';
$string['newviewaccesssubject'] = 'Přístup k pohledu';
$string['newviewmessage'] = '%s vytvořil(a) nový pohled "%s"';
$string['newviewsubject'] = 'Vytvořen nový pohled';
$string['newwatchlistmessage'] = 'Nová činnost na vašem seznamu sledování';
$string['newwatchlistmessageview'] = '%s změnil(a) nový pohled "%s"';
$string['objectionablecontentview'] = 'Nevhodný obsahv  pohledu "%s" ohlášen uživatelem %s';
$string['objectionablecontentviewartefact'] = 'Nevhodný obsah v pohledu "%s" v "%s" ohlášen uživatelem %s';
$string['objectionablecontentviewartefacthtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Nevhodný obsah pohledu "%s" v "%s" ohlášen uživatelem %s<strong></strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Stížnost se týká: <a href="%s">%s</a></p> <p>Ohlášeno uživatelem: <a href="%s">%s</a></p> </div>';
$string['objectionablecontentviewartefacttext'] = 'Nevhodný obsah na "%s" v "%s" ohlášen uživatelem %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Pokud chcete pohled vidět, navštivte odkaz: %s Pro zobrazení profilu nahlašovatele pak odkaz: %s';
$string['objectionablecontentviewhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Nevhodný obsah v pohledu "%s" ohlášen uživatelem %s<strong></strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Stížnost se týká: <a href="%s">%s</a></p> <p>Ohlášeno uživatelem: <a href="%s">%s</a></p> </div>';
$string['objectionablecontentviewtext'] = 'Nevhodný obsah na "%s" ohlášen uživatelem %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Pokud chcete pohled vidět, navštivte odkaz: %s Pro zobrazení profilu nahlašovatele pak odkaz: %s';
$string['ongroup'] = 'na skupinu';
$string['ownedby'] = 'vlastník';
$string['prefsdescr'] = 'Ať už si vyberete kteroukoliv z možností zasílání e-mailu, budou nové události zobrazeny v Přehledu událostí, ale budou automaticky označeny jako přečtené.';
$string['read'] = 'Přečíst';
$string['reallydeleteallnotifications'] = 'Skutečně chcete smazat všechna svá upozornění?';
$string['recurseall'] = 'Rekurzivně vše';
$string['removedgroupmembersubj'] = '%s již není členem skupiny';
$string['removefromwatchlist'] = 'Odstranit ze seznamu sledování';
$string['selectall'] = 'Vybrat vše';
$string['stopmonitoring'] = 'Zastavit monitorování';
$string['stopmonitoringfailed'] = 'Zastavení monitorování selhalo';
$string['stopmonitoringsuccess'] = 'Monitorování úspěšně zastaveno';
$string['subject'] = 'Předmět';
$string['type'] = 'Typ';
$string['typeadminmessages'] = 'Zprávy pro správce';
$string['typecontactus'] = 'Kontaktujte nás';
$string['typegroupmessage'] = 'Zpráva pro skupinu';
$string['typeinstitutionmessage'] = 'Zpráva pro instituci';
$string['typemaharamessage'] = 'Systémová zpráva';
$string['typeobjectionable'] = 'Nevhodný obsah';
$string['typeusermessage'] = 'Zpráva od jiných uživatelů';
$string['typeviewaccess'] = 'Nový přístup k pohledu';
$string['typevirusrelease'] = 'Upozornění na virus';
$string['typevirusrepeat'] = 'Opakované ukladání zavirovaného obsahu';
$string['typewatchlist'] = 'Sledování';
$string['unread'] = 'Nepřečteno';
$string['viewmodified'] = 'změnil(a) svůj pohled';
$string['viewsubmittedmessage'] = 'Uživatel %s odevzdal/a pohled "%s" k ohodnocení ve skupině %s';
$string['viewsubmittedsubject'] = 'Pohled odevzdán k ohodnocení v rámci skupiny %s';
