<?php

defined('INTERNAL') || die();

$string['description'] = 'Předá vám export ve <a href="http://wiki.leapspecs.org/2A/specification">standardním formátu Leap2A</a>.

Později ho můžete použít pro import vašich dat do <a href="http://wiki.mahara.org/index.php?title=Developer_Area/Import%2F%2FExport/Interoperability/">jiných, Leap2A kompatibilních systémů</a>, i když je export obtížně čitelný lidem.';
$string['title'] = 'Leap2A';
