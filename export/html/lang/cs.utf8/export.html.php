<?php

defined('INTERNAL') || die();

$string['buildingindexpage'] = 'Vytvářím úvodní stránku';
$string['copyingextrafiles'] = 'Kopíruji další soubory';
$string['description'] = 'Vytvářím samostatné webové stránky s daty portfolia Ty již znovu nemůžete importovat, ale jsou čitelné ve standardním webovém prohlížeči.';
$string['exportingdatafor'] = 'Exportuji data z %s';
$string['preparing'] = 'Připravuji %s';
$string['title'] = 'Samostatná webová stránka HTML';
$string['usersportfolio'] = '%s - portfolio';
