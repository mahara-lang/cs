<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['Allow'] = 'Povolit';
$string['Attachments'] = 'Přílohy';
$string['Comment'] = 'Komentář';
$string['Comments'] = 'Komentáře';
$string['Moderate'] = 'Moderovat';
$string['allowcomments'] = 'Povolit komentáře';
$string['approvalrequired'] = 'Komentáře jsou moderovány, takže pokud se rozhodnete tento komentář zveřejnit, nebude viditelný pro ostatní až do schválení majitelem.';
$string['artefactdefaultpermissions'] = 'Výchozí oprávnění komentáře';
$string['artefactdefaultpermissionsdescription'] = 'Vybrané položky portfolia budou mít při vytvoření povolené komentáře. Uživatelé mohou přepsat toto nastavení pro jednotlivé položky portfolia.';
$string['attachfile'] = 'Přiložit soubor';
$string['cantedithasreplies'] = 'Můžete upravovat pouze nejnovější komentář';
$string['canteditnotauthor'] = 'Nejste autor tohoto komentáře';
$string['cantedittooold'] = 'Můžete upravit komentáře, které nejsou starší více jak tento počet minut: %d';
$string['comment'] = 'komentář';
$string['commentdeletedauthornotification'] = 'Váš komentář na %s byl odstraněn: %s';
$string['commentdeletednotificationsubject'] = 'Komentář na %s odstraněn';
$string['commentmadepublic'] = 'Zveřejnit komentář';
$string['commentnotinview'] = 'Komentář %d není v pohledu %d';
$string['commentratings'] = 'Povolit komentář hodnocení';
$string['commentremoved'] = 'Komentář odstraněn';
$string['commentremovedbyadmin'] = 'Komentář odstraněn správcem';
$string['commentremovedbyauthor'] = 'Komentář odstraněn autorem';
$string['commentremovedbyowner'] = 'Komentář odstraněn majitelem';
$string['comments'] = 'komentáře';
$string['commentupdated'] = 'Komentář aktualizován';
$string['editcomment'] = 'Upravit komentář';
$string['editcommentdescription'] = 'Můžete upravit své komentáře, pokud nejsou starší více jak %d minut a nebylo na ně nijak reagováno. Po uplynutí této doby můžte své komentáře odstranit a přidat nové.';
$string['entriesimportedfromleapexport'] = 'Položky byly importovány z exportu LEAP, není tedy možné je importovat jinde.';
$string['feedback'] = 'Zpětná vazba';
$string['feedbackattachdirdesc'] = 'Soubory připojené ke komentáři ve vašem portfoliu';
$string['feedbackattachdirname'] = 'commentfiles';
$string['feedbackattachmessage'] = 'Připojený soubor (soubory) byly přidány do vaší složky %s';
$string['feedbackdeletedhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>Komentář na %s byl odstraněn.</strong><br />%s</div><div style="margin: 1em 0;">%s</div><div style="font-size: smaller; border-top: 1px solid #999;"><p><a href="%s">%s</a></p></div>';
$string['feedbackdeletedtext'] = 'Komentář k %s byl odstraněn uživatelem %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Pokud chcete vidět %s online, otevřete tento odkaz: %s';
$string['feedbacknotificationhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s byl komentován na %s</strong><br />%s</div><div style="margin: 1em 0;">%s</div><div style="font-size: smaller; border-top: 1px solid #999;"><p><a href="%s">Odpovědět na tento komentář online</a></p></div>';
$string['feedbacknotificationtext'] = '%s komentován na %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Pokud chcete vidět a odpovědět na tento komentář online, otevřete tento odkaz: %s';
$string['feedbackonviewbyuser'] = 'Zpětná vazba na %s od %s';
$string['feedbacksubmitted'] = 'Zpětná vazba odeslána';
$string['lastcomment'] = 'Poslední komentář';
$string['makepublic'] = 'Zveřejnit';
$string['makepublicnotallowed'] = 'Nemáte oprávnění zveřejnit tento komentář';
$string['makepublicrequestbyauthormessage'] = '%s požádal o zveřejnění tohoto komentáře.';
$string['makepublicrequestbyownermessage'] = '%s požádal o zveřejnění tohoto komentáře.';
$string['makepublicrequestsent'] = 'Zpráva s žádostí o zveřejnění příspevku byla odeslána uživateli %s.';
$string['makepublicrequestsubject'] = 'Žádost o změnu soukromého příspevku na veřejný';
$string['messageempty'] = 'Zpráva je prázdná. Prázdné zprávy jsou povoleny pouze pokud k nim přiložíte soubor.';
$string['moderatecomments'] = 'Moderovat komentáře';
$string['moderatecommentsdescription'] = 'Komentář zůstane soukromý, dokud ho neschválíte.';
$string['newfeedbacknotificationsubject'] = 'Nová zpětná vazba na %s';
$string['placefeedback'] = 'Zanechat zpětnou vazbu';
$string['pluginname'] = 'Komentář';
$string['rating'] = 'Hodnocení';
$string['reallydeletethiscomment'] = 'Jste si jisti, že chcete odstranit tento komentář?';
$string['thiscommentisprivate'] = 'Tento komentář je soukromý';
$string['typefeedback'] = 'Zpětná vazba';
$string['viewcomment'] = 'Zobrazit komentář';
$string['youhaverequestedpublic'] = 'Požádal jste o zveřejnění tohoto komentáře.';
