<?php

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'Necháte-li prázdné, bude použit název pole';
$string['description'] = 'Zobrazuje vybraný údaj ze životopisu';
$string['fieldtoshow'] = 'Pole k zobrazení';
$string['filloutyourresume'] = '%sVyplňte svůj životopis%s a objeví se zde na výběr více polí!';
$string['title'] = 'Jeden údaj ze životopisu';
