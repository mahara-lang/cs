<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['Created'] = 'Vytvořeno';
$string['Description'] = 'Popis';
$string['Download'] = 'Stáhnout';
$string['Note'] = 'Poznámka';
$string['Notes'] = 'Poznámky';
$string['Owner'] = 'Vlastník';
$string['Preview'] = 'Náhled';
$string['Size'] = 'Velikost';
$string['Title'] = 'Název';
$string['Type'] = 'Typ';
$string['aboutdescription'] = 'Zadejte vaše skutečné křestní jméno a příjmení. Pokud chcete být znám pod jiným jménem, vložte jej do pole Upřednostňované jméno.';
$string['aboutme'] = 'O mně';
$string['addbutton'] = 'Přidat';
$string['address'] = 'Poštovní adresa';
$string['aimscreenname'] = 'AIM účet';
$string['blogaddress'] = 'Adresa blogu';
$string['businessnumber'] = 'Pracovní telefon';
$string['city'] = 'Město';
$string['confirmdeletenote'] = 'Tato poznámka je použita v %s blocích a %pohledech. Pokud ji odstraníte, všechny stávající bloky, které ji obsahují, budou prázdné.';
$string['contact'] = 'Kontaktní údaje';
$string['containedin'] = 'Obsažená v:';
$string['country'] = 'Země';
$string['editnote'] = 'Upravit poznámku';
$string['email'] = 'E-mailová adresa';
$string['emailactivation'] = 'Aktivace e-mailu';
$string['emailactivationdeclined'] = 'Aktitace e-mailu byla úspěšně odmítnuta.';
$string['emailactivationfailed'] = 'Aktivace e-mailu selhala';
$string['emailactivationsucceeded'] = 'Aktivace e-mailu úspěšně provedena';
$string['emailaddress'] = 'Další e-mail';
$string['emailalreadyactivated'] = 'E-mail byl již aktivován';
$string['emailingfailed'] = 'Profil uložen, ale emaily nebyly odeslány na: %s';
$string['emailvalidation_body'] = 'Toto je automaticky generovaná zpráva pro: %s

K vašemu uživatelskému účtu byla přiřazena další emailová adresa %s. Tuto novou adresu je ještě třeba aktivovat na adrese:
%s';
$string['emailvalidation_body1'] = 'Dobrý den %s, do vašeho uživatelského účtu jste přidali e-mailovou adresu %s. Prosím, klikněte na odkaz níže pro její aktivaci.%s Pokud tento e-mail patří na vás, ale jste nepožádali o přidání do vašeho účtu %s, klikněte na následující odkaz, kterým tuto aktivaci odmítnete. %s';
$string['emailvalidation_subject'] = 'Ověření funkčnosti e-mailové adresy';
$string['faxnumber'] = 'Fax';
$string['firstname'] = 'Křestní jméno';
$string['fullname'] = 'Celé jméno';
$string['general'] = 'Obecné';
$string['homenumber'] = 'Telefon domů';
$string['icqnumber'] = 'ICQ';
$string['industry'] = 'Odvětví';
$string['infoisprivate'] = 'Tato informace zůstane soukromá dokud ji nepřidáte do pohledu, který sdílíte spolu s ostatními.';
$string['institution'] = 'Instituce';
$string['introduction'] = 'Úvod';
$string['invalidemailaddress'] = 'Neplatná e-mailová adresa';
$string['jabberusername'] = 'Jabber';
$string['lastmodified'] = 'Naposledy upraveno';
$string['lastname'] = 'Příjmení';
$string['loseyourchanges'] = 'Zahodit změny?';
$string['maildisabled'] = 'E-mail zakázán';
$string['mandatory'] = 'Povinné';
$string['mandatoryfields'] = 'Povinná pole';
$string['mandatoryfieldsdescription'] = 'Pole v profilu, která musí být vyplněna v';
$string['messaging'] = 'Zasílání zpráv';
$string['mobilenumber'] = 'Mobil';
$string['msnnumber'] = 'MSN';
$string['mynotes'] = 'Moje poznámky';
$string['name'] = 'Jméno';
$string['notedeleted'] = 'Poznámka odstraněna';
$string['notesdescription'] = 'Toto jsou html poznámky, které jste vytvořili uvnitř bloků textového pole ve vašich pohledech.';
$string['notesfor'] = 'Poznámky pro %s';
$string['noteupdated'] = 'Poznámka aktualizována';
$string['occupation'] = 'Pracovní pozice';
$string['officialwebsite'] = 'Oficiální webová stránka';
$string['personalwebsite'] = 'Osobní webová stránka';
$string['pluginname'] = 'Profil';
$string['preferredname'] = 'Upřednostňované jméno';
$string['principalemailaddress'] = 'Hlavní e-mail';
$string['profile'] = 'Profil';
$string['profilefailedsaved'] = 'Ukládání profilu selhalo';
$string['profileinformation'] = 'Profilové informace';
$string['profilepage'] = 'Profilová stránka';
$string['profilesaved'] = 'profil úspešně uložen';
$string['public'] = 'Veřejné';
$string['saveprofile'] = 'Uložit profil';
$string['searchablefields'] = 'Prohledávatelná pole';
$string['searchablefieldsdescription'] = 'Pole v profilu, která lze prohledávat ostatními';
$string['skypeusername'] = 'Skype';
$string['studentid'] = 'Studentské ID';
$string['town'] = 'Městská část';
$string['unvalidatedemailalreadytaken'] = 'Tato e-mailová adresa je již zabraná';
$string['validationemailsent'] = 'Ověření e-mailové adresy bylo odesláno';
$string['validationemailwillbesent'] = 'Po uložení profilu vám bude zaslán kontrolní e-mail pro ověření funkčnosti adresy';
$string['verificationlinkexpired'] = 'Odkaz pro ověření platnosti e-mailové adresy již není platný.';
$string['viewallprofileinformation'] = 'Zobrazit všechny profilové informace';
$string['viewmyprofile'] = 'Zobrazit můj profil';
$string['viewprofilepage'] = 'Zobrazit profilovou stránku';
$string['yahoochat'] = 'Yahoo';
