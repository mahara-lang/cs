<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['blockcontent'] = 'Obsah bloku';
$string['description'] = 'Přidat poznámky do pohledu';
$string['makeacopy'] = 'Vytvořit kopii';
$string['managealltextboxcontent'] = 'Spravovat obsah všech textových polí';
$string['readonlymessage'] = 'Vybraný text na této stránce nelze upravovat.';
$string['textusedinotherblocks'] = 'Pokud upravíte text tohoto bloku, dojde ke změněně obsahu %s dalších bloků.';
$string['title'] = 'Textové pole';
$string['usecontentfromanothertextbox'] = 'Použít obsah z jiného textového pole';
