<?php

defined('INTERNAL') || die();

$string['aboutme'] = 'O mně';
$string['description'] = 'Zobrazuje vybranou informaci z uživatelského profilu';
$string['dontshowemail'] = 'Nezobrazovat e-mailovou adresu';
$string['dontshowprofileicon'] = 'Nezobrazovat fotografii';
$string['fieldstoshow'] = 'Zobrazit pole';
$string['introtext'] = 'Úvodní text';
$string['title'] = 'Informace z profilu';
$string['uploadaprofileicon'] = 'Nemáte žádnou fotografii v profilu. <a href="%sartefact/file/profileicons.php" target="_blank">Přidejte ji zde.</a>';
$string['useintroductioninstead'] = 'Můžete také nechat zobrazit pole s úvodním popisem z vašeho profilu a ponechat zde toto pole prázdné.';
