<?php

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'Pokud je toto polé prázdné, bude použit název plánu.';
$string['description'] = 'Zobrazit seznam mých plánů';
$string['newerplans'] = 'Novější plány';
$string['noplansaddone'] = 'Dosud nejsou vytvořeny žádné plány. %sPřidejte nějaký%s!';
$string['olderplans'] = 'Starší plány';
$string['planstoshow'] = 'Plán k zobrazení';
$string['title'] = 'Moje plány';
