<?php

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'Ponecháte-li prázdné, bude použit název blogu';
$string['description'] = 'Zobrazuje celý blog';
$string['postsperpage'] = 'Příspevky na stránku';
$string['title'] = 'Blog';
