<?php

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'Ponecháte-li pole prázdné, bude použit název příspěvku blogu';
$string['description'] = 'Zobrazuje jeden příspěvek z blogu';
$string['title'] = 'Příspěvek v blogu';
