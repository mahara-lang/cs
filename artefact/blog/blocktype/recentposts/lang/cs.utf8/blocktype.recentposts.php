<?php

defined('INTERNAL') || die();

$string['description'] = 'Zobrazuje posledních 10 příspěvků v blogu';
$string['itemstoshow'] = 'Položky ke zobrazení';
$string['postedin'] = 'z';
$string['postedon'] = 'dne';
$string['title'] = 'Nedávné příspěvky z blogu';
