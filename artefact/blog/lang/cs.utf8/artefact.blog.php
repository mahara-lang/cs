<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['addblog'] = 'Přidat blog';
$string['addone'] = 'Přidat příspěvek!';
$string['addpost'] = 'Přidat příspěvek';
$string['alignment'] = 'Zarovnání';
$string['allowcommentsonpost'] = 'Povolit komentáře k tomuto příspěvku';
$string['allposts'] = 'Všechny přístpěvky';
$string['alt'] = 'Popis';
$string['attach'] = 'Připojit';
$string['attachedfilelistloaded'] = 'Nahrán seznam připojených souborů';
$string['attachedfiles'] = 'Připojené soubory';
$string['attachment'] = 'Příloha';
$string['attachments'] = 'Přílohy';
$string['baseline'] = 'Spodek obrázku je zarovnán na účaří (baseline)';
$string['blog'] = 'Blog';
$string['blogcopiedfromanotherview'] = 'Poznámka: tento blok byl zkopírován z jiného pohledu. Můžete jej přesouvat, ale nemůžete měnit jeho obsah, tj. zobrazený %s.';
$string['blogdeleted'] = 'Blog odstraněn';
$string['blogdesc'] = 'Popis';
$string['blogdescdesc'] = 'např. "Deník Davidových zkušeností a postřehů"';
$string['blogdoesnotexist'] = 'Pokoušíte se zobrazit neexistující blog';
$string['blogpost'] = 'Příspěvek blogu';
$string['blogpostdeleted'] = 'Příspěvek odstraněn';
$string['blogpostdoesnotexist'] = 'Pokoušíte se zobrazit neexistující příspěvek blogu';
$string['blogpostpublished'] = 'Příspěvek publikován';
$string['blogpostsaved'] = 'Příspěvek uložen';
$string['blogs'] = 'Blogy';
$string['blogsettings'] = 'Nastavení blogu';
$string['blogtitle'] = 'Název';
$string['blogtitledesc'] = 'např. "Eliščin deník ke Cvičení z ošetřovatelských postupů"';
$string['border'] = 'Ohraničení';
$string['bottom'] = 'Spodek obrázku je zarovnán na spodek písma (bottom)';
$string['cancel'] = 'Zrušit';
$string['cannotdeleteblogpost'] = 'Vyskytla se chyba při odstraňování tohoto příspěvku';
$string['copyfull'] = 'Při kopírování pohledu se vytvoří nová kopie tohoto bloku %s';
$string['copynocopy'] = 'Při kopírování pohledu tento blok přeskočit';
$string['copyreference'] = 'Při kopírování pohledu se vytvoří odkaz na váš blok %s';
$string['createandpublishdesc'] = 'Tímto se vytvoří nový příspěvek v blogu a zároveň se zpřístupní ostatním.';
$string['createasdraftdesc'] = 'Tímto se vytvoří nový příspěvek v blogu, ale prozatím nebude dostupný ostatním, dokud jej nezveřejníte.';
$string['createblog'] = 'Vytvořit blog';
$string['dataimportedfrom'] = 'Data importována z %s';
$string['defaultblogtitle'] = 'Blog %s\'';
$string['delete'] = 'Odstranit';
$string['deleteblog?'] = 'Opravdu si přejete odstranit celý tento blog a všechny příspěvky v něm?';
$string['deleteblogpost?'] = 'Opravdu si přejete odstranit celý tento příspěvek?';
$string['description'] = 'Popis';
$string['dimensions'] = 'Rozměry';
$string['draft'] = 'Pracovní verze';
$string['edit'] = 'upravit';
$string['editblogpost'] = 'Upravit příspěvek v blogu';
$string['enablemultipleblogstext'] = 'Již jeden blog máte. Pokud chcete mít další, povolte volbu více blogů na stránce s <a href="%saccount/">nastavením účtu</a>.';
$string['entriesimportedfromleapexport'] = 'Položky importované z exportu LEAP není možné importovat jinam';
$string['errorsavingattachments'] = 'Při ukládání příloh se vyskytla chyba';
$string['feedrights'] = 'Copyright %s.';
$string['feedsnotavailable'] = 'Kanály nejsou k dispozici pro tento typ artefaktu.';
$string['horizontalspace'] = 'Okraj ve vodorovném směru (hspace)';
$string['image_list'] = 'Přiložený soubor';
$string['insert'] = 'Vložit';
$string['insertimage'] = 'Vložit obrázek';
$string['left'] = 'Obrázek je umístěn k levému okraji a obtékán zprava';
$string['middle'] = 'Střed obrázku je zarovnán s účařím písma řádku (middle)';
$string['moreoptions'] = 'Více možností';
$string['mustspecifycontent'] = 'Musíte vložit obsah vašeho příspěvku';
$string['mustspecifytitle'] = 'Musíte zadat název vašeho příspěvku';
$string['name'] = 'Název';
$string['newattachmentsexceedquota'] = 'Souhrnná velikost souborů připojených k tomuto příspěvku by přesáhla vám přidělenou kvótu. Před uložením příspěvku musíte odebrat některé z přiložených souborů.';
$string['newblog'] = 'Nový blog';
$string['newblogpost'] = 'Nový příspěvek v blogu "%s"';
$string['newerposts'] = 'Novější příspěvky';
$string['nodefaultblogfound'] = 'Nenalezen výchozí blog. Jedná se o chybu v systému. Pro opravu je třeba povolit volbu více blogů na stránce s <a href="%saccount/">nastavením účtu</a>.';
$string['nofilesattachedtothispost'] = 'Bez příloh';
$string['noimageshavebeenattachedtothispost'] = 'K příspěvku nebyly připojeny žádné obrázky. Před vložením obrázku jej musíte buď nahrát nebo připojit.';
$string['nopostsyet'] = 'Dosud žádné příspěvky.';
$string['noresults'] = 'Nebyly nalezeny žádné příspěvky';
$string['olderposts'] = 'Starší příspěvky';
$string['pluginname'] = 'Blogy';
$string['post'] = 'příspěvek';
$string['postbody'] = 'Tělo příspěvku';
$string['postedbyon'] = '%s, %s';
$string['postedon'] = 'Publikováno';
$string['posts'] = 'příspěvků';
$string['postscopiedfromview'] = 'Příspěvky zkopírovány z %s';
$string['posttitle'] = 'název';
$string['publish'] = 'Zveřejnit';
$string['publishblogpost?'] = 'Opravdu chcete zveřejnit tento příspěvek?';
$string['published'] = 'Zveřejněno';
$string['publishfailed'] = 'Vyskytla se chyba. Příspěvek nebyl zveřejněn';
$string['remove'] = 'Odebrat';
$string['right'] = 'Obrázek je umístěn k pravému okraji a obtékán zleva (right)';
$string['save'] = 'Uložit';
$string['saveandpublish'] = 'Uložit a zveřejnit';
$string['saveasdraft'] = 'Uložit jako pracovní verzi';
$string['savepost'] = 'Uložit příspěvek';
$string['savesettings'] = 'Uložit nastavení';
$string['settings'] = 'Nastavení';
$string['src'] = 'URL obrázku';
$string['textbottom'] = 'Text dole';
$string['texttop'] = 'Vršek obrázku je zarovnán s nejvyšším bodem textu (texttop)';
$string['thisisdraft'] = 'Toto je pracovní verze příspěvku';
$string['thisisdraftdesc'] = 'Dokud je příspěvek v pracovní verzi, není nikomu kromě vás dostupný';
$string['title'] = 'Název';
$string['top'] = 'Vršek obrázku je zarovnán s nejvyšším bodem libovolného objektu řádku (top)';
$string['update'] = 'Aktualizovat';
$string['verticalspace'] = 'Okraj ve svislém směru (vspace)';
$string['viewblog'] = 'Zobrazit blog';
$string['viewposts'] = 'Zkopírované příspěvky (%s)';
$string['youarenottheownerofthisblog'] = 'Nejste vlastníkem tohoto blogu';
$string['youarenottheownerofthisblogpost'] = 'Nejste vlastníkem tohoto příspěvku';
$string['youhaveblogs'] = 'Počet vašich blogů je %s.';
$string['youhavenoblogs'] = 'Nemáte žádné blogy.';
$string['youhaveoneblog'] = 'Máte jeden blog';
