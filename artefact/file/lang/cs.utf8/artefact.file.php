<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['Contents'] = 'Obsah';
$string['Continue'] = 'Pokračovat';
$string['Created'] = 'Vytvořeno';
$string['Date'] = 'Datum';
$string['Default'] = 'Výchozí';
$string['Description'] = 'Popis';
$string['Details'] = 'Detaily';
$string['Download'] = 'Stáhnout';
$string['File'] = 'Soubor';
$string['Files'] = 'Soubory';
$string['Folder'] = 'Složka';
$string['Folders'] = 'Složky';
$string['Images'] = 'Obrázky';
$string['Name'] = 'Název';
$string['Owner'] = 'Vlastník';
$string['Preview'] = 'Náhled';
$string['Size'] = 'Velikost';
$string['Title'] = 'Název';
$string['Type'] = 'Typ';
$string['Unzip'] = 'Rozbalit';
$string['addafile'] = 'Přidat soubor';
$string['ai'] = 'Postscript';
$string['aiff'] = 'AIFF audio';
$string['application'] = 'Neznámá aplikace';
$string['archive'] = 'Archív';
$string['au'] = 'AU audio';
$string['audio'] = 'Soubor s audiem';
$string['avi'] = 'AVI video';
$string['bmp'] = 'Rastrová grafika';
$string['bytes'] = 'bajtů';
$string['bz2'] = 'Bzip2 archív';
$string['cannoteditfolder'] = 'Nemáte oprávnění přidat obsah do této složky';
$string['cannoteditfoldersubmitted'] = 'Nemůžete přidat obsah do složky s odeslaným pohledem.';
$string['cannotremovefromsubmittedfolder'] = 'Nemůžete obdebrat obsah ze složky s odeslaným pohledem.';
$string['cantcreatetempprofileiconfile'] = 'Nelze uložit dočasný soubor - %s';
$string['changessaved'] = 'Změny uloženy';
$string['clickanddragtomovefile'] = 'Klepnutím a tažením přesunout %s';
$string['confirmdeletefile'] = 'Opravdu chcete odstranit tento soubor?';
$string['confirmdeletefolder'] = 'Opravdu chcete odstranit tuto složku?';
$string['confirmdeletefolderandcontents'] = 'Opravdu chcete odstranit tuto složku s celým jejím obsahem?';
$string['contents'] = 'Obsah';
$string['copyrightnotice'] = 'Prohlášení';
$string['create'] = 'Vytvořit';
$string['createdtwothings'] = 'Vytvořit %s a %s.';
$string['createfolder'] = 'Vytvořit složku';
$string['customagreement'] = 'Vlasní dohoda';
$string['defaultagreement'] = 'Výchozí dohoda';
$string['defaultgroupquota'] = 'Výchozí kvóta skupiny';
$string['defaultgroupquotadescription'] = 'Můžete nastavit množství diskového prostoru, které mohou využít nové skupiny pro své soubory.';
$string['defaultprofileicon'] = 'Toto je v současnosti nastaveno jako výchozí obrázek profilu.';
$string['defaultquota'] = 'Výchozí kvóta';
$string['defaultquotadescription'] = 'Výchozí hodnota velikosti prostoru na disku, který budou mít noví uživatelé k dispozici. Již nastavené kvóty nebudou změněny.';
$string['deletefile?'] = 'Opravdu chcete odstranit tento soubor?';
$string['deletefolder?'] = 'Opravdu chcete odstranit tuto složku?';
$string['deleteselectedicons'] = 'Odstranit vybrané';
$string['deletingfailed'] = 'Odstranění se nezdařilo: soubor nebo složka již dále neexistuje';
$string['destination'] = 'Cíl';
$string['doc'] = 'Dokument MS Word';
$string['downloadfile'] = 'Stáhnout %s';
$string['downloadoriginalversion'] = 'Stáhnout původní verzi';
$string['dss'] = 'DSS audio';
$string['editfile'] = 'Upravit soubor';
$string['editfolder'] = 'Upravit složku';
$string['editingfailed'] = 'Úprava se nezdařila: soubor nebo složka již dále neexistuje';
$string['emptyfolder'] = 'Prázdná složka';
$string['extractfilessuccess'] = 'Vytvořeno %s složek a %s souborů.';
$string['file'] = 'soubor';
$string['fileadded'] = 'Soubor vybrán';
$string['filealreadyindestination'] = 'Přesouvaný soubor v této složce již existuje';
$string['fileappearsinviews'] = 'Tento soubor se zobrazí v jednom nebo více z vašich pohledů.';
$string['fileattached'] = 'Tento soubor je připojen k tomuto počtu dalších položek ve vašem portfoliu: %s';
$string['fileattachedtoportfolioitems'] = 'Pole';
$string['fileexists'] = 'Soubor existuje';
$string['fileexistsoverwritecancel'] = 'Soubor s tímto názvem již existuje. Můžete buď zvolit jiný název nebo přepsat stávající soubor.';
$string['fileinstructions'] = 'Sem můžete nahrávat obrázky, dokumenty a další soubory, které budete chtít zveřejňovat v pohledech na vaše portfolio. K přesunutí souboru nebo složky na ně klikněte myší a přetáhněte na požadované místo.';
$string['filelistloaded'] = 'Nahrán seznam souborů';
$string['filemoved'] = 'Soubor úspěšně přesunut';
$string['filenamefieldisrequired'] = 'Pole se souborem je povinné';
$string['filepermission.edit'] = 'Upravit';
$string['filepermission.republish'] = 'Zveřejnit';
$string['filepermission.view'] = 'Pohled';
$string['fileremoved'] = 'Soubor odstraněn';
$string['files'] = 'soubory';
$string['filesextractedfromarchive'] = 'Extrakce souborů z archívu';
$string['filesextractedfromziparchive'] = 'Extrakce souborů z archívu ZIP';
$string['fileswillbeextractedintofolder'] = 'Soubory byly extrahovány do %s';
$string['filethingdeleted'] = '%s odstraněn';
$string['fileuploadedas'] = '%s nahrán jako "%s"';
$string['fileuploadedtofolderas'] = '%s nahrán do %s jako "%s"';
$string['filewithnameexists'] = 'Soubor nebo složka jména "%s" již existuje.';
$string['flv'] = 'Flash video';
$string['folder'] = 'Složka';
$string['folderappearsinviews'] = 'Tato složka se zobrazí v jednom nebo více z vašich pohledů.';
$string['foldercontainsprofileicons'] = 'Tato složka obsahuje následující počet obrázků profilu: %s.';
$string['foldercreated'] = 'Složka vytvořena';
$string['foldernamerequired'] = 'Prosím zadejte jméno nové složky.';
$string['foldernotempty'] = 'Tato složka není prázdná.';
$string['gif'] = 'GIF obrázek';
$string['gotofolder'] = 'Přejít do %s';
$string['groupfiles'] = 'Seskupit soubory';
$string['gz'] = 'Gzip archív';
$string['home'] = 'Domů';
$string['html'] = 'HTML soubor';
$string['htmlremovedmessage'] = 'Prohlížíte si <strong>%s</strong> od uživatele <a href="%s">%s</a>. Obsah níže zobrazeného souboru byl profiltrován, aby neobsahoval případný škodlivý obsah a nemusí zcela odpovídat původnímu originálu.';
$string['htmlremovedmessagenoowner'] = 'Prohlížíte si <strong>%s</strong>. Obsah níže zobrazeného souboru byl profiltrován, aby neobsahoval případný škodlivý obsah a nemusí zcela odpovídat původnímu originálu.';
$string['image'] = 'Obrázek';
$string['imagesdir'] = 'Obrázky';
$string['imagesdirdesc'] = 'Soubory s obrázky';
$string['imagetitle'] = 'Popisek obrázku';
$string['institutionoverride'] = 'Kvóta instituce';
$string['institutionoverridedescription'] = 'Můžete povolit správcům institucí, aby mohli nastavit kvóty pro soubory uživatelů a mít výchozí kvóty pro každou z institucí.';
$string['insufficientquotaforunzip'] = 'Vaše zbývající disková kvóta je příliš malá k rozbalení tohoto souboru.';
$string['invalidarchive'] = 'Chyba při čtení archívu.';
$string['jpeg'] = 'JPEG obrázek';
$string['jpg'] = 'JPEG obrázek';
$string['js'] = 'Javascript';
$string['lastmodified'] = 'Naposledy upraveno';
$string['latex'] = 'Dokument LaTeX';
$string['m3u'] = 'M3U audio';
$string['maxquota'] = 'Maximální disková kvóta';
$string['maxquotadescription'] = 'Můžete nastavit maximální diskovou kvótu, kterou správce může přidělit uživateli. Stávající diskové kvóty tím nebudou nijak ovlivněny.';
$string['maxquotaenabled'] = 'Vynutit maximální diskovou kvótu na webu.';
$string['maxquotaexceeded'] = 'Zadali jste vyšší diskovou kvótu než momentální nastavení pro tuto stránku (% s). Zkuste zadat nižší hodnotu nebo kontaktujte správce webu, aby maximální diskovou kvótu zvýšil.';
$string['maxquotaexceededform'] = 'Prosím nastavte kvótu pro velikost nahrávaných souborů menší než %s.';
$string['maxquotatoolow'] = 'Maximální disková kvóta nemůže být menší než výchozí kvóta.';
$string['maxuploadsize'] = 'Maximální velikost nahrávaných souborů';
$string['mov'] = 'MOV Quicktime video';
$string['movefailed'] = 'Přesun selhal';
$string['movefaileddestinationinartefact'] = 'Nelze přesunout složku do sebe sama';
$string['movefaileddestinationnotfolder'] = 'Cíl přesunu musí být složka';
$string['movefailednotfileartefact'] = 'Přesouvat lze pouze soubory, složky a obrázky';
$string['movefailednotowner'] = 'Nemáte oprávnění přesouvat tento soubor do zadané složky';
$string['movingfailed'] = 'Přesun selhal: soubor nebo složky již dále neexistuje';
$string['mp3'] = 'MP3 audio';
$string['mp4_audio'] = 'MP4 audio';
$string['mp4_video'] = 'MP4 video';
$string['mpeg'] = 'MPEG video';
$string['mpg'] = 'MPEG video';
$string['myfiles'] = 'Moje soubory';
$string['namefieldisrequired'] = 'Musíte vyplnit pole s názvem';
$string['nametoolong'] = 'Název je příliš dlouhý. Vyberte nějaký kratší.';
$string['nfiles'] = 'Pole';
$string['nfolders'] = 'Pole';
$string['nofilesfound'] = 'Nenalezeny žádné soubory';
$string['noimagesfound'] = 'Nenalezeny žádné obrázky';
$string['notpublishable'] = 'Nemáte oprávnění ke zveřejnění tohoto souboru';
$string['nprofilepictures'] = 'Pole';
$string['odb'] = 'Databáze OpenOffice';
$string['odc'] = 'Tabulka OpenOffice';
$string['odf'] = 'Vzorec OpenOffice';
$string['odg'] = 'Grafika OpenOffice';
$string['odi'] = 'Obrázek OpenOffice';
$string['odm'] = 'Hlavní dokument OpenOffice';
$string['odp'] = 'Prezentace OpenOffice';
$string['ods'] = 'Sešit OpenOffice';
$string['odt'] = 'Textový dokument OpenOffice';
$string['onlyfiveprofileicons'] = 'Můžete uložit nejvýše pět obrázků do vašeho profilu';
$string['or'] = 'nebo';
$string['oth'] = 'Webová stránka OpenOffice';
$string['ott'] = 'Šablona OpenOffice';
$string['overwrite'] = 'Přepsat';
$string['parentfolder'] = 'Nadřazená složka';
$string['pdf'] = 'PDF dokument';
$string['pleasewaitwhileyourfilesarebeingunzipped'] = 'Počkejte prosím, než se vaše soubory rozbalí.';
$string['pluginname'] = 'Soubory';
$string['png'] = 'PNG obrázek';
$string['ppt'] = 'Prezentace Powerpoint';
$string['profileicon'] = 'Obrázek profilu';
$string['profileiconaddedtoimagesfolder'] = 'Váš obrázek profilu byl nahrán do složky %s.';
$string['profileiconimagetoobig'] = 'Vámi nahraný obrázek je příliš velký (%sx%s pixelů). Nesmí být větší než %sx%s pixelů';
$string['profileicons'] = 'Obrázky profilu';
$string['profileiconsdefaultsetsuccessfully'] = 'Výchozí obrázek profilu úspěšně nastaven';
$string['profileiconsdeletedsuccessfully'] = 'Obrázek zdárně odstraněn';
$string['profileiconsetdefaultnotvalid'] = 'Nebylo možno nastavit výchozí obrázek profilu, volba nebyla platná';
$string['profileiconsiconsizenotice'] = 'Sem můžete nahrát až <strong>pět</strong> obrázků s vaší fotografií, avatarem či ikonou. Jeden z nich pak kdykoliv můžete vybrat jako výchozí, který bude zobrazován. Obrázky musí mít velikost mezi 16x16 až %sx%s pixelů.';
$string['profileiconsize'] = 'Velikost obrázku v profilu';
$string['profileiconsnoneselected'] = 'Nebyly vybrány žádné obrázky k odstranění';
$string['profileiconuploadexceedsquota'] = 'Nahráním těchto obrázků by byla překročena vám přidělená kvóta diskového prostoru. Zkuste zmenšit rozměry obrázku nebo odstranit nějaké nepotřebné soubory.';
$string['quicktime'] = 'Quicktime video';
$string['ra'] = 'Real audio';
$string['ram'] = 'Real video';
$string['removingfailed'] = 'Odstranění selhalo: soubor či složka již dále neexistuje';
$string['requireagreement'] = 'Potvrzení vyžadováno';
$string['rm'] = 'Real video';
$string['rpm'] = 'Real video';
$string['rtf'] = 'RTF dokument';
$string['savechanges'] = 'Uložit změny';
$string['selectafile'] = 'Vyberte soubor';
$string['selectingfailed'] = 'Výběr selhal: soubor či složka již dále neexistuje';
$string['setdefault'] = 'Nastavit výchozí';
$string['sgi_movie'] = 'SGI video';
$string['sh'] = 'Shellový skript';
$string['sitefilesloaded'] = 'Soubory stránek nahrány';
$string['spacerequired'] = 'Požadovaný prostor';
$string['spaceused'] = 'Využitý prostor';
$string['swf'] = 'Flash video';
$string['tar'] = 'TAR archív';
$string['timeouterror'] = 'Nahrávání souboru selhalo - pokuste se nahrát soubor znovu';
$string['title'] = 'Název';
$string['titlefieldisrequired'] = 'Pole s názvem je povinné';
$string['txt'] = 'Čistý text';
$string['unzipprogress'] = '%s souborů/složek vytvořeno.';
$string['updategroupquotas'] = 'Aktualizovat kvótu skupiny';
$string['updategroupquotasdesc'] = 'Je-li zaškrtnuto, bude výše zvolená výchozí kvóta použita pro všechny existující skupiny.';
$string['updateuserquotas'] = 'Aktualizovat kvótu uživatele';
$string['updateuserquotasdesc'] = 'Je-li zaškrtnuto, bude výše zvolená výchozí kvóta použita pro všechny existující uživatele.';
$string['upload'] = 'Nahrát';
$string['uploadagreement'] = 'Nahrát dohodu';
$string['uploadagreementdescription'] = 'Povolte tuto volbu, pokud chcete přinutit uživatele, aby souhlasili s níže uvedenou dohodou, než budou moci nahrát soubor na web.';
$string['uploadedprofileicon'] = 'Nahraný profilový obrázek';
$string['uploadedprofileiconsuccessfully'] = 'Nové obrázky profilu úspěšně nahrány';
$string['uploadexceedsquota'] = 'Nahráním těchto souborů by byla překročena vám přidělená kvóta diskového prostoru. Zkuste odstranit nějaké nepotřebné soubory.';
$string['uploadfile'] = 'Nahrát soubor';
$string['uploadfileexistsoverwritecancel'] = 'Soubor s tímto názvem již existuje. Můžete buď zvolit jiný název nebo přepsat stávající soubor.';
$string['uploadingfile'] = 'nahrávám soubor...';
$string['uploadingfiletofolder'] = 'Nahrávám %s do %s';
$string['uploadoffilecomplete'] = 'Nahrávání souboru %s dokončeno';
$string['uploadoffilefailed'] = 'Nahrávání souboru %s selhalo';
$string['uploadoffiletofoldercomplete'] = 'Nahrávání souboru %s do složky %s dokončeno';
$string['uploadoffiletofolderfailed'] = 'Nahrávání souboru %s do složky %s selhalo';
$string['uploadprofileicon'] = 'Nahrát obrázek profilu';
$string['usecustomagreement'] = 'Použít vlastní dohodu';
$string['usenodefault'] = 'Nezobrazovat žádný';
$string['usingnodefaultprofileicon'] = 'Žádný obrázek nebude zobrazován';
$string['video'] = 'Soubor s videem';
$string['wav'] = 'WAV audio';
$string['wmv'] = 'WMV video';
$string['wrongfiletypeforblock'] = 'Vámi nahraný soubor nekoresponduje s vybraným typem bloku.';
$string['xml'] = 'XML soubor';
$string['youmustagreetothecopyrightnotice'] = 'Musíte vyjádřit svůj souhlas s prohlášením o autorských právech';
$string['zip'] = 'ZIP archív';
