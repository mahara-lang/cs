<?php

defined('INTERNAL') || die();

$string['configdesc'] = 'Nastavte, jaké typy souborů budou moci uživatelé vkládat do tohoto bloku. Jednotlivé typy musejí být rovněž povoleny v konfiguraci položky portfolia Soubor. Pokud zakážete typ, který byl už někam vložen, přestane se zobrazovat.';
$string['description'] = 'Zobrazuje nástroj pro přehrávání videa';
$string['flashanimation'] = 'Flash animace';
$string['media'] = 'Média';
$string['title'] = 'Přehrávání videa';
$string['typeremoved'] = 'V tomto bloku se má zobrazovat video, ale použitý formát byl správcem serveru zakázán.';
