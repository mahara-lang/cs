<?php

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'Ponecháte-li toto pole prázdné, bude použit název složky';
$string['description'] = 'Zobrazuje jednu celou složku';
$string['title'] = 'Složka';
