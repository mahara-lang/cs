<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/cs.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     David Mudrak, Viktor Fuglik
 * @copyright  (C) 2008-2012
 *
 */

defined('INTERNAL') || die();

$string['Photo'] = 'Fotografie';
$string['by'] = 'od';
$string['cannotdisplayslideshow'] = 'Promítání snímků nelze zobrazit.';
$string['description'] = 'Kolekce obrázků z mého datového úložiště';
$string['externalgalleryurl'] = 'URL nebo RSS galerie';
$string['externalgalleryurldesc'] = 'Můžete vložit následující externí galerie:';
$string['externalnotsupported'] = 'Vložené externí URL není podporováno';
$string['flickrapikey'] = 'Klíč Flickr API';
$string['flickrapikeydesc'] = 'Chcete-li zobrazit soubory fotografií z Flickr, budete potřebovat platný klíč Flickr API. <a href="http://www.flickr.com/services/api/keys/apply/" target="_blank">Získejte klíč online.</ a>.';
$string['flickrsets'] = 'Soubory Flickr';
$string['flickrsettings'] = 'Nastavení Flickr';
$string['gallerysettings'] = 'Nastavení galerie';
$string['panoramiocopyright'] = 'Fotografie z Panoramio jsou chráněny autorskými právy jejich vlastníků.';
$string['panoramiouserphotos'] = 'Fotografie uživatelů Panoramio';
$string['pbapikey'] = 'Klíč Photobucket API';
$string['pbapikeydesc'] = 'Chcete-li zobrazit alba fotografií z Photobucket, budete potřebovat platný klíč nebo privátní klíč API. Navštivte <a href="http://www.flickr.com/services/api/keys/apply/" target="_blank">stránku vývojářů Photobucket</ a>, souhlaste s podmínkami služby, zaregistrujte se získejte klíče API.';
$string['pbapiprivatekey'] = 'Privátní klíč Photobucket API';
$string['pbsettings'] = 'Nastavení Photobucket';
$string['photobucketphotosandalbums'] = 'Fotografie a alba Photobucket';
$string['photoframe'] = 'Použít fotorámeček?';
$string['photoframedesc'] = 'Pokud je povoleno, rámeček bude vykreslen okolo náhledu každého obrázku v galerii.';
$string['picasaalbums'] = 'Alba Picasa';
$string['previewwidth'] = 'Maximální šířka obrázku';
$string['previewwidthdesc'] = 'Nastavit maximální šířku obrázků použitou při zobrazení se Slimbox 2.';
$string['select'] = 'Výběr obrázku';
$string['selectexternal'] = 'Zobrazit snímky z externí galerie';
$string['selectfolder'] = 'Zobrazit všechny obrázky z jedné z mých složek (bude obsahovat i obrázky nahrané později)';
$string['selectimages'] = 'Budu si vybírat jednotlivé obrázky ke zobrazení.';
$string['style'] = 'Vzhled';
$string['styleslideshow'] = 'Promítání snímků';
$string['stylesquares'] = 'Miniatury (čtvtercové)';
$string['stylethumbs'] = 'Miniatury';
$string['title'] = 'Galerie obrázků';
$string['useslimbox2'] = 'Použít Slimbox 2?';
$string['useslimbox2desc'] = 'Slimbox 2 (vizuální klon Lightbox 2) je jednoduchý, nenápadný skript používaný pro překrytí snímků na aktuální stránce.';
$string['width'] = 'Šířka';
$string['widthdescription'] = 'Určete šířku pro vaše obrázky (v pixelech). Snímky budou na tuto šířku zmenšeny.';
$string['windowslivephotoalbums'] = 'Alba Windows Live photo gallery';
