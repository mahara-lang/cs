<?php

defined('INTERNAL') || die();

$string['emailfooter'] = 'Toto je automaticky zasílané upozornění ze stránek %s. Pro nastavení zasílání upozornění navštivte stránku %s';
$string['emailheader'] = 'Máte nové upozornění ze stránek %s s následujícím zněním:';
$string['emailsubject'] = '%s';
$string['name'] = 'E-mailem';
$string['referurl'] = 'Vizte %s';
